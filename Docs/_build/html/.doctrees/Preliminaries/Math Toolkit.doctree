���C      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Math Toolkit�h]�h	�Text����Math Toolkit�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhKh�Q/home/ryan/repos/gitlab/physics/quantum-fields/Docs/Preliminaries/Math Toolkit.md�hhhhubh	�	paragraph���)��}�(hX+  There are some mathematical tools that you'll need to be firmly in control of to tackle real field theory problems. Here I'll only go over broad points and give a sample problem or two; the real required level of maturity with these techniques will be far more sophisticated than those on this page.�h]�hX/  There are some mathematical tools that you’ll need to be firmly in control of to tackle real field theory problems. Here I’ll only go over broad points and give a sample problem or two; the real required level of maturity with these techniques will be far more sophisticated than those on this page.�����}�(hhhh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hKhh,hhhhubeh}�(h!]��math-toolkit�ah#]�(�tex2jax_ignore��mathjax_ignore�eh%]��math toolkit�ah']�h)]�uh+h
hKhh,hhhhubh)��}�(hhh]�(h)��}�(h�Fourier Transforms�h]�h�Fourier Transforms�����}�(hhhhJhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhKhh,hhGhhubh)��}�(hhh]�(h)��}�(h�FT/IFT Conventions�h]�h�FT/IFT Conventions�����}�(hhhh[hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhKhh,hhXhhubh	�target���)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]��refid��-equation-7c61a234-acc7-4e39-a57f-feb97963c0b2�uh+hihKhh,hhXhhubh	�
math_block���)��}�(hX�  \begin{align}
\text{FT}: \quad g(\mathbf{k}) &= \int^{\infty}_{\infty} f(\mathbf{x})  
    e^{i\mathbf{k\cdot x}} d^d\mathbf{x} \\
g(\omega) &= \int^{\infty}_{\infty} f(t)   e^{-i\omega t} dt\\
\text{IFT}: \quad f(\mathbf{x}) &= \frac{1}{(2 \pi)^d} \int^{\infty}_{\infty} g(\mathbf{k})
    e^{-i\mathbf{k\cdot x}} d^d\mathbf{k} \\
f(t) &= \frac{1}{(2 \pi)}\int^{\infty}_{\infty} g(\omega) e^{i\omega t} d\omega
\end{align}�h]�hX�  \begin{align}
\text{FT}: \quad g(\mathbf{k}) &= \int^{\infty}_{\infty} f(\mathbf{x})  
    e^{i\mathbf{k\cdot x}} d^d\mathbf{x} \\
g(\omega) &= \int^{\infty}_{\infty} f(t)   e^{-i\omega t} dt\\
\text{IFT}: \quad f(\mathbf{x}) &= \frac{1}{(2 \pi)^d} \int^{\infty}_{\infty} g(\mathbf{k})
    e^{-i\mathbf{k\cdot x}} d^d\mathbf{k} \\
f(t) &= \frac{1}{(2 \pi)}\int^{\infty}_{\infty} g(\omega) e^{i\omega t} d\omega
\end{align}�����}�(hhhhxubah}�(h!]�huah#]��amsmath�ah%]�h']�h)]��nowrap���number�K�label��$7c61a234-acc7-4e39-a57f-feb97963c0b2��	xml:space��preserve��docname��Preliminaries/Math Toolkit�uh+hvhKhh,hhXhh�expect_referenced_by_name�}��expect_referenced_by_id�}�huhksubh.)��}�(h��With this convention, the two sets of $(d+1)$ integrals can be combined simply into one equation, as is often done in relativistic QFT in Minkowski spacetime.�h]�(h�&With this convention, the two sets of �����}�(hhhh�hhhNhNubh	�math���)��}�(h�(d+1)�h]�h�(d+1)�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hK$hh,hh�hhubh�q integrals can be combined simply into one equation, as is often done in relativistic QFT in Minkowski spacetime.�����}�(hhhh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hK$hh,hhXhhubhj)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�ht�-equation-06fc68c1-ac75-45a6-a215-51d6a11e7822�uh+hihK&hh,hhXhhubhw)��}�(h��\begin{align}
\text{FT}: \quad g(k^\mu) &= \int^{\infty}_{\infty} f(x)  
    e^{ik_\mu x^\mu} d^{(d+1)}x^\mu \\
\text{IFT}: \quad g(k^\mu) &= \int^{\infty}_{\infty} f(x^\mu)   e^{-i k_\mu x^\mu} d^{(d+1)}k\\
\end{align}�h]�h��\begin{align}
\text{FT}: \quad g(k^\mu) &= \int^{\infty}_{\infty} f(x)  
    e^{ik_\mu x^\mu} d^{(d+1)}x^\mu \\
\text{IFT}: \quad g(k^\mu) &= \int^{\infty}_{\infty} f(x^\mu)   e^{-i k_\mu x^\mu} d^{(d+1)}k\\
\end{align}�����}�(hhhh�ubah}�(h!]�h�ah#]�h�ah%]�h']�h)]��nowrap���number�K�label��$06fc68c1-ac75-45a6-a215-51d6a11e7822�h�h�h�h�uh+hvhK&hh,hhXhhh�}�h�}�h�h�subh.)��}�(h�"So far we haven't chosen a metric.�h]�h�$So far we haven’t chosen a metric.�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hK,hh,hhXhhubhw)��}�(h�K
kx \equiv \mathbf{k \cdot x} \equiv k_\mu x^\mu = g_{\mu \nu} k^\mu x^\nu
�h]�h�K
kx \equiv \mathbf{k \cdot x} \equiv k_\mu x^\mu = g_{\mu \nu} k^\mu x^\nu
�����}�(hhhh�ubah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�Nh�h�uh+hvhK.hh,hhXhhubh.)��}�(hXX  If we work with the Minkowski metric, there are two main choices: $(1, -3)$, with positive time component and negative space components, or $(-1, 3)$, with negative time component and positive space components. One reason I like the FT sign conventions above is that I prefer the $(-1, 3)$ metric; defined this way, the phase of a plane wave is�h]�(h�BIf we work with the Minkowski metric, there are two main choices: �����}�(hhhh�hhhNhNubh�)��}�(h�(1, -3)�h]�h�(1, -3)�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hK2hh,hh�hhubh�A, with positive time component and negative space components, or �����}�(hhhh�hhhNhNubh�)��}�(h�(-1, 3)�h]�h�(-1, 3)�����}�(hhhj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hK2hh,hh�hhubh��, with negative time component and positive space components. One reason I like the FT sign conventions above is that I prefer the �����}�(hhhh�hhhNhNubh�)��}�(h�(-1, 3)�h]�h�(-1, 3)�����}�(hhhj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hK2hh,hh�hhubh�7 metric; defined this way, the phase of a plane wave is�����}�(hhhh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hK2hh,hhXhhubhw)��}�(h�=
\psi(x^\mu) \sim e^{i k_\mu x^\mu} = e^{i (kx - \omega t)} 
�h]�h�=
\psi(x^\mu) \sim e^{i k_\mu x^\mu} = e^{i (kx - \omega t)} 
�����}�(hhhj5  ubah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�Nh�h�uh+hvhK4hh,hhXhhubh)��}�(hhh]�(h)��}�(h�A brief detour on plane waves�h]�h�A brief detour on plane waves�����}�(hhhjH  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhK:hh,hjE  hhubh.)��}�(h��The difference between a forward and backward propagating wave is not an overall plus or minus sign, but a difference between the space and time components. Consider a plane wave in quantum mechanics.�h]�h��The difference between a forward and backward propagating wave is not an overall plus or minus sign, but a difference between the space and time components. Consider a plane wave in quantum mechanics.�����}�(hhhjV  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hK<hh,hjE  hhubhj)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�ht�-equation-63d33f06-a4a0-42ec-bb18-ed88bdb633ec�uh+hihK>hh,hjE  hhubhw)��}�(hX!  \begin{align}
\left< p | p |x \right> &= p^\dagger \left< p | x \right> \text{(operate } \leftarrow \text{)} \\
                        &=  \frac{\hbar}{i} \left< p \bigg| \frac{\partial}{\partial x} \bigg| x \right> \\
                        &= \frac{\hbar}{i} \frac{\partial}{\partial x} \left< p | x \right> 
                                                            \text{(operate } \rightarrow \text{)} \\
            \int \frac{d \left< p | x \right>}{\left< p | x \right>}  &= \frac{-ip}{\hbar} \int dx \qquad p^\dagger = p
\end{align}�h]�hX!  \begin{align}
\left< p | p |x \right> &= p^\dagger \left< p | x \right> \text{(operate } \leftarrow \text{)} \\
                        &=  \frac{\hbar}{i} \left< p \bigg| \frac{\partial}{\partial x} \bigg| x \right> \\
                        &= \frac{\hbar}{i} \frac{\partial}{\partial x} \left< p | x \right> 
                                                            \text{(operate } \rightarrow \text{)} \\
            \int \frac{d \left< p | x \right>}{\left< p | x \right>}  &= \frac{-ip}{\hbar} \int dx \qquad p^\dagger = p
\end{align}�����}�(hhhjn  ubah}�(h!]�jm  ah#]�h�ah%]�h']�h)]��nowrap���number�K�label��$63d33f06-a4a0-42ec-bb18-ed88bdb633ec�h�h�h�h�uh+hvhK>hh,hjE  hhh�}�h�}�jm  jd  subhw)��}�(h�(
\boxed{\left< x | p \right> = e^{ipx}}
�h]�h�(
\boxed{\left< x | p \right> = e^{ipx}}
�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�Nh�h�uh+hvhKFhh,hjE  hhubh	�	container���)��}�(hhh]�(j�  )��}�(hhh]�h	�literal_block���)��}�(hXu  import numpy as np
from ipywidgets import interact
import matplotlib.pyplot as plt
%matplotlib inline 

N = 256

def cos_propagate(x, t, k=1, w=1, signature=1):
    theta = signature * (k * x - w * t)
    return np.exp(1j*theta)

@interact(X=(-5, 5, 0.1),
          T=(-5, 5, 0.1),
          w=(-5, 5, 0.1),
          k=(-2, 2, 0.1),
          signature=[('(-1, 3)', 1), ('(1, -3)', -1)])
def plot_cos(X=0, T=0, w=1, k=1, signature=1):
    fig, ax = plt.subplots(1, 2, figsize=(10, 5))

    xs = np.linspace(-10, 10, N)
    ts = np.linspace(0, 10, N)
    
    ax[0].plot(xs, cos_propagate(x=xs, t=T, k=k, w=w, signature=signature).real)
    ax[0].set(xlabel="xs", ylabel="A", title=f"cos(kx-wt), t={T:1.4f}")
    ax[1].plot(ts, cos_propagate(x=X, t=ts, k=k, w=w, signature=signature).real)
    ax[1].set(xlabel="ts", ylabel="A", title=f"cos(kx-wt), x={X:1.4f}")
    plt.tight_layout();�h]�hXu  import numpy as np
from ipywidgets import interact
import matplotlib.pyplot as plt
%matplotlib inline 

N = 256

def cos_propagate(x, t, k=1, w=1, signature=1):
    theta = signature * (k * x - w * t)
    return np.exp(1j*theta)

@interact(X=(-5, 5, 0.1),
          T=(-5, 5, 0.1),
          w=(-5, 5, 0.1),
          k=(-2, 2, 0.1),
          signature=[('(-1, 3)', 1), ('(1, -3)', -1)])
def plot_cos(X=0, T=0, w=1, k=1, signature=1):
    fig, ax = plt.subplots(1, 2, figsize=(10, 5))

    xs = np.linspace(-10, 10, N)
    ts = np.linspace(0, 10, N)
    
    ax[0].plot(xs, cos_propagate(x=xs, t=T, k=k, w=w, signature=signature).real)
    ax[0].set(xlabel="xs", ylabel="A", title=f"cos(kx-wt), t={T:1.4f}")
    ax[1].plot(ts, cos_propagate(x=X, t=ts, k=k, w=w, signature=signature).real)
    ax[1].set(xlabel="ts", ylabel="A", title=f"cos(kx-wt), x={X:1.4f}")
    plt.tight_layout();�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]��language��ipython3�h�h�uh+j�  hh,hKLhj�  hhubah}�(h!]�h#]��
cell_input�ah%]�h']�h)]��
nb_element��cell_code_source�uh+j�  hKLhh,hj�  hhubj�  )��}�(hhh]�j�  )��}�(hhh]�(j�  )��}�(hhh]�h	�raw���)��}�(h��<script type="application/vnd.jupyter.widget-view+json">{"model_id": "13327e1ee9fe4361a64505d23242ec14", "version_major": 2, "version_minor": 0}</script>�h]�h��<script type="application/vnd.jupyter.widget-view+json">{"model_id": "13327e1ee9fe4361a64505d23242ec14", "version_major": 2, "version_minor": 0}</script>�����}�(hhhj�  hKLhh,ubah}�(h!]�h#]�h%]�h']�h)]��format��html�h�h�uh+j�  hj�  hKLhh,ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type��(application/vnd.jupyter.widget-view+json�uh+j�  hj�  hKLhh,ubj�  )��}�(hhh]�j�  )��}�(h�qinteractive(children=(FloatSlider(value=0.0, description='X', max=5.0, min=-5.0), FloatSlider(value=0.0, descr…�h]�h�qinteractive(children=(FloatSlider(value=0.0, description='X', max=5.0, min=-5.0), FloatSlider(value=0.0, descr…�����}�(hhhj�  hKLhh,ubah}�(h!]�h#]�(�output��
text_plain�eh%]�h']�h)]��language��	myst-ansi�h�h�uh+j�  hh,hKLhj�  ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type��
text/plain�uh+j�  hj�  hKLhh,ubeh}�(h!]�h#]�h%]�h']�h)]��
nb_element��mime_bundle�uh+j�  hKLhh,hj�  hhubah}�(h!]�h#]��cell_output�ah%]�h']�h)]��
nb_element��cell_code_output�uh+j�  hKLhh,hj�  hhubeh}�(h!]�h#]��cell�ah%]�h']�h)]��
nb_element��	cell_code��
cell_index�K�
exec_count�K�cell_metadata�}�uh+j�  hKLhh,hjE  hhubeh}�(h!]��a-brief-detour-on-plane-waves�ah#]�h%]��a brief detour on plane waves�ah']�h)]�uh+h
hK:hh,hhXhhubeh}�(h!]��ft-ift-conventions�ah#]�h%]��ft/ift conventions�ah']�h)]�uh+h
hKhh,hhGhhubeh}�(h!]��fourier-transforms�ah#]�(hAhBeh%]��fourier transforms�ah']�h)]�uh+h
hKhh,hhhhubh)��}�(hhh]�(h)��}�(h�Green's Functions (heuristic)�h]�h�Green’s Functions (heuristic)�����}�(hhhj.  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhKhhh,hj+  hhubhj)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�ht�-equation-30401b2f-73c3-47fb-9944-3f2dc0a4f3d3�uh+hihKjhh,hj+  hhubhw)��}�(hXV  \begin{align}
\left(\Box + m^2 \right) \psi \left(x \right) &= J\left(x \right) \\
\left(\Box + m^2 \right) G \left(x, y \right)  &= \delta \left(x - y \right) \\
\left(\Box + m^2 \right) G \left(x, y \right) J \left(y \right) &= \delta \left(x - y \right) J \left(y \right) \\
\int dy \left(\Box + m^2 \right)G \left( x, y \right) J \left(y \right) &= \int dy \delta \left(x - y \right) J \left(y \right)\\
\left(\Box + m^2 \right) \int dy G \left(x, y \right) J \left((y \right) &= J \left(x \right)\\
\rightarrow \int dy G \left(x, y \right) J \left(y \right) &= \psi \left(x \right)
\end{align}�h]�hXV  \begin{align}
\left(\Box + m^2 \right) \psi \left(x \right) &= J\left(x \right) \\
\left(\Box + m^2 \right) G \left(x, y \right)  &= \delta \left(x - y \right) \\
\left(\Box + m^2 \right) G \left(x, y \right) J \left(y \right) &= \delta \left(x - y \right) J \left(y \right) \\
\int dy \left(\Box + m^2 \right)G \left( x, y \right) J \left(y \right) &= \int dy \delta \left(x - y \right) J \left(y \right)\\
\left(\Box + m^2 \right) \int dy G \left(x, y \right) J \left((y \right) &= J \left(x \right)\\
\rightarrow \int dy G \left(x, y \right) J \left(y \right) &= \psi \left(x \right)
\end{align}�����}�(hhhjF  ubah}�(h!]�jE  ah#]�h�ah%]�h']�h)]��nowrap���number�K�label��$30401b2f-73c3-47fb-9944-3f2dc0a4f3d3�h�h�h�h�uh+hvhKjhh,hj+  hhh�}�h�}�jE  j<  subeh}�(h!]��greens-functions-heuristic�ah#]�(hAhBeh%]��green’s functions (heuristic)�ah']�h)]�uh+h
hKhhh,hhhhubeh}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���embed_images���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�wordcount-words�h	�substitution_definition���)��}�(h�182�h]�h�182�����}�(hhhj�  ubah}�(h!]�h#]�h%]��wordcount-words�ah']�h)]�uh+j�  hh,ub�wordcount-minutes�j�  )��}�(h�1�h]�h�1�����}�(hhhj�  ubah}�(h!]�h#]�h%]��wordcount-minutes�ah']�h)]�uh+j�  hh,ubu�substitution_names�}�(�wordcount-words�j�  �wordcount-minutes�j�  u�refnames�}��refids�}�(hu]�hkah�]�h�ajm  ]�jd  ajE  ]�j<  au�nameids�}�(hDh?j(  j%  j   j  j  j  j_  j\  u�	nametypes�}�(hDNj(  Nj   Nj  Nj_  Nuh!}�(h?hj%  hGj  hXhuhxh�h�j  jE  jm  jn  j\  j+  jE  jF  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�(h	�system_message���)��}�(hhh]�h.)��}�(hhh]�h�SHyperlink target "equation-7c61a234-acc7-4e39-a57f-feb97963c0b2" is not referenced.�����}�(hhhj  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type��INFO��source�h,�line�Kuh+j  ubj  )��}�(hhh]�h.)��}�(hhh]�h�SHyperlink target "equation-06fc68c1-ac75-45a6-a215-51d6a11e7822" is not referenced.�����}�(hhhj1  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj.  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type�j+  �source�h,�line�K&uh+j  ubj  )��}�(hhh]�h.)��}�(hhh]�h�SHyperlink target "equation-63d33f06-a4a0-42ec-bb18-ed88bdb633ec" is not referenced.�����}�(hhhjK  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hjH  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type�j+  �source�h,�line�K>uh+j  ubj  )��}�(hhh]�h.)��}�(hhh]�h�SHyperlink target "equation-30401b2f-73c3-47fb-9944-3f2dc0a4f3d3" is not referenced.�����}�(hhhje  ubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hjb  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type�j+  �source�h,�line�Kjuh+j  ube�transformer�N�include_log�]��
decoration�Nhh�
myst_slugs�}�ub.