---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (tunneling)
  language: python
  name: tunneling
---

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt
```

# Supersymmetric Quantum Mechanics

## References
- [Supersymmetry in Quantum Mechanics](https://worldscientific.com/worldscibooks/10.1142/4687#t=aboutBook), Cooper, World Scientific (2001)

+++

> *It is not often appreciated that if one knows the ground state wave function, then one knows the potential (up to a constant).*

\begin{gather}
H\psi_0 = \left( -\frac{\hbar^2}{2m}\partial^2_x + V_1(x)\right) \psi_0 = 0 \\
\frac{\hbar^2}{2m} \frac{\psi^{''}_0(x)}{ \psi_0(x)} = V_1(x)
\end{gather}

Example: For the ground state SHO, the wave function is $\psi_0(x) = Ae^{-x^2/2\sigma^2}$.

\begin{gather}
\psi^{''}_0(x) = \left(\frac{x^2}{\sigma^4} - \frac{1}{\sigma^2}\right)\psi_0(x) \\
\frac{x^2}{\sigma^4} = \frac{m \omega^2 x^2}{2} \qquad \sigma = \sqrt{\frac{\hbar}{m\omega}} \\
V_1(x) = \frac{m\omega^2 x^2}{2} - \frac{\hbar \omega}{2} \qquad \psi_0(x) = A e^{-x^2 m \omega / 2 \hbar}
\end{gather}

+++

The Hamiltonian for the Schrodinger equation is 

\begin{gather}
\hat H = \frac{-\hbar^2}{2m} \nabla^2 + V(x)
\end{gather}

For a quadratic potential (such as the simple harmonic oscillator) the Hamiltonian can be readily factored, but what about for a general $\hat H$?

\begin{gather}
\hat H = \frac{-\hbar^2}{2m}\nabla^2 + V(x) = A^\dagger A \\
A^\dagger = \frac{-\hbar}{\sqrt{2m}}\nabla + W(x) \qquad A \equiv \frac{\hbar}{\sqrt{2m}}\nabla + W(x)\\
\end{gather}

> what is the motivation to have a partner hamiltonian? Why consider $AA^\dagger$ at all?
> Weinberg suggests the simple answer that one might be seeking a set of operators that take bosons into fermions and vice-versa. This is not how SUSY was developed historically: see Weinberg III for details.

\begin{align}
H_1 = A^\dagger A &= \frac{-\hbar^2}{2m}\nabla^2 \underbrace{- \frac{\hbar}{\sqrt{2m}} W'(x) + W^2(x)}_{V_1(x)} \\
H_2 = A A^\dagger &= \frac{-\hbar^2}{2m}\nabla^2 \underbrace{+ \frac{\hbar}{\sqrt{2m}} W'(x) + W^2(x)}_{V_2(x)} \\
\end{align}

> What happened to the terms $\sim W(x) \nabla$? To see this, remember that these operators act on the wavefunction. 
> $ -\nabla W(x) \psi(x) + W(x) \nabla \psi(x) = \nabla \left(W(x)\psi(x)\right) + W(x)\psi'(x)$ \
> $\left[W(x), \nabla\right] = W'(x) $ \ 
> This is essentially a communtation with the momentum, missing some factors of $i$ and $\hbar$. 

Since $H_1 \psi^1_0 = A^\dagger A \psi^1_0$, we can choose $A \psi_0^1 = 0$ to enforce this. In that case, 
we can determine $W(x)$.

\begin{gather}
A\psi_0^1(x) = \frac{\hbar}{\sqrt{2m}} \nabla \psi_0^1 + W(x) \psi_0^1(x) = 0 \\
\frac{\hbar}{\sqrt{2m}} \psi_0^{'1} = - W(x) \psi_0^1(x) \\
\boxed{W(x) = - \frac{\hbar}{\sqrt{2m}} \frac{\psi_0^{'1}}{\psi_0^1(x)}}
\end{gather}

We call $W(x)$ the *superpotential* for the system, while $V_1(x)$ and $V_2(x)$ are the partner potentials. 
# Infinite Square Well

The path to supersymmetry begins by factorizing the hamiltonian. As a minimal example, we will work through the infinite square well in detail. 

The familiar solution to the well can probably be regurgitated by memory. 
\begin{gather}
\psi_n^{(1)} = \sqrt{\frac{2}{a}}\sin{\left(\frac{(n\pi x}{a}\right)} \qquad E_n = n^2\frac{\hbar^2\pi^2}{2ma^2}; \qquad n = 1, 2, \cdots
\end{gather}

\begin{gather}
\psi_n^{(1)} = \sqrt{\frac{2}{a}}\sin{\left(\frac{(n+1)\pi x}{a}\right)} \qquad E^{(1)}_n = n(n+2)\frac{\hbar^2\pi^2}{2ma^2}
\end{gather}

```{code-cell} ipython3
def psi_square_well(x, n=1, a=1):
    return np.sqrt(2 / a) * np.sin((n + 1) * np.pi * x / a)


L = 1
Nx = 201
xs = np.linspace(0, L, Nx)

fig, ax = plt.subplots()
for n in range(0, 10):
    psi = psi_square_well(x=xs, n=n, a=L)
    ax.plot(xs, 3 * n + psi)
    # print(np.sum(abs(psi)**2)* L / Nx)
```

By substitution and an easy derivative of $\sin{kx}$, the superpotential is 
\begin{gather}
W_{\text{SW}}(x) = -\frac{\hbar}{\sqrt{2m}}\frac{\pi}{a}\cot{\left(\frac{\pi x}{a}\right)}
\end{gather}

```{code-cell} ipython3
def W_square_well(x, a):
    theta = np.pi * x / a
    factor = - (hbar / np.sqrt(2 * m)) * (np.pi / a)
    return factor / np.tan(theta)

hbar = m = 1
a = 1.4
xs = np.linspace(0, a, 128)

#plt.plot(xs, W_square_well(x=xs, a=a));
```

```{code-cell} ipython3

```

```{code-cell} ipython3
a = 1.5
theta = np.pi * xs / a

psi_book = - 2 / np.sqrt(a) * np.sin(theta) * np.sin(2 * theta)
psi_paper = - 4 / np.sqrt(a) * np.cos(theta) * np.sin(theta)**2

plt.plot(xs, psi_book)
plt.plot(xs, psi_paper, "--")
```

## Partner Potentials

We will now calculate the partner potentials for the ISW.

\begin{align}
W^{'}(x) &= \frac{\hbar \pi}{\sqrt{2 m}a} \text{csc}^2\left( \frac{\pi x}{a} \right) \\
V_1(x) &= -\frac{\hbar}{\sqrt{2m}}W^{'}(x) + W^2(x) \\
       &= \frac{\hbar^2}{2m} \frac{\pi^2}{a^2} \left(- \text{csc}^2\left( \frac{\pi x}{a} \right)
    + \cot^2{\left(\frac{\pi x}{a}\right)} \right) \\
                 &= \frac{\hbar^2\pi^2}{2ma^2} \left(-1\right) \\
V_1(x) &= -\frac{\hbar^2\pi^2}{2ma^2}
\end{align}

The potential of the hamiltonian $H_1$ is the constant potential we subtracted from the traditional infinite square well so that our ground state would give an energy eigenvalue $E^{(1)}_0 = 0$.

> Since by the quotient rule, $\partial_x \left(\frac{\cos{x}}{\sin{x}}\right) = \frac{-\sin^2{x} - \cos^2{x}}{\sin^2{x}} = - \frac{1}{\sin^2{x}}$ \
> $\sin^2{x} + \cos^2{x} = 1$, so dividing the whole equation by $\sin^2{x}$ gives the useful (but never remembered (by this author)) $1 + \cot^2{x} = \text{csc}^2{x}$. \
> By adding a factor of $\text{csc}^2{x}$ to both sides of the identity and subtracting one, we obtain 
> $\cot^2{x} + \text{csc}^2{x} = 2 \text{csc}^2{x} - 1$. \
> This will be helpful when calculating $V_2(x)$.

For $V_2(x)$, we can reuse a lot of the neuron fires from above.

\begin{align}
W^{'}(x) &= \frac{\hbar \pi}{\sqrt{2 m}a} \text{csc}^2\left( \frac{\pi x}{a} \right) \\
V_2(x) &= \frac{\hbar}{\sqrt{2m}}W^{'}(x) + W^2(x) \\
       &= \frac{\hbar^2}{2m} \frac{\pi^2}{a^2} \left(\text{csc}^2\left( \frac{\pi x}{a} \right)
    + \cot^2{\left(\frac{\pi x}{a}\right)} \right) \\
V_2(x) &= \frac{\hbar^2 \pi^2}{2ma^2}  \left(2 \, \text{csc}^2\left( \frac{\pi x}{a}  \right) - 1 \right)
\end{align}

```{code-cell} ipython3
def E1_n(n, a=1):
    E_0 = hbar**2 * np.pi**2 / 2 / m / a**2
    return ((n + 1)**2 - 1) * E_0

def psi1_square_well(x, n=1, a=1):
    return np.sqrt(2 / a) * np.sin((n + 1) * np.pi * x / a)

def E2_n(n, a=1):
    return E1_n(n=n+1, a=a)

# def psi2_square_well(x, n=1, a=1):
#     return np.sqrt(2 / a) * np.sin((n + 1) * np.pi * x / a)

def psi2_square_well(x, n=1, a=1):
    
    theta = np.pi * x / a
    
    if n == 0:
        psi = - 2 * np.sqrt(2 / 3 / a) * np.sin(theta)**2
    elif n == 1:
        psi = - 2 / np.sqrt(a) * np.sin(theta) * np.sin(2 * theta)
    else:
        raise NotImplementedError(f"Only n = 0, 1 implemented. Got {n=}.")
    return psi
    
Nx = 201
xs = np.linspace(0, a, Nx)


# plots
fig, axs = plt.subplots(1, 2, figsize=(8, 5), sharey=True)

ns = np.arange(0, 4)
E1s = E1_n(ns, a=a)
E2s = E2_n(ns, a=a)

E_scale = hbar**2 * np.pi**2 / 2 / m / a**2

isw_args = dict(color="k", alpha=0.5)

ax = axs[0]
for n, E in zip(ns, E1s):
    psi = psi1_square_well(x=xs, n=n, a=a)
    ax.plot(xs, (E / E_scale) + psi)
    ax.hlines(E / E_scale, xmin=0, xmax=a, linestyle="--", **isw_args)
    # print(np.sum(abs(psi)**2)* L / Nx)
    
padding = 3
ax.vlines(0, 0, E1s[-1]/E_scale + padding, **isw_args)
ax.vlines(a, 0, E1s[-1]/E_scale + padding, **isw_args)
ax.hlines(E1s[0], xmin=0, xmax=a, **isw_args)

ax = axs[1]

theta = np.pi * xs / a

# need a range of the quickly diverging partner potential where it is finite.

for i, (n, E) in enumerate(zip([0, 1], E2s)):
    psi = psi2_square_well(x=xs, n=n, a=a)
    ax.plot(xs, (E / E_scale) + psi)
    ax.hlines(E / E_scale, xmin=0, xmax=a, linestyle="--", **isw_args)

with np.errstate(divide='ignore'):
    # This will only suppress the error message inside the loop!
    V2 = (2 / np.sin(theta)**2 - 1);
    inds = np.where(V2 <= E1s[-1] / E_scale + padding)[0]

ax.plot(xs[inds], V2[inds], linestyle="--", **isw_args)
# ax.axhline(y=0, **isw_args)
axs[0].set(title=r"$H_1$, $\psi_{1 n}$, $E_{1 n}$", xlabel="xs", ylabel="$\psi_n + E_n/E_0$")
axs[1].set(title=r"$H_2$, $\psi_{2 n}$, $E_{2 n}$", xlabel="xs")
fig.tight_layout();
```

```{code-cell} ipython3
#ys = np.linspace(-np.pi, np.pi, 256)
#plt.plot(ys, np.tan(ys + 0.01j).real)
```

```{code-cell} ipython3
def W_square_well(x, a, imag=0.0):
    # This behaves poorly numerically.
    theta = np.pi * x / a
    factor = - (hbar / np.sqrt(2 * m)) * (np.pi / a)
    return (factor / (np.tan(theta + imag*1j))).real

def _psi2_square_well(x, n=1, a=1, _imag=1e-15):
    # A |psi1_{n+1}> / E1_{n+1} = |psi2_n>
    # A = hbar / sqrt{2m} \nabla + W(x)
    E_1_n = E1_n(n=n)
    psi1_n1 = psi1_square_well(x=x, n=n+1, a=a)
    factor = hbar / np.sqrt(2 * m)
    N = len(x)
    dx = x.ptp()/ N
    # try finite difference
    ks = 2 * np.pi * np.fft.fftfreq(N, d=dx)
    dpsi = np.fft.ifft(1j * ks * np.fft.fft(psi1_n1)).real
    
    return factor * dpsi #+ W_square_well(x=x, a=a, imag=_imag)

ys = xs[1:-1]
psi2_0 = _psi2_square_well(x=ys, n=0, a=a, _imag=0.0)

plt.plot(ys, psi2_0.real)
plt.plot(ys, psi2_square_well(x=ys, n=0, a=a))
```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

# [wip] rosen-morse

\begin{gather}
V(x) = -12 \ \text{sech}^2{x/d}
\end{gather}

```{code-cell} ipython3
Lx = 10
xs = np.linspace(-Lx / 2, Lx / 2, 201)

def V_rosen_morse(x, d=1):
    return -12 / (np.cosh(x / d))**2


fig, ax = plt.subplots()
ax1 = plt.twinx(ax)
ax.plot(xs, V_rosen_morse(xs), label="Rosen-Morse");
ax.plot(xs, 1/np.cosh(xs), "--", label="sech(x)", alpha=0.5);
ax1.plot(xs, np.cosh(xs), "--", label="cosh(x)", alpha=0.5);
ax.plot(xs, -12 * np.exp(-xs**2/2), "--", label="gaussian", alpha=0.5)
ax.legend(loc=(0.0,0.0));
ax1.legend(loc=(0.8,0.05));
fig.tight_layout();
```
