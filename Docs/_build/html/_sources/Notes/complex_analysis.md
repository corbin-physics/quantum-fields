---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (tunneling)
  language: python
  name: tunneling
---

# Contour Integration
---
Essential Math

+++

# Jacobian

Map between coordinates.

$$
J = \left\lvert\frac{\partial(x, y)}{\partial(r, \theta)}\right\rvert = 
\begin{vmatrix}
\frac{\partial x}{\partial r} & \frac{\partial x}{\partial \theta} \\
\frac{\partial y}{\partial r} & \frac{\partial y}{\partial \theta}
\end{vmatrix}
\qquad 
J^{-1} = \left\lvert\frac{\partial(r, \theta)}{\partial(x, y)}\right\rvert = 
\begin{vmatrix}
\frac{\partial r}{\partial x} & \frac{\partial r}{\partial y} \\
\frac{\partial \theta}{\partial x} & \frac{\partial \theta}{\partial y}
\end{vmatrix}
$$

example: polar coordinates

$$
x = r \cos\theta \qquad y = r \sin \theta \\
x^2 + y^2 = r^2 \qquad \sin^2\theta + \cos^2 \theta = 1 \\
\theta = \arctan\left(\frac{y}{x}\right)
$$

$$
dx dy = J dr d\theta \\
\frac{\partial x}{\partial r} = \cos \theta \qquad \frac{\partial x}{\partial \theta} = - r \sin\theta \\
\frac{\partial y}{\partial r} = \sin \theta \qquad \frac{\partial y}{\partial \theta} = r \cos\theta \\
J = \begin{vmatrix}
\cos \theta & - r \sin\theta \\
r \cos \theta & r \cos \theta
\end{vmatrix}
= r \left(\sin^2 \theta + \cos^2 \theta \right) = r
$$

$$
\frac{\partial r}{\partial x} = \partial_x (x^2 + y^2)^2 = \frac{2x}{2r} = \frac{x}{r} = \cos \theta \\
\frac{\partial r}{\partial y} = \frac{y}{r} = \sin\theta \\
$$

$$
\frac{\partial \theta}{\partial x} = \frac{-y}{r^2}\\
$$

Proof:
```{toggle}

$$
\partial_x \left( \cos \theta\right) = - \sin \theta \qquad \theta_{,x} \partial_x \left( \frac{x}{r} \right) \\
= \frac{1}{r^2}\left(r - x \frac{\partial r}{\partial x}\right) \\
= \frac{1}{r^2}\left(r - \frac{x^2}{r} \right) \\
= \frac{y^2}{r^3} \\
\theta_{,x} = -\frac{y^2}{r^3 \sin\theta} = \frac{-y}{r^2}
$$

Likewise, 

$$
\theta_{,y} = \frac{x^2}{r^3 \cos\theta} = \frac{x}{r^2}
$$
```

So $J^{-1} = \frac{1}{r}$

$$
dx dy = J dr d\theta = r dr d\theta
$$

Jacobian maps coordinate transformations.

---

Another example: particle dynamics

$$
J = \left\lvert\frac{\partial\left( x(t_0), p(x_0) \right)}{\partial \left( x(t), p(t) \right)}\right\rvert
$$

$$
N = \int dx_0 dp_0 = \int J dx dp \\
= \int dx \lvert \psi(x) \rvert^2
$$

+++

# Derivatives, Cauchy-Riemann

For a function $f(x) : \mathbb{R}$, the derivative is defined to converge to the same value regardless of the direction of approach.

$$
\frac{d f(x)}{dx} = \lim_{\epsilon \rightarrow 0} \frac{f(x+\epsilon) - f(x)}{\epsilon} 
= \lim_{\epsilon \rightarrow 0} \frac{f(x) - f(x-\epsilon)}{\epsilon}
$$

```{code-cell} ipython3
# show the derivative limits graphically?
```

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt

thetas = np.linspace(0, np.pi, 120)
zs = np.exp(1j * thetas)

def f(z, a=2):
    return z.real + 1j * a * z.imag

plt.plot(zs.real, zs.imag)
plt.plot(f(zs).real, f(zs).imag);
```

$$
(x + iy)(x + iy) = x^2 - y^2 + 2ixy
$$

```{code-cell} ipython3
from mmfutils.plot import imcontourf

L = 10
x = np.linspace(-L, L, 100)
y = np.linspace(-L, L, 101)

X, Y = np.meshgrid(x, y, indexing="ij")
Z = X + 1j * Y

def plot_complex_contour(f, Z):
    x = Z[:, 0].real
    y = Z[0, :].imag
    
    fig, axs = plt.subplots(1, 2, figsize=(10, 5))
    fig.sca(axs[0])
    imcontourf(x, y, f(Z).real)
    axs[0].set(title="real part", xlabel="x", ylabel="y")
    fig.sca(axs[1])
    imcontourf(x, y, f(Z).imag);
    axs[1].set(title="imag part", xlabel="x", ylabel="y")
    if hasattr(f, "__name__"):
        fig.suptitle(f"f(z)={f.__name__}(z)")
    fig.tight_layout()
    
    
```

```{code-cell} ipython3
plot_complex_contour(f=np.square, Z=Z)
```

```{code-cell} ipython3
plot_complex_contour(f=abs, Z=Z)
```

```{code-cell} ipython3
plot_complex_contour(f=np.conj, Z=Z)
```

```{code-cell} ipython3
plot_complex_contour(f=np.log, Z=Z)
```

$$
z = re^{i\theta} = re^{i(\theta + 2\pi n)}; n = 0, 1, 2, \cdots \\
\ln{z} = \ln{r} + i\theta + 2 \pi i n \qquad \theta = \arctan{\left(\frac{y}{x}\right)}
$$

```{code-cell} ipython3
(12 / 2 * 125) / 60
7722604 / 1024 / 1024
```

## Cauchy-Riemann Conditions

The derivative in calculus is introduced as the limit 

$$
f'(x) = \lim_{h\rightarrow 0} \frac{f(x+h) - f(x)}{h}
$$

for a smooth (continuous) function $f(x)$. The limit must be the same regardless of the direction of approach; if h is a small negative number increasing until it reaches zero, the limit must be the same as the one above.

$$
f'(x) = \lim_{-h\rightarrow 0} \frac{f(x-h) - f(x)}{h}
$$


The complex derivative works the same way. We can write

$$
f'(z) = \lim_{h\rightarrow 0} \frac{f(z+h) - f(z)}{h}
$$

just as before, only now we allow $h$ to take on complex values in $\mathbb{C}$. 

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt

xs = np.linspace(0, 3*np.pi)

def f(x, h=0.001, d=0):
    if d == 0:
        return np.sin(x)
    elif d == 1:
        dx = f(x + h, d=0) - f(x, d=0)
        return dx / h
    else:
        raise ValueError(f"{d=} not supported.")

plt.plot(xs, np.sin(xs), label="f(x)")
plt.plot(xs, np.cos(xs), "--", alpha=0.5, label="cos(x)")
for h in [10, 1, 0.1, 0.01, -0.01, -0.1, -1]:
    plt.plot(xs, f(xs, d=1, h=h), label=f"f'(x), {h=}")
plt.legend()
plt.tight_layout();
```

```{code-cell} ipython3
#plt.plot(xs, np.sin(xs), label="f(x)")
#plt.plot(xs, np.cos(xs), "--", alpha=0.5, label="cos(x)")
errs_p = []
errs_m = []

xs = 5 / np.pi 

for h in np.logspace(0, -14, 30):
    errs_p.append(abs(f(xs, d=1, h=h) - np.cos(xs)).max())
    #plt.semilogy(xs, rerr, label=f"err, {h=}")
    h = -h
    errs_m.append(abs(f(xs, d=1, h=h) - np.cos(xs)).max())
    #plt.semilogy(xs, rerr, label=f"err, {h=}")

plt.semilogy(errs_p, ".-", label="+h")
plt.semilogy(errs_m, ".-", label="-h")
plt.legend()
plt.tight_layout();
```

Whatever we take $h$ to be, it must approach $0$ and give the same limit. We can therefore consider cases where we approach from the pure real axis, or the pure imaginary axis. If the function has a derivative, both of the derivatives should match.

Rewrite $f(z)$ in terms of a pure real part and a pure imaginary part, each themselves functions of $z = x + iy$. 

$$
f(z) = f(x, y) = u(x, y) + i v(x, y)
$$

Limits for $h = \Delta x$ and for $h = i\Delta y$ should be equivalent. 

\begin{align}
\frac{d f(x, y)}{dx} &= u_{,x} + iv_{,x} \\
\frac{d f(x, y)}{i dy} &= -i u_{,y} + v_{,y} \\
\end{align}

$\frac{d f(x, y)}{dx} = \frac{d f(x, y)}{idy}$, so we can equate the real and imaginary parts of the expressions above:

\begin{align}
\frac{\partial u(x, y)}{\partial x} &= \frac{\partial v(x, y)}{\partial y} \\
\frac{\partial u(x, y)}{\partial y} &= - \frac{\partial v(x, y)}{\partial x} \\
\end{align}

Or, more compactly,

$$
u_{,x} = v_{,y} \qquad u_{,y} = -v_{,x}
$$

The two above expressions are the [Cauchy-Riemann Equations](https://en.wikipedia.org/wiki/Cauchy%E2%80%93Riemann_equations). If they are satisfied, the function $f$ is analytic and differentiable. 

The Cauchy-Riemann conditions also imply that $u$ and $v$ are [harmonic functions](https://en.wikipedia.org/wiki/Harmonic_function). To see this, differentiate each equation by $x$ or by $y$ one time further.

\begin{align}
u_{,xx} &= v_{,xy} \qquad u_{xy} = - v_{xx} \\
u_{,xy} &= v_{,yy} \qquad u_{yy} = - v_{xy} \\
\end{align}

By substituting the above equations into each other and moving both parts to the left hand side, the functions $u$ and $v$ are found to obey Laplace's Equation:

$$
u_{,xx} + u_{,yy} = \nabla^2 u(x, y) = 0 \\
v_{,xx} + v_{,yy} = \nabla^2 v(x, y) = 0 \\
$$

+++

## Example
When we know either the function $u$ or $v$ and we know that they are analytic, we can obtain the other function, called the *conjugate harmonic function*. From $u(x,y) = \sin{x}\cosh{y}$, deduce $v(x, y)$.

---
$$
u_{,x} = \cos{x}\cosh{y} \qquad u_{,xx} = -\sin{x}\cosh{y} = -u \\
u_{,y} = \sin{x}\sinh{y} \qquad u_{,yy} = \sin{x}\cosh{y} = u \\
$$

Thus $\nabla^2 u = 0$, as desired. From the Cauchy-Riemann equations, $u_{,x} = v_{,y}$ and $u_{,y}=-v_{,x}$. Integrating,

$$
v(x, y) = \int \cos{x}\cosh{y} dy = \cos{x}\sinh{y} + g(x) + c \\
v(x, y) = - \int \sin{x}\sinh{y} dx = \cos{x}\sinh{y} + h(y) + c \\
$$

Both lines must be the same, so $g(x) = h(y) = 0$.

$$
v(x, y) = \cos{x} \sinh{y} + c
$$

```{code-cell} ipython3
def u(x, y):
    return np.sin(x) * np.cosh(y)

def v(x, y):
    return np.cos(x) * np.sinh(y)

N = 1000
L = 3
xs = np.linspace(-L, L, N)
ys = np.linspace(-L, L, N+1)
XY = np.meshgrid(xs, ys, indexing="ij")

plt.contour(xs, ys, u(*XY).T, levels=20, cmap="Blues")
plt.contour(xs, ys, v(*XY).T, levels=20, cmap="Reds");
```

At points of intersection, the lines of $u$ are always perpendicular to the lines of $v$.

+++

## Example 2

$$
u(x, y) = x^2 - y^2
$$

$$
u_{,x} = 2x \qquad v(x,y) = \int 2x dy = 2xy + f(x) + c \\
u_{,y} = -2y \qquad v(x,y) = \int 2y dx = 2xy + f(y) + c \\
v(x, y) = 2xy + c \\
f(x, y) = x^2 - y^2 + 2xyi \qquad c = 0
$$

```{code-cell} ipython3
def u(x, y):
    return x**2 - y**2

def v(x, y):
    return 2 * x * y

N = 1000
L = 5
xs = np.linspace(-L, L, N)
ys = np.linspace(-L, L, N+1)
XY = np.meshgrid(xs, ys, indexing="ij")

plt.contour(xs, ys, u(*XY).T, levels=20, cmap="Blues")
plt.contour(xs, ys, v(*XY).T, levels=20, cmap="Reds");
```

```{code-cell} ipython3
def u(x, y):
    return np.exp(x) * np.cos(y)

def v(x, y):
    return np.exp(x) * np.sin(y)

N = 1000
L = 2
xs = np.linspace(-1, L, N)
ys = np.linspace(-1, 2*L, N+1)
XY = np.meshgrid(xs, ys, indexing="ij")

plt.contour(xs, ys, u(*XY).T, levels=20, cmap="Blues")
plt.contour(xs, ys, v(*XY).T, levels=20, cmap="Reds");
```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

## Triangle inequality

For two complex numbers $z$ and $w$, 

$$
|z + w| \leq |z| + |w|
$$

Prove this by squaring both sides.

Because $z$ is a vector on some unit circle, its maximum in the real axis will always be when its complex part is zero. Imagine projecting down a point on the unit circle from anywhere else; its x-component (real) will be less than one. Therefore,
$$
- |z| \leq \mathrm{Re}(z) \leq |z|
$$

$$
|z + w|^2 = |z|^2 + |w|^2 + 2 \mathrm{Re}(|z||w|)
$$

since the absolute value has already taken the real projection of the cross term. This could have discarded "information", however, so the inequality may be invoked.

\begin{align}
|z + w|^2 &= |z|^2 + |w|^2 + 2 \mathrm{Re}(|z||w|) \\
          &\leq |z|^2 + |w|^2 + 2 |z||w| \\
          &\leq \left(|z| + |w|\right)^2
\end{align}

+++

## Multivalued Roots

**Nth roots of unity**

Complex numbers $z=e^{i\theta}$ on a unit circle have a U(1) symmetry. After rotating $2\pi$, they come back to their original point. Discrete rotations form a group; rotating $2\pi/n$ degrees $n$ times will bring you back to the starting point. The n points can be considered the group elements, with angle addition as the group multiplication. 

Raising $z$ to a power increases the angle rotated. For example, if $z = e^{i\pi} = -1$, z is the point on the left of the unit circle. Squaring this gives $z^2 = e^{2\pi}=1$, and the angle has rotated by z twice. *Raising to a power increases the angle.* This suggests that taking a root will contract the angle in the same way, which is true. There's an extra complication now, however, because the roots have become multivalued.

```{code-cell} ipython3
eps = np.finfo(float).eps

def complex_root(z, n=2):
    r = abs(z)
    theta = np.arctan2(z.imag, z.real)
        
    arg = (theta + np.linspace(0, 2 * np.pi * (n-1), n)) / n
    
    roots = r**(1/n) * np.exp(1j * arg)
    return roots

def plot_roots(z, n, save=False, title=None):
        
    thetas = np.linspace(0, 2 * np.pi, 100)
    unit_circle = np.exp(1j * thetas)

    fig, ax = plt.subplots(figsize=(5,5))
    ax.set(aspect=1, title=f"Complex {n} root of {z=}")

    ax.grid(True, which="both")
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')

    ax.plot(unit_circle.real, unit_circle.imag, alpha=0.5)
    
    roots = complex_root(z=z, n=n)
    
    ax.plot(roots.real, roots.imag, ".", color="k", markersize=10.0)
    plt.tight_layout();
    if save is True:
        if title is None:
            title = f"roots_{n=}.pdf"
        fig.savefig(title)
        
    # hack to get roots out of this
    return roots
```

$$
z = e^{i\pi} = e^{i (\pi + 2\pi)} = -1
$$

We typically treat the middle two terms above as equivalent, since wrapping the phase by $2\pi$ is a full cycle and we end up back at the beginning. Under the square root scaling, this only becomes a half-wrap! So $\sqrt{z}$ in general has two roots:


\begin{align}
\sqrt{z} &= e^{i\pi/2} \\
         &= \cos{\pi/2} + i \sin{\pi/2} \\
         &= i \\
         &= e^{i (\pi/2 + \pi)} = e^{i 3\pi/2} \\
         &= \cos{3\pi/2} + i \sin{3\pi/2} \\
         &= -i
\end{align}

$$
\sqrt{z} = \pm i
$$

Adding $4\pi$ to the initial phase in $z$ will give $\sqrt{z} = i$ again, so there are only two square roots for z... which is a pretty generalization of traditional arithmetic, where the square root of real numbers had two values.

```{code-cell} ipython3
plot_roots(z=-1, n=2);
```

For the cube root, there are now three roots, unlike in the real numbers only case (though in that case, we had three roots, with degeneracies.) Take the same case again, where $z = e^{i\pi}$.


\begin{align}
\sqrt{z} &= e^{i\pi/3} \\
         &= \cos{\pi/3} + i \sin{\pi/3} \\
         &= \frac{1 + \sqrt{3}i}{2} \\
         &= e^{i (\pi/3 + 2\pi/3)} = e^{i \pi} \\
         &= -i \\
         &= e^{i(\pi/3 + 4\pi/3)} \\
         &= e^{i5\pi/3} \\
         &= \cos{5\pi/3} + i \sin{5\pi/3} \\
         &= \frac{1 - \sqrt{3}i}{2} \\
\end{align}

```{code-cell} ipython3
roots = plot_roots(z=-1, n=3)
```

```{code-cell} ipython3
roots = plot_roots(z=-40, n=3)
```

## Trig Identities
## Dot and Cross Products

The product of a complex number and the complex conjugate of another gives you the vector dot and cross products, neatly packaged into one operation.

\begin{equation*}
A = r_a e^{i\phi_a} \qquad B = r_b e^{i\phi_b} \qquad \theta \equiv \phi_b - \phi_a \\
\end{equation*}

\begin{align*}
\bar{A}B &= r_a r_b e^{i(\phi_b - \phi_a)} = r_a r_b e^{i(\theta)} \\
         &= r_a r_b \left(\cos{\theta} + i \sin{\theta}\right) \\
\end{align*}

\begin{align}
\mathbf{Re}(\bar{A}B) &= r_a r_b \cos{\theta} = \mathbf{A} \cdot \mathbf{B} \\
\mathbf{Im}(\bar{A}B) &= r_a r_b \sin{\theta} = \mathbf{A} \times \mathbf{B} \\
\end{align}

The dot product gives the product of $A$ with the value of $B$ projected onto $A$, and the cross product is the area of the parallelogram created by the two vectors. 


+++

For a simply-connected region, one where the contour can be reduced to a point without entering a pole,

$$
\oint_C d(z) dz = 0
$$

which can be proven with the complex Stokes' theorem.

Integrals are by convention taken to be in the counter-clockwise direction. The easy way to remember this is that the positive direction is the direction a phasor on an [Argand diagram](https://mathworld.wolfram.com/ArgandDiagram.html) (complex plane).

## Example : $z^n$
Integrating $z^n$ over a circular contour (i.e., $dr = 0$) with $z = r e^{i\theta}$, 

\begin{gather}
z = re^{i\theta} \qquad dz = i r e^{i \theta} d\theta \\
\oint_C z^n dz = i \oint_C r^n e^{i\theta n } r e^{i \theta} d\theta \\
= i r^{n+1} \oint_C e^{i \theta(n + 1)} d\theta \\
\end{gather}

If $n \neq -1$, the integral proceeds simply.

$$
= \frac{r^{n+1}}{n+1} e^{i\theta(n+1)}\big\rvert_0^{2\pi} = 0
$$

If $ n = -1$, we get a different result.

\begin{align}
\oint_C \frac{dz}{z} &= i \oint_C \frac{r e^{i\theta}}{r e^{i \theta}} d\theta \\
                     &= i \int_0^{2\pi} = 2\pi i
\end{align}

$$
\oint_C z^n dz = \left\{ \begin{array}{cl}
0 & : n \neq -1 \\
2\pi i & : n = 1
\end{array}\right.
$$

---

+++

## Cauchy's Integral Theorem

Cauchy's integral theorem is as follows:

$$
\boxed{\frac{1}{2\pi i} \oint_C \frac{f(z)}{z-z_0} dz = f(z_0)}
$$

As a simple example, try this by shifting $z = r e^{i\theta} + z_0$.

\begin{align}
\oint_C \frac{f(z)}{z - z_0} dz &= \int_0^{2\pi} \frac{f(re^{i\theta} + z_0)}{re^{i\theta} + z_0 - z_0} i r e^i\theta d\theta \\
                              I &= i \int_0^{2\pi} f(re^{i\theta} + z_0) d\theta
\end{align}

We're free to choose a contour of any radius here as long as it has the same endpoints in the integration variable $\theta$, so consider the limit $r \rightarrow 0$.

$$
I = \lim_{r\rightarrow 0} i \int_0^{2\pi} f(re^{i\theta} + z_0) d\theta = 2 \pi i f(z_0)
$$

## Derivative Rule
By taking subsequent derivatives of the integral theorem, useful rules for derivatives and higher order poles can be obtained.

\begin{align}
f(z_0) &= \frac{1}{2\pi i} \oint_C \frac{f(z)}{z-z_0} dz \\
f'(z_0) &= \frac{1}{2\pi i} 1! \oint_C \frac{f(z)}{(z-z_0)^2} dz \\
f''(z_0) &= \frac{1}{2\pi i} 2! \oint_C \frac{f(z)}{(z-z_0)^3} dz \\
\end{align}

$$
\boxed{f^{(n)}(z_0) = \frac{1}{2\pi i} n! \oint_C \frac{f(z)}{(z-z_0)^{n+1}} dz}
$$

+++

## Laurent Expansion
The Laurent expansion combined with Cauchy's integral theorem combine to give the residue theorem.

## Cauchy Principal Value

Over intervals where an integrand may be non-analytic, a contour integral is often well defined in terms of its [principal value](https://en.wikipedia.org/wiki/Cauchy_principal_value), which restricts the integral to the well defined region.

+++


## Example 1

*Arfken 11.4.2.*

$$
\oint_\mathbb{C} \frac{dz}{z^2 -1}
$$

Closed by a contour fulfiling the circular parameterization $c : |z-1| = 1$. This is a circle of radius 1, centered at $1 + 0j$.

```{code-cell} ipython3
thetas = np.linspace(0, 2*np.pi)
c = 1 * np.exp(1j * thetas) + 1

fig, ax = plt.subplots(figsize=(3,3))
ax.plot(c.real, c.imag);
ax.grid("both")
ax.scatter([-1, 1], [0, 0], color="k", s=5)
ax.set(aspect=1);
```

The contour at $z=-1$ is not closed, so the answer is simply the residue from the remaining contour at $z=1$.

$$
\oint_\mathbb{C} \frac{dz}{z^2 -1} = \oint_\mathbb{C} \frac{dz}{(z+1)(z-1)} \\
 = \frac{2 \pi i}{(1+1)}
 = i\pi
$$

## Example 2

*Arfken 11.4.6.*

$$
\oint_\mathbb{C} \frac{e^{iz}}{z^3} dz
$$

Square contour, centered at 0, sides length a>0.

This is a third order pole at $z=0$, so we need to take two derivatives of the residue.

\begin{align}
\oint_\mathbb{C} \frac{e^{iz}}{z^3} dz &= 2 \pi i \left( \frac{1}{2!} \frac{\partial^2}{\partial z^2} e^{iz}\right) \bigg|_{z=0} \\
    &= \pi i (i^2) e^{i(0)} \\
    &= -i\pi
\end{align}

## Example 3

$$
\frac{1}{2\pi i} \oint_\mathbb{C} \frac{z}{(z-1)^3(z+2)^2} dz \\
$$
There are two poles: $z=-2, z=1$.

```{code-cell} ipython3
thetas = np.linspace(0, 2*np.pi)
c = 1.5 * np.exp(1j * thetas)

fig, ax = plt.subplots(figsize=(3,3))
ax.plot(c.real, c.imag);
ax.grid("both")
ax.scatter([-2, 1], [0, 0], color="k", s=5)
ax.set(aspect=1);
```

Closing with a circle centered at the origin with radius $3/2$, only the third order pole will be enclosed. Using the derivative rule, the residue of this integral will be

\begin{align}
\frac{1}{2!} \frac{\partial^2}{\partial z^2} \left[ \frac{z}{(z+2)^2} \right] \bigg|_{z=1} &=
    \frac{1}{2!} \frac{\partial}{\partial z} \left[ \frac{1}{(z+2)^2} + \frac{-2z}{(z+2)^3} \right] \bigg|_{z=1} \\
    &= \frac{1}{2!} \left[ \frac{-2}{(z+2)^3} + \frac{-2}{(z+2)^3} + \frac{6z}{(z+2)^4} \right] \bigg|_{z=1}\\
    &= \left[ \frac{-1}{3^3} + \frac{-1}{(3^3} + \frac{3}{3^4} \right] \\
    &= \frac{-1}{27} \\
\end{align}

```{code-cell} ipython3

```
