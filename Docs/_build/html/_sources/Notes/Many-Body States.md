---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

```{code-cell} ipython3

```
