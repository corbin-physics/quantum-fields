<!-- Phys  - Quantum Fields
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->


<!-- Include ../README.md
     If you would like to use the contents of your top-level README.md file here, then
     you can literally include it here with the following:

```{include} ../README.md
```

    Note that this may will break `sphinx-autobuild` (`make doc-server`) which will not rebuild
    this index file when ../README.md changes.  See the note at the bottom of the file
    if you want to do this while using sphinx-autobuild.
-->

```{include} ../README.md
```

```{toctree}
---
maxdepth: 2
caption: "Contents:"
titlesonly:
hidden:
glob:
---
References
```

```{toctree}
---
maxdepth: 2
caption: "Preliminaries"
titlesonly:
hidden:
glob:
---
Preliminaries/*
```

```{toctree}
---
maxdepth: 2
caption: "Notes [all WIP]"
titlesonly:
hidden:
glob:
---
Notes/*
```

```{toctree}
---
maxdepth: 2
caption: "Miscellaneous"
hidden:
---
Demonstration
README.md <../README>
```

<!-- If you opt to literally include files like ../README.md and would like to be able
     to take advantage of `sphinx-autobuild` (`make doc-server`), then you must make
     sure that you pass the name of any of these files to `sphinx-autobuild` in the
     `Makefile` so that those files will be regenerated.  We do this already for
     `index.md` but leave this note in case you want to do this elsewhere.

     Alternatively, you can include them separately and view these directly when editing.
     We do not include this extra toc when we build on RTD or on CoCalc.  We do this
     using the `sphinx.ext.ifconfig extension`:

     https://www.sphinx-doc.org/en/master/usage/extensions/ifconfig.html

```{eval-rst}
.. ifconfig:: not on_rtd and not on_cocalc

   .. toctree::
      :maxdepth: 0
      :caption: Top-level Files:
      :titlesonly:
      :hidden:

      README.md <../README>
      InstructorNotes.md <../InstructorNotes>
```
-->
