---
jupytext:
  encoding: '# -*- coding: utf-8 -*-'
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (nuclear)
  language: python
  name: nuclear
---

```{code-cell} ipython3
# fmt: off
%matplotlib inline
#try: # Atempt to install my mmf_setup package.  May need kernel restart.
#    import mmf_setup
#except:
#    import sys
   !{sys.executable} -m pip install mmf_setup
import mmf_setup;mmf_setup.nbinit()
import numpy as np, matplotlib.pyplot as plt
# fmt: on
```

# Fourier Basis

+++

Here we present some more details about the Fourier bases.  I use the typical physics normalization here:

* Functions on $x \in (-\infty, \infty)$:

  $$
    \braket{f|g} = \int_{-\infty}^{\infty}f^*(x)g(x)\d{x}, \qquad
    \op{1} = \int_{-\infty}^{\infty}\ket{x}\bra{x}\d{x} = \int_{-\infty}^{\infty}\ket{k}\bra{k}\frac{\d{k}}{2\pi},\qquad
    \braket{x|k} = e^{\I k x}.
  $$

* Periodic functions on $x \in [0, L)$:

  $$
    \braket{f|g} = \int_{0}^{L}f^*(x)g(x)\d{x}, \qquad
    \op{1} = \int_{0}^{L}\ket{x}\bra{x}\d{x} = \frac{1}{L}\sum_{n}\ket{k_n}\bra{k_n},\qquad
    k_n = \left.\frac{2\pi n}{L}\right|_{n=0}^{N-1}, \qquad
    \braket{x|k_n} = e^{\I k_n x}.
  $$

* Periodic functions on a lattice $x \in \{x_n = n\d{x}\}$ for $n \in \{0, 1, \cdots, N-1\}$:

  $$
    \braket{f|g} = \sum_{n=0}^{N-1} f^*(x_n) g(x_n), \qquad
    \op{1} = \overbrace{\frac{L}{N}}^{\d{x}}\sum_{n}\ket{x_n}\bra{x_n} 
           = \overbrace{\frac{1}{L}}^{\d{k}/2\pi}\sum_{n}\ket{k_n}\bra{k_n},\qquad
    x_n = \left.\frac{L}{N}\right|_{N=0}^{N-1}, \qquad
    k_n = \left.\frac{2\pi n}{L}\right|_{n=0}^{N-1}, \qquad
    \braket{x_m|k_n} = e^{\I k_n x_m}.
  $$

  Note that these can be expressed without any reference to $L$ or $\d{x}$ if we just regard $f_n = \braket{x_n|f} = f(x_n)$ and $\tilde{f}_{m} = \braket{k_m|f}$ as a collection of numbers.  It is on these that the fast Fourier transform FFT acts:

  $$
    \braket{f|g} = \sum_{n=0}^{N-1} f_n^* g_n, \qquad
    \op{1} = \overbrace{\frac{L}{N}}^{\d{x}}\sum_{n}\ket{x_n}\bra{x_n} 
           = \overbrace{\frac{1}{L}}^{\d{k}/2\pi}\sum_{n}\ket{k_n}\bra{k_n},\qquad
    \braket{x_m|k_n} = e^{2\pi\I n m/N} .
  $$
  

+++

## Exercise
The FFT algorithm simply changes bases from the position basis $\ket{x}$ to the momentum basis $\ket{k}$ and back:

$$
  \DeclareMathOperator{\FFT}{FFT}
  \DeclareMathOperator{\IFFT}{IFFT}
  \FFT(\vect{f}) = \mat{F}\cdot \vect{f}, \qquad
  \IFFT\big(\FFT(\vect{f})\big) = \mat{F}^{-1}\cdot\mat{F}\cdot \vect{f} = \vect{f}.
$$

Explicitly compute the transformation matrix $\mat{F}$ and check that this is what the code does.
* Pay particular attention to the normalization.  *($\mat{F}^{-1} = \mat{F}^\dagger/N$ for most implementations.)*
* Note also that if you use a centered lattice $x_n = n\d{x} - L/2$, then you will need to include another phase factor in $\mat{F}$.  Explicitly check this.

```{code-cell} ipython3
N = 256
L = 10.0
dx = L/N
x = np.arange(N)*dx
k = 2*np.pi * np.fft.fftfreq(N, dx)
F = np.exp(-1j*k[:, np.newaxis]*x[np.newaxis, :])

np.random.seed(3)
f = (np.random.random(2*N) - 0.5).view(dtype=complex)
assert np.allclose(np.fft.fft(f), F @ f)
assert np.allclose(np.fft.ifft(f), F.T.conj() @ f / N)
```

```{code-cell} ipython3
import time
Ns1 = np.arange(2, 1024)
Ns2 = 2**np.arange(1, 11)
Ns3 = [_N for _N in sorted(set((2**np.arange(1, 12)[:, None]*3**np.arange(1, 12)[None, :]).ravel()))
      if _N < 1025]
Nrun = 10
Nsamp = 20

Nss = [Ns1, Ns2, Ns3]
tss = []
for Ns in Nss:
    ts = []
    for N in Ns:
        f = f = (np.random.random(2*N) - 0.5).view(dtype=complex)
        _ts = []
        for _n in range(Nsamp):
            tic = time.time()
            for _n in range(Nrun):
                np.fft.fft(f)
            t = (time.time() - tic)/Nrun
            _ts.append(t)
        ts.append(min(_ts))
    tss.append((Ns, ts))

for Ns, ts in tss:
    plt.semilogy(Ns, ts);

plt.xlabel('Size of array')
plt.ylabel('Time/FFT (s)')
#plt.plot(Ns, Ns*np.log(Ns) / (Ns[-1]*np.log(Ns[-1])) * ts[-1], '--')
#plt.plot(Ns, np.array(Ns)**2 / (Ns[-1]**2) * ts[-1], '--')
```

# Multiplication

```{code-cell} ipython3
res = np.fft.ifft(
    np.fft.fft([0,0,0,0,1000,200,30,4])
    * np.fft.fft([0,0,0,0,0,300,20,1])).real.round(0)
sum(res), 1234*321
```

# PSD

```{code-cell} ipython3
1234*234, 29*1000+20*100+11*10+4*1
```

Sample demonstration how to use FFT to extract a single with the Power-Spectral Density (PSD).

```{code-cell} ipython3
N = 1024
T = 1.0
dt = T/N
t = np.arange(N)*dt
f = 20.1
y = np.sin(2*np.pi * f*t)
np.random.seed(3)
dy = np.random.normal(size=N)
fs = np.fft.fftfreq(N, d=dt)

fig, axs = plt.subplots(1,2, figsize=(20,5))
axs[0].plot(t, y+dy)
axs[0].plot(t, y)
axs[0].set(xlabel='t', ylabel='y')

axs[1].psd(y+dy, Fs=N/T);
axs[1].psd(y, Fs=N/T);
axs[1].axvline(f, c='y')
```

```{code-cell} ipython3
fig, axs = plt.subplots(2, 1, figsize=(20,10))
axs[0].semilogy(np.fft.fftshift(fs), abs(np.fft.fftshift(np.fft.fft(y+dy)))**2)
axs[0].axvline([-f], c='y')
axs[0].axvline([f], c='y')

axs[1].semilogy(np.fft.fftshift(fs), abs(np.fft.fftshift(np.fft.fft(y+dy)))**2)
axs[1].axvline([-f], c='y')
axs[1].axvline([f], c='y')

axs[1].set(xlim=(-30,30))
```

# Time Dependent Schrödinger Equation

+++

## Particle in a Box

+++

Here we solve the problem of a particle in a box as discussed in class.  The Schrödinger equation has the form

$$
  \I \hbar \dot{\psi}(x, t) = \frac{-\hbar^2\psi''(x, t)}{2m} + V(x, t)\psi(x, t).
$$

A particle in a box has the following potential:

$$
  V(x, t) = \begin{cases}
    0 & 0<x<L,\\
    \infty & \text{otherwise}.
  \end{cases}
$$

The eigenstates are:

$$
  \psi_n(x) = \braket{x|n} = \sqrt{\frac{2}{L}}\sin(k_n x), \qquad
  k_n = \left.\frac{\pi n}{L}\right|_{n=1}^{\infty}, \qquad
  E_n = \frac{\hbar^2k_n^2}{2m}.
$$

These satisfy the boundary conditions that $\psi(0)= \psi(L) = 0$ and the SEQ in the interior.

+++

### Sudden Expansion

+++

In class, we considered the problem of what happens to the ground state $\ket{1}$ when the box is suddenly changed to have length $2L$.  To deal with this, we first express the eigenstates $\ket{n'}$ of box of length $2L$:

$$
  \braket{x|n'} = \sqrt{\frac{2}{2L}}\sin(k_{n'} x) 
                         = \sqrt{\frac{1}{L}}\sin(k_{n'} x), \qquad
  k_{n'} = \left.\frac{\pi n'}{2L}\right|_{n'=1}^{\infty}, \qquad
  E_{n'} = \frac{\hbar^2k_{n'}^2}{2m} = \frac{\hbar^2 \pi^2}{8mL^2}{n'}^2.
$$

The strategy is to express the initial state $\ket{1}$ in terms of the new eigenstates $\ket{n'}$ and then to follow the time evolution in the new eigenbasis of the Hamiltonian in the expanded box:

$$
  \ket{1, t=0} = \sum_{n'}\ket{n'}\braket{n'|1}
                       = \sum_{n'}\ket{n'}\int_{0}^{L} \sqrt{\frac{2}{L}}\sqrt{\frac{1}{L}}\sin(k_{n'}x)\sin(k_1x)\d{x}
                       = \frac{\sqrt{2}}{L}\sum_{n'}\ket{n'}\int_{0}^{L} \sin\frac{\pi n' x}{2L}\sin\frac{\pi x}{L}\d{x} = \\
  = \frac{\sqrt{2}}{\pi}\sum_{n'}\ket{n'} \int_{0}^{\pi} \sin\frac{n'\theta}{2}\sin\theta\d{\theta}.
$$

The integral can be evaluated by converting to exponential form $\sin\theta = \frac{e^{\I\theta}-e^{-\I\theta}}{2}$, and after some algebra, we have:

$$
  \ket{1, t=0} = \frac{\sqrt{2}}{\pi}\sum_{n'}\ket{n'}\frac{4\sin\frac{n'\pi}{2}}{4-{n'}^2}
                       = \frac{\ket{2'}}{\sqrt{2}} + \frac{\sqrt{2}}{\pi}\sum_{n' \text{ odd}}^{\infty}\ket{n'}\frac{4(-1)^{(n'-1)/2}}{4-{n'}^2}\\
  = \frac{\ket{2'}}{\sqrt{2}} 
      + \frac{4\sqrt{2}}{\pi}\left(
      - \frac{\ket{1'}}{3\times (-1)}
     + \frac{\ket{3'}}{5\times 1} 
     - \frac{\ket{5'}}{7\times 3} 
     + \frac{\ket{7'}}{9\times 5} 
     - \frac{\ket{9'}}{11\times 7}
     %+ \frac{\ket{11'}}{13\times 9}
     %- \frac{\ket{13'}}{15\times 11}
     + \dots
     \right)
$$

All even terms vanish except for $n'=2$ which has the specified limiting value.

```{code-cell} ipython3
%pylab inline --no-import-all
N_terms = 5000
L = 1.0
x = np.linspace(0, 2*L, 100)   # Abscissa for plotting
n = np.arange(N_terms)         # Odd n'
c_n = np.sqrt(2)/np.pi * np.where(n%2 == 0, 0, 4*(-1)**((n-1)/2)/(4-n**2))
c_n[2] = 1./np.sqrt(2)

assert np.allclose(sum(c_n**2), 1)  # Check normalization
x_ = x[None, :]
n_ = n[:, None]
c_n_ = c_n[:, None]
hbar = m = 1.0
k_n_ = (np.pi*n_/2/L)
E_n_ = (hbar*k_n_)**2/2/m

def get_psi(t):
  """Return the time-dependent wavefunction."""
  return (np.exp(E_n_*t/1j/hbar)
          * c_n_ 
          * np.sin(k_n_*x_)*np.sqrt(2/2/L)
         ).sum(axis=0)

def get_dpsi(t):
  """Return the derivative of the time-dependent wavefunction"""
  return (np.exp(E_n_*t/1j/hbar)
          * c_n_ * k_n_
          * np.cos(k_n_*x_)*np.sqrt(2/2/L)
         ).sum(axis=0)

plt.plot(x, abs(get_psi(t=0))**2)
```

```{code-cell} ipython3
from ipywidgets import interact

tau = 16*m*L**2/hbar/np.pi

@interact(t_tau=(0, 1, 0.001))
def draw(t_tau=0):
    plt.plot(x, abs(get_psi(t=t_tau*tau))**2)
    plt.axis([0,2,0,2.2])
```

```{code-cell} ipython3
#@title Particle in a Box { run: "auto", display-mode: "form" }
from collections import namedtuple
from matplotlib import animation, rc
from IPython.display import HTML, clear_output
from matplotlib.patches import Rectangle

tau = 16*m*L**2/hbar/np.pi

# animate over some set of ts
Nt = 492          #@param {type:"slider", min:0, max:1000, step:2}
t_max_tau = 0.7   #@param {type:"slider", min:0.1, max:1.0, step:0.1}
t_max = t_max_tau * tau
ts = np.linspace(0, t_max, Nt+1)

# First set up the figure, the axes, and the plot element
fig, ax = plt.subplots()
plt.close()
ax.set_xlim((-0.1, 2*L+0.1))
ax.set_ylim((0, 2.3))
_rect_args = dict(width=0.1, height=2.3, hatch='\\', fill=False)
ax.add_patch(Rectangle((-0.1,0), **_rect_args))
p = ax.add_patch(Rectangle((L,0.), **_rect_args))

# We use a namedtuple to store the observables.  They can
# then be accessed by name later on.
Observables = namedtuple(
    'Observables',
    ['density', 'x_avg'])

def get_observables(t):
    """Return various observables at time t.
    
    Returns
    -------
    density : abs(psi)^2
    x_avg : expecatation value of x
    """
    psi = get_psi(t=t)
    #dpsi = get_dpsi(t=0)
    #j = (psi.conj()*(-1j*hbar*dpsi)/m).real
    density = abs(psi)**2
    x_avg = (density*x).sum()/density.sum()
    
    return Observables(
        density=density,
        x_avg=x_avg)

line1, = ax.plot([], [], 'b-', lw=2)
#ax2 = plt.twinx()
#line2, = ax.plot([], [], 'g--', lw=2)
line_x = ax.axvline(0.1, color='y')
title = ax.set_title(r"$t/\tau=0$")

# initialization function: plot the background of each frame
def init():
    obs = get_observables(t=0)
    density = obs.density
    line1.set_data(x, density)
    #line2.set_data(x, obs.j)
    #line_x.set_data(obs.x_avg)
    return (line1,)

# animation function: this is called sequentially
def animate(i):
    t = ts[i] 
    # Update data
    obs = get_observables(t=t)
    line1.set_data(x, obs.density)
    #line2.set_data(x, obs.j)    
    line_x.set_data([(obs.x_avg,)*2, (0,1)])
    if i > 0:
        # Move right wall after first step.
        p.set_xy((2*L, 0))
        title.set_text(r"$t/\tau={:.4f}$".format(t/tau))
    return (line1,)

anim = animation.FuncAnimation(
    fig, animate, init_func=init, frames=len(ts), 
    interval=50, repeat=False, blit=True)
plt.close('all')
display(anim)
```

```{code-cell} ipython3
x = np.linspace(-1,1,100)[:, None]
n = np.arange(-100, 100)[None, :]
th = np.exp(-np.pi*x**2*n).sum(axis=-1)
plt.plot(x, th)
#plt.ylim(-4,4)
```

```{code-cell} ipython3

```
