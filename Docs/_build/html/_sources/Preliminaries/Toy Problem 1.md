---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

# The "Baby" Problem

## Introduction

The baby problem is a pedagogic exercise in introductory field theory to help give a flavor of the process of perturbation theory and Feynman diagrams. It touches on major topics essential when you get to the real thing:
- path integrals and their pieces
- interaction vs. mass terms
- methods of expansion
- diagrammatic bookkeeping

+++

\begin{equation}
\boxed{Z(J, \lambda) = \int_{-\infty}^{\infty}dq 
    \exp\left(-\frac{1}{2}m^2 q^2 + Jq - \frac{1}{4!}\lambda q^4\right)\label{eq:toy}}
\end{equation}

The toy problem is a single integral of a scalar quantity standing in for what will eventually be a field. The $m$ term represents the particle mass, $J$ the source currents, and $\lambda$ the interactions. The prescription of field theory is to write a Lagrangian for the desired model that obeys all of the observed symmetries of the physical system, then to check how well it corresponds with reality. For instance, in a theory whose goal is to describe a parity-symmetric effect, a $\lambda q^3$ term could possibly represent the interaction, but $q^3$ does not respect parity symmetry, so we can reject it from the lagrangian.

```{code-cell} ipython3
import numpy as np
from matplotlib import pyplot as plt
%matplotlib inline

from scipy.special import factorial, factorial2
from ipywidgets import interact
```

# Exact solution

This problem has a flaw in that the diagrammatic bookkeping via Feynman diagrams becomes harder than simply doing the integral. Each of the pieces in the perturbation are easily obtained analytically, so why even bother with counting things like symmetry factors (more on these below)? I think the answer is that, because you can compare to an analytic solution for each term, you can check that counting such factors is sensible and see why you'd want to define Feynman rules differently. 

Because this toy problem is a 1D integral over scalar parameters, we can just _do_ the integral, as below.

```{code-cell} ipython3
def dZ(q, J=1, lam=1, m=1):
    _mass = - 0.5 * (m * q)**2
    _current = J * q
    _interact = - lam * q**4 / 24
    W = _mass + _current + _interact
    return np.exp(W)


@interact(lam=(0, 10, 0.1),
            m=(0, 5, 0.1),
            J=(-5, 5, 0.1))
def plot(lam=2, m=1, J=1):
    Nq = 10000
    Lq = 5
    
    q = np.linspace(-Lq, Lq, Nq)
    dq = np.diff(q)[0]
    dZs = dZ(q=q, J=J, lam=lam, m=m)

    fig, ax = plt.subplots()
    ax.plot(q, dZs, label="prob. dens.")
    ax.plot(q, np.cumsum(dZs*dq), label="cdf");
    ax.set(xlabel="qs", ylabel="P(x)",
          title=rf"$\lambda$={lam}, {m=}, {J=}")
    plt.legend();
    plt.tight_layout();
```

## Perturbation theory

In a pretend world without computers where I want to know what this integral is, I could begin to solve it by assuming that the interactions are weak enough that I can expand in orders of $\lambda$. The expansion will be

$$
Z(J, \lambda) = \int_{-\infty}^{\infty}dq \left[ \sum_{n=0}^{\infty}\frac{1}{n!} \left(\frac{\lambda}{4!} q^4 \right)^n \right] \exp\left(-\frac{1}{2}m^2 q^2 + Jq\right)
$$

The factor $q^{4n}$ can be cleverly expressed in terms of derivatives of $J$, so that a factor of $q$ is taken down from the exponent for every derivative.

$$
q^4 \rightarrow \frac{\partial^4}{\partial J^4} \equiv \partial_J^4
$$

Since these derivatives commute with the integration operation, they can be pulled out of the integrand, which is then integrable.

\begin{equation}
\begin{aligned}
Z(J, \lambda) &= \left[ \sum_{n=0}^{\infty}\frac{1}{n!} \left(\frac{\lambda}{4!} \frac{\partial^4}{\partial J^4} \right)^n \right] \int_{-\infty}^{\infty}dq \exp\left(-\frac{1}{2}m^2 q^2 + Jq\right) \\
              &= \left[ \sum_{n=0}^{\infty}\frac{1}{n!} \left(\frac{\lambda}{4!} \frac{\partial^4}{\partial J^4} \right)^n \right]
 \sqrt{\frac{2\pi}{m}}e^{J^2/2m^2}\label{eq:J_int}
\end{aligned}
\end{equation}

The last line comes from completing the square of the argument in the exponent:

$$
-\frac{1}{2} m^2 q^2 + Jq = -\frac{1}{2}\left( mq - \frac{J}{m}\right)^2 + \frac{J^2}{2m^2}
$$


The $Z(J, \lambda)$ expansion above has an overall normalization term $\sqrt{\frac{2\pi}{m}}$. Since this term is common to all terms of the expansion, and is also equivalent to the $Z(J=0, \lambda=0)$ term, it's common to drop this term and work in terms of a new Z:

$$
\tilde Z(J, \lambda) \equiv \frac{Z(J, \lambda)}{Z(J=0, \lambda=0)}
$$

Now we can consider terms in a double expansion in terms of $\lambda$ and $J$, and take the expansion to whatever order we want. In this case, it's very simple (which is the purpose of the pedagogical problem--we could always simply _do_ this integral, whether numerically or analytically, but in real field theory that will not be possible.)

\begin{equation}
\tilde Z(J, \lambda) = \left[ \sum_{n=0}^{\infty}\frac{1}{n!} \left(\frac{\lambda}{4!} \frac{\partial^4}{\partial J^4} \right)^n \right]  
\left[ \sum_{n=0}^{\infty}\frac{1}{n!} \left( \frac{J^2}{2m^2} \right)^n \right] \\
\end{equation}

+++

## Patterns and Diagrams

> *"You can do this as well as I can!"* \
>     - Zee, *QFT Nut* (p.45)

Looking quickly at the terms of the double expansion, you can see that there must be at least the number of $J$ terms as of derivatives $\partial / \partial_J$, or the terms will vanish. Each order of expansion in $\lambda$ comes with four derivatives, and each order of the expansion in $J$ comes with a factor of $J^2$. We can declare a sort of "rule" here, that for $\lambda^V$ and $J^m$, $4V \leq m$. Additionally, for each $\lambda$, four factors of $J$ will disappear, leaving $E = m - 4V$ factors of $J$ behind. Finally, the order of the expansion in $J$ is $L = m / 2$, or $2L = E + 4V$. At some point in history, clever people decided to represent orders of more complicated expansions like this with [diagrams](https://en.wikipedia.org/wiki/Feynman_diagram) instead of repetitive math. The same is possible here.

- Each factor of J corresponds to an external line.  (It might be useful to think of these as half-lines.)
- Each factor of $\lambda$ "eats" four factors of J, bringing them to a point on the diagram. The point where the lines meet is called a *vertex*, and in the present notation there are $V$ vertices.
- Lines not contributing to a vertex end at an external point, E. There are $E = m - 4V$ of these.
- Twice the number of lines in the diagram is equal to the number of external lines plus four times the number of vertices, $2L = E + 4V$.
- Each line gets a factor of $1 / m^2$.

This will give the correct proportionality and dimensionality for the relevant term, but not the correct numerical value. The number can be inferred from each diagram, however, and related back to the number extracted from the terms in the perturbative series under consideration.

[How am I going to draw diagrams in this notebook?]


## Rules for the Toy Problem

- $\lambda^V$ : one $\lambda$ for each vertex
- $J^E$       : one $J$ for each free endpoint
- $(1/m^2)^L$     : one factor for each line
- $1/L!$      : ways to interchange lines
- $1/V!$      : ways to interchange vertices
- $1/E!$      : ways endpoints could be interchanged
- $1/(4!)^V$   : ways to interchange endpoints joined at vertex 
- $(1/2^L)$   : ways of swapping endpoint pairs between lines
- $(4V + E)!$ : the overall J multiplicity. This is in some sense the total number of non-unique diagrams.

The rules above seem contrived to force-fit the problem, and make the diagrams give the correct number. That is the purpose of the diagram method though--to avoid the double expansions and repetitive integrals, and reduce the problem to one of drawing the correct pictures, with rigorous rules to extract the numeric factor and return to math. Counterintuitively, the above rules are likely reduced in complexity for a more realistic field theory, since in that case each $J$ will have a label corresponding to a spacetime location, reducing the number of possible diagrams significantly. In addition, I suspect it will reduce some of the seemingly redundant rules, such as a factor of $1/(4V!)$ for each set of $J$ meeting at a vertex, and the factor $1/V!$ for swapping the vertices themselves.

---

Each order of the double expansion reduces to a bunch of derivatives on a simple polynomial. It's pretty easy to get a formula for generating an arbitrary term in the double expansion, which can then be used to check against the number inferred from new diagrams and the above rules.

I'll show some algebra here, since the numeric generation of factors is error-prone, and it has been difficult to come up with a notation that seems simple and consistent.

\begin{align}
    L &\equiv \frac{4V+E}{2}; \quad 2L - 4V = E; \\
\tilde Z(\lambda^V, J^E) &= \left[\frac{1}{V!} \
    \left( \frac{-\lambda}{4!} \right)^V \left(\frac{\partial^4}{\partial J^4}\right)^V\right] \
    \left[ \frac{1}{\frac{4V+E}{2}!} \frac{1}{(2m^2)^{(4V+E)/2}} J^{4V+E} \right] \\
    &= \left[\frac{1}{V!} \
    \left( \frac{-\lambda}{4!} \right)^V \left(\frac{\partial^4}{\partial J^4}\right)^V\right] \
    \left[ \frac{1}{L!} \frac{1}{(2m^2)^{L}} J^{2L} \right] \\
    &= \left[\frac{1}{V!} \
    \left( \frac{-\lambda}{4!} \right)^V \right] \
    \left[ \frac{1}{L!} \frac{1}{(2m^2)^{L}}\right] \left[ \frac{\partial^{4V}}{\partial J^{4V}} J^{2L} \right] \\
    &= \left[\frac{1}{V!} \
    \left( \frac{-\lambda}{4!} \right)^V \right] \
    \left[ \frac{1}{L!} \frac{1}{(2m^2)^{L}}\right]  \left[ \frac{(2L)!}{(2L-4V)!} J^{2L-4V} \right] \\
\end{align}
$$
\boxed{\tilde Z(\lambda^V, J^E) = \left[\frac{1}{V!} \
    \left( \frac{-\lambda}{4!} \right)^V \right] \
    \left[\frac{(2L-1)!!}{E!(m^2)^{L}} J^E \right]}
$$

The last line comes from the identity 
$$\frac{(2L)!}{2^L L!}  = (2L-1)!!$$

+++

## Example: $\lambda = 0$

As a simple first case, let's look at the contribution to the integral from the $\lambda=0$ term of the double expansion.

*[Q: Is there a parallel with "ladder expansions" or Dyson series of more mature theories?]*

### J=0

There are no vertices. There are no lines. There are no external points. 

$$ \tilde Z(0, 0) = 1 $$

### J=2

There are $2!$ ways of distributing the two J terms, and $2!$ ways to interchange the endpoints. There's only one line, so there's nothing to swap it with.

$$
\tilde Z(J^2, 0) = \frac{2!}{2!} \frac{1}{(2m^2)^1} J^2 = \frac{J^2}{2m^2}
$$

### J=4

We have 4! ways of distributing the four J terms, and $4!$ ways to interchange the endpoints. Additionally, there are now two lines that can be swapped, giving a $1/2!$ factor.

$$
\tilde Z(J^4, 0) = \frac{4!}{4!}{2!} \frac{1}{(2m^2)^2} J^4 = \frac{1}{2!}\left(\frac{J^2}{2m^2}\right)^2
$$

... and so on. This series converges back to the function we expanded to get it, $e^{J^2/2m^2}$.

```{code-cell} ipython3
def _assert_even(E):
    try:
        assert np.all([_E % 2 == 0 for _E in [E]][0])
    except:
        raise ValueError("The number of external points E must be even.")
    pass


# lam = 0 terms
J = 1
m = 1
lam = 0.5

def Z_toy(E, V, J=1, lam=0.5, m=1):
    
    _assert_even(E)

    def _fact2(x):
        return np.where(x>0, factorial2(x), 1)

    L = (E + 4 * V) // 2
    
    param_factor = J**E * (-lam)**V / m**(2 * L)
    
    num = _fact2(2 * L - 1)
    
    denom = factorial(V) * factorial(4)**V * factorial(E)
    
    return param_factor * num / denom


def test_J_series(E, J=1, m=1):

    def J_series(O=1, J=1, m=1):
        """This counts the expansion in order of J^2 terms, which is half
        the number of external lines E when lambda=0."""
        
        def _expansion_term(n, J=J, m=m):
            """Simple Taylor expansion of e**{J**2/(2m**2)}"""    
            return (J**2 / 2 / m**2)**n / factorial(n)
        
        return np.array([_expansion_term(n=i, J=J, m=m) for i in range(O+1)])

    _assert_even(E)
    Es = np.arange(0, E)[::2]
    
    assert(np.allclose(_J_series, _Double_expansion) 
           for (_J_series, _Double_expansion) in zip(J_series(J=J, m=m, O=E//2),
                                                Z_toy(lam=0, V=0, E=Es, J=J, m=m)))

test_J_series(E=10, J=1, m=1)

Emax = 40
Es = np.arange(0, Emax)[::2]
J_terms = Z_toy(E=Es, V=0, lam=0, m=m, J=J)
J_terms_sum = np.cumsum(J_terms)
J_exp = np.exp(J**2/2/m**2)

fig, ax = plt.subplots(figsize=(6, 3))
ax.plot(Es/2, J_terms_sum, label="series");
err = abs(J_exp - J_terms_sum) / J_exp
ax.hlines(J_exp, 0, Es.max()/2, "k", "--", label="exact")
ax.set(xlabel=rf"$O(E/2)$", ylabel=rf"$Z(J^E)$", title=rf"$\lambda$=0, J convergence")
ax1 = plt.twinx(ax)
ax1.semilogy(Es/2, err, "red", alpha=0.5, linestyle="--", label="error")
ax1.set(ylabel="absolute cumulative error")
plt.tight_layout();
```

## Example: $\lambda=1$, $0$

Since there is a single interaction vertex and no external lines, the diagram is composed of two lines that close at the vertex. This double loop has $4!$ factors of J that need to be distributed $4!$ ways, so the terms cancel.
$$
\tilde Z(\lambda, 0) = \frac{1}{2!}\frac{1}{1!}\frac{4!}{4!}\frac{-\lambda}{(2m^2)^2} = \frac{1}{2!}\frac{-\lambda}{(2m^2)^2}
$$
### $J^{\text{odd}}$

There is no way to get a term in the Taylor expansion of $\exp\left({J^2}\right)$ proportional to $J^{1+2n}$. That statement is the same as saying that there's no way to draw a single line with only one unconnected endpoint. (Note for future use--that statement doesn't hold for a $\phi^3$ theory!)

### $J^2$

There are three lines, one vertex, and two external Js of six total. 

$$
\tilde Z(\lambda, J^2) = \frac{6!}{4!2!} \frac{1}{3!}\frac{-\lambda J^2}{(2m^2)^3}
$$
### $J^4$

Four lines, one vertex, four external Js, four internal Js.

$$
\tilde Z(\lambda, J^4) = \frac{8!}{4!4!} \frac{1}{4!}\frac{-\lambda J^4}{(2m^2)^4}
$$

## Example: $\lambda=2$, $0$

Two vertices and no external Js means there are eight internal Js and four lines.

$$
\tilde Z(\lambda^2, 0) = \frac{1}{2!} \frac{8!}{4!4!}\frac{(-\lambda)^2}{(2m^2)^4}
$$


### $J^2$

Two vertices and two external Js means there are eight internal Js, two external Js, and five lines.

$$
\tilde Z(\lambda^2, J^2) = \frac{1}{2!} \frac{10!}{(4!)^2 5!2!}\frac{(-\lambda)^2}{(2m^2)^5}J^2 = \frac{1}{2!}\left(\frac{-\lambda}{4!}\right)^2 \frac{1}{5!}\frac{10!}{2!}\frac{J^2}{(2m^2)^5}
$$

### $J^4$

Two vertices and four external Js means there are eight internal Js, four external Js, and six lines.

$$
\tilde Z(\lambda^2, J^4) = \frac{1}{2!} \frac{12!}{(4!)^2 4! 6!}\frac{(-\lambda)^2}{(2m^2)^6}J^4 = 
\frac{1}{2!}\left(\frac{-\lambda}{4!}\right)^2 \frac{1}{6!}\frac{12!}{4!}\frac{J^4}{(2m^2)^6}
$$


I hope at this point you are convinced, because the nutritional value of this problem has hit its wall.

+++

## Rules to the Toy Problem Reduced

If I've convinced you that the rules for the toy problem work for the diagrams possible in the theory, we can do a little more work at cleaning up the rules. Notice that the number of lines is what gives both the factor of $1/2^L$ and $1/(m^2)^L$, or that there's a same exponent of $(1/4!)^V$ as for the interaction strength, $\lambda^V$. Since all we need to do is define self-consistent rules to use later, it doesn't matter if we combine some out of convenience.

$$
\boxed{
\textbf{Toy Problem "Feynman Rules":} \\
- \left(\frac{-\lambda}{4!}\right)^V \text{: one factor for each vertex} \\
- 1/V!      \text{: ways to interchange vertices} \\
- (1/2m^2)^L     \text{: one factor for each line} \\
- 1/L!      \text{: ways to interchange lines} \\
- J^E       \text{: one $J$ for each free endpoint} \\
- 1/E!      \text{: ways endpoints could be interchanged} \\
- (4V + E)! \text{: the overall J multiplicity.} \\
}
$$

The final factor is in some sense the total number of non-unique diagrams.

In short, drawing a diagram and then applying the above rules will give you the symmetry factors for any term in the double expansion of $\tilde Z(J, \lambda)$. The topological variants of the diagrams for any particular order have no meaning in this toy theory, but will become crucial in real field theory.

+++

## Green's functions

There are different ways of attacking these perturbation problems, leading to what some authors will call "different" perturbation theories (which should yield the same answer!)

As an example, let's expand the original toy integral in terms of $J$ first. This will lead to a more complicated framework in the current easy problem, but is a core tool in particle physics.

\begin{align}
Z(J, \lambda) &= \int_{-\infty}^{\infty}dq \exp\left(-\frac{1}{2}m^2 q^2 + Jq - \frac{1}{4!}\lambda q^4\right) \\
&= \int_{-\infty}^{\infty}dq \left[ \sum_n \frac{1}{n!} (Jq)^n \right] \exp\left(-\frac{1}{2}m^2 q^2- \frac{1}{4!}\lambda q^4\right) \\
&\equiv \sum_n \frac{1}{n!} J^n G^{(n)} \\
\end{align}

$$
\boxed{G^{(n)} = \int_{-\infty}^{\infty} dq \, q^n \exp \left(-\frac{1}{2} m^2 q^2 - \frac{\lambda}{4!}q^4 \right)}
$$

+++

Then, expand in $\lambda$.

\begin{align}
\mathcal O(0): \quad G^{(0)} &= \int_{-\infty}^{\infty} dq \; \exp\left(\frac{1}{2}m^2 q^2\right) \
        = \int_{-\infty}^{\infty} dq \left( e^{-m^2q^2/2} \right) = \left< 0 \right>\\
        \quad G^{(2)} &= \int_{-\infty}^{\infty} dq \; q^2 \exp\left(\frac{1}{2}m^2 q^2\right) \
        = \int_{-\infty}^{\infty} dq \left(q^2 e^{-m^2q^2/2} \right) = \left< qq \right>\\
        \quad G^{(4)} &= \int_{-\infty}^{\infty} dq \; q^4 \exp\left(\frac{1}{2}m^2 q^2\right) \
        = \int_{-\infty}^{\infty} dq \left(q^4 e^{-m^2q^2/2} \right) = \left< qqqq \right>\\
\end{align}


\begin{align}
\mathcal O(\lambda): \quad G^{(0)} &= \int_{-\infty}^{\infty} dq \; \exp\left(\frac{1}{2}m^2 q^2\right) \
        \left[\frac{-\lambda}{4!}q^4\right] \
        = \frac{-\lambda}{4!} \int_{-\infty}^{\infty} dq \left(q^4 e^{-m^2q^2/2} \right) \\
        &= \frac{-\lambda}{4!} \left< qqqq \right> \\
        \quad G^{(2)} &= \int_{-\infty}^{\infty} dq \; q^2 \exp\left(\frac{1}{2}m^2 q^2\right) \
        \left[\frac{-\lambda}{4!}q^4\right] \
        = \frac{-\lambda}{4!} \int_{-\infty}^{\infty} dq \left(q^6 e^{-m^2q^2/2} \right) \\
        &= \frac{-\lambda}{4!} \left< qqqqqq \right> \\
        \quad G^{(4)} &= \int_{-\infty}^{\infty} dq \; q^4 \exp\left(\frac{1}{2}m^2 q^2\right) \
        \left[\frac{-\lambda}{4!}q^4\right] \
        = \frac{-\lambda}{4!} \int_{-\infty}^{\infty} dq \left(q^8 e^{-m^2q^2/2} \right) \\
        &= \frac{-\lambda}{4!} \left< qqqqqqqq \right> \\
\end{align}

Contracting terms like $\left< q^n \right>$ means picking a factor of q, counting the number of pairs you could make with that q, then repeating the process on the remaining qs. That gives $(n-1)\dot(n-3)\dot...\dot (1) = (n-1)!!$ ways of combining the factors, which is the number you'd get from performing the integral manually. By dividing by $m^n$ and remembering that we've typically dropped the normalization factor $\sqrt{2\pi}/m$, we get these "Green's functions" by inspection (relying on a _lot_ of machinery and prior calculation to justify the "inspection"!)

+++

Check the $\mathcal O(\lambda), G^{(4)}$ term with the value obtained by $\lambda, J$ double expansion:

\begin{align}
\frac{J^4}{4!} \left(\frac{-\lambda}{4!}\right) G^{(4)} &= \frac{J^4}{4!} \left(\frac{-\lambda}{4!}\right)^2 \left<qqqqqqqq\right> \\
    &= \frac{J^4}{4!} \left(\frac{-\lambda}{4!}\right) \frac{7!!}{m^8} \\
    &= \frac{J^4}{4! m^8} \frac{8!}{2^4 4!} \left(\frac{-\lambda}{4!}\right) \\
    &= \left(\frac{-\lambda}{4!}\right) \frac{8!}{(4!)^2} \frac{J^4}{(2m^2)^4}
\end{align}

This is the same answer as before. Slick.

+++

## Convergence

```{code-cell} ipython3
def Z_toy(E, V, J=1, lam=0.5, m=1):
    
    _assert_even(E)

    def _fact2(x):
        return np.where(x>0, factorial2(x), 1)

    L = (E + 4 * V) // 2
    
    param_factor = J**E * (-lam)**V / m**(2 * L)
    
    num = _fact2(2 * L - 1)
    
    denom = factorial(V) * factorial(4)**V * factorial(E)
    
    return param_factor * num / denom

def Z_order(n=2, V=1, lam=0.5):

    denom = (factorial(V) * (factorial(4))**V * factorial(n) * (2**n) * factorial(4 * V)) 
    return (-lam)**V * factorial(n + 4 * V) / denom


Emax = 20
Vmax = 5
Es = np.arange(0, Emax)[::2]
fig, ax = plt.subplots()
for V in np.arange(0, Vmax):
    ax.plot(Es, Z_toy(E=Es, V=V, lam=0.5), label=f"{V=}")
# ax.set(ylim=(-0.22, 0.22))
plt.legend();
plt.tight_layout();
```

```{code-cell} ipython3
from mmfutils.plot import imcontourf

#plt.contourf(ns.ravel(), Vs.ravel(), Z_order(n=ns, V=Vs).T, levels=50)
O_V = 15
O_E = 30
_assert_even(O_E)

Vs = np.arange(0, O_V)[None,:]
Es = np.arange(0, O_E)[::2, None]

imcontourf(Es, Vs, Z_toy(E=Es, V=Vs, lam=0.3), 
           #interpolation="bicubic")
           interpolation=None, vmin=0, vmax=1)
plt.xlabel(rf"O($J^2$)")
plt.ylabel(rf"O($\lambda$)")
plt.tight_layout()
```

## Asymptotic Series

> "Divergence should be expected when the solution depends on two independent length scales." - Milton van Dyke

I'm going to rescale the toy problem so that this is easier to analyze.

\begin{equation}
\tilde Z(J, \lambda) = \left[ \sum_{n=0}^{\infty}\frac{1}{n!} \left(-\epsilon \frac{\partial^4}{\partial J^4} \right)^n \right] e^{J^2/2}
\end{equation}

+++

How does the J term converge?

```{code-cell} ipython3
def Z_toy(E, V, J=1, eps=0.5):
    
    _assert_even(E)

    def _fact2(x):
        return np.where(x>0, factorial2(x), 1)

    L = (E + 4 * V) // 2
    
    param_factor = J**E * (-eps)**V
    
    num = _fact2(2 * L - 1)
    
    denom = factorial(V) * factorial(E)
    
    return param_factor * num / denom
```

```{code-cell} ipython3
def J_approx(J, E):
    return (J**2 / 2)**E / factorial(E)

def J_exact(J):
    return np.exp(J**2 / 2)

jmin, jmax = 4, 5
Js = np.linspace(0, jmax, 1000)

J_series = []
Order = 20
for n in range(0, Order):
    J_series.append(J_approx(J=Js, E=n))
J_series = np.cumsum(np.array(J_series), axis=0).T

fig, ax = plt.subplots(figsize=(8, 4))
ax.plot(Js, J_exact(Js), "--", color="k");
for O in range(J_series.shape[1])[::2]:
    ax.plot(Js, J_series[:, O], label=f"{O=}");
ax.set(xlim=(jmin, jmax), xlabel="J")
plt.legend(loc=2);
```

J converges well for small J and a small number of expansion terms N, and converges well for "large" J if the number of terms N is large. But is this a general behavior of series expansions?

Rather than deal with messy subsequent derivatives of $e^{J^2/2}$, I'll consider terms in the double expansion only, and choose a small J so that I know that series converges quickly for low N, to keep computational costs down.

```{code-cell} ipython3
from scipy.integrate import trapezoid, quad

def Z(J, eps, qmax=5, Nqs=1000):
    def _dZ(q, J, eps):
        _mass = - 0.5 * (q)**2
        _current = J * q
        _interact = - eps * q**4
        W = _mass + _current + _interact
        return np.exp(W)

    qs = np.linspace(-qmax, qmax, Nqs)[:, None]
    eps = eps[None, :]
    dZs = _dZ(q=qs, J=J, eps=eps)
    res = trapezoid(y=dZs, x=qs, axis=0)
    return res / np.sqrt(2 * np.pi)

J = 1 # Note: I do not trust something here unless J=1 or 0.
O_V = 15
O_E = 6
_assert_even(O_E)

Vs = np.arange(0, O_V)[None,:]
Es = np.arange(0, O_E)[::2, None]

epss = np.linspace(-0.01, 0.06, 1000)
Z_exact = Z(J=J, eps=epss)
res = np.array([Z_toy(E=Es, V=_V, J=J, eps=epss) for _V in range(O_V)])
res = res.sum(axis=1).cumsum(axis=0)

fig, ax = plt.subplots(figsize=(8, 4))
ax.plot(epss, Z_exact, "--", label="exact", color="black");
for N in range(O_V):
    ax.plot(epss, res[N, :], label=f"{N=}");
ax.set(ylim=(-1, 3), xlabel=(rf"$\epsilon$"))
plt.legend(bbox_to_anchor=(1, 1.05));
```

This quickly diverges for N of any order. Higher order approximations do a better job for very small $\epsilon$, but actually get worse after. The factorials in the expansion get huge quickly, and the series oscillates to compensate for the previous (enormous) overcorrection.

Key: radius of convergence in $\epsilon$: consider $\epsilon\rightarrow -\epsilon$. The radius of convergence for a perturbative series is in terms of $|\epsilon|$. If the series clearly does not converge for $-\epsilon$, it probably won't for $+\epsilon$.

```{code-cell} ipython3

```
