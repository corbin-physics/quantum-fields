---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

```{code-cell} ipython3
%matplotlib inline 

import numpy as np
import matplotlib.pyplot as plt
```

# Complex Analysis

Scattering problems in physics often require contour integration to extract relevant physics. Here the basics relevant to problems in quantum scattering and tunneling are covered.

+++

Superbasics:
- Complex numbers have a real and imaginary part
- They can be decomposed into a magnitude and a phase
$$
r e^{i\theta} = r \left( \cos(\theta) + \sin(\theta)\right)
$$

- r : magnitude or modulus
- $\theta$ : argument

+++

## Triangle inequality

For two complex numbers $z$ and $w$, 

$$
|z + w| \leq |z| + |w|
$$

Prove this by squaring both sides.

Because $z$ is a vector on some unit circle, its maximum in the real axis will always be when its complex part is zero. Imagine projecting down a point on the unit circle from anywhere else; its x-component (real) will be less than one. Therefore,
$$
- |z| \leq \mathrm{Re}(z) \leq |z|
$$

$$
|z + w|^2 = |z|^2 + |w|^2 + 2 \mathrm{Re}(|z||w|)
$$

since the absolute value has already taken the real projection of the cross term. This could have discarded "information", however, so the inequality may be invoked.

\begin{align}
|z + w|^2 &= |z|^2 + |w|^2 + 2 \mathrm{Re}(|z||w|) \\
          &\leq |z|^2 + |w|^2 + 2 |z||w| \\
          &\leq \left(|z| + |w|\right)^2
\end{align}

+++

## Multivalued Roots

**Nth roots of unity**

Complex numbers $z=e^{i\theta}$ on a unit circle have a U(1) symmetry. After rotating $2\pi$, they come back to their original point. Discrete rotations form a group; rotating $2\pi/n$ degrees $n$ times will bring you back to the starting point. The n points can be considered the group elements, with angle addition as the group multiplication. 

Raising $z$ to a power increases the angle rotated. For example, if $z = e^{i\pi} = -1$, z is the point on the left of the unit circle. Squaring this gives $z^2 = e^{2\pi}=1$, and the angle has rotated by z twice. *Raising to a power increases the angle.* This suggests that taking a root will contract the angle in the same way, which is true. There's an extra complication now, however, because the roots have become multivalued.

```{code-cell} ipython3
eps = np.finfo(float).eps

def complex_root(z, n=2):
    r = abs(z)
    theta = np.arctan2(z.imag, z.real)
        
    arg = (theta + np.linspace(0, 2 * np.pi * (n-1), n)) / n
    
    roots = r**(1/n) * np.exp(1j * arg)
    return roots

def plot_roots(z, n, save=False, title=None):
        
    thetas = np.linspace(0, 2 * np.pi, 100)
    unit_circle = np.exp(1j * thetas)

    fig, ax = plt.subplots(figsize=(5,5))
    ax.set(aspect=1, title=f"Complex {n} root of {z=}")

    ax.grid(True, which="both")
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')

    ax.plot(unit_circle.real, unit_circle.imag, alpha=0.5)
    
    roots = complex_root(z=z, n=n)
    
    ax.plot(roots.real, roots.imag, ".", color="k", markersize=10.0)
    plt.tight_layout();
    if save is True:
        if title is None:
            title = f"roots_{n=}.pdf"
        fig.savefig(title)
        
    # hack to get roots out of this
    return roots
```

$$
z = e^{i\pi} = e^{i (\pi + 2\pi)} = -1
$$

We typically treat the middle two terms above as equivalent, since wrapping the phase by $2\pi$ is a full cycle and we end up back at the beginning. Under the square root scaling, this only becomes a half-wrap! So $\sqrt{z}$ in general has two roots:


\begin{align}
\sqrt{z} &= e^{i\pi/2} \\
         &= \cos{\pi/2} + i \sin{\pi/2} \\
         &= i \\
         &= e^{i (\pi/2 + \pi)} = e^{i 3\pi/2} \\
         &= \cos{3\pi/2} + i \sin{3\pi/2} \\
         &= -i
\end{align}

$$
\sqrt{z} = \pm i
$$

Adding $4\pi$ to the initial phase in $z$ will give $\sqrt{z} = i$ again, so there are only two square roots for z... which is a pretty generalization of traditional arithmetic, where the square root of real numbers had two values.

```{code-cell} ipython3
plot_roots(z=-1, n=2);
```

For the cube root, there are now three roots, unlike in the real numbers only case (though in that case, we had three roots, with degeneracies.) Take the same case again, where $z = e^{i\pi}$.


\begin{align}
\sqrt{z} &= e^{i\pi/3} \\
         &= \cos{\pi/3} + i \sin{\pi/3} \\
         &= \frac{1 + \sqrt{3}i}{2} \\
         &= e^{i (\pi/3 + 2\pi/3)} = e^{i \pi} \\
         &= -i \\
         &= e^{i(\pi/3 + 4\pi/3)} \\
         &= e^{i5\pi/3} \\
         &= \cos{5\pi/3} + i \sin{5\pi/3} \\
         &= \frac{1 - \sqrt{3}i}{2} \\
\end{align}

```{code-cell} ipython3
roots = plot_roots(z=-1, n=3)
```

```{code-cell} ipython3
roots = plot_roots(z=-40, n=3)
```

## Trig Identities
## Dot and Cross Products

The product of a complex number and the complex conjugate of another gives you the vector dot and cross products, neatly packaged into one operation.

\begin{equation*}
A = r_a e^{i\phi_a} \qquad B = r_b e^{i\phi_b} \qquad \theta \equiv \phi_b - \phi_a \\
\end{equation*}

\begin{align*}
\bar{A}B &= r_a r_b e^{i(\phi_b - \phi_a)} = r_a r_b e^{i(\theta)} \\
         &= r_a r_b \left(\cos{\theta} + i \sin{\theta}\right) \\
\end{align*}

\begin{align}
\mathbf{Re}(\bar{A}B) &= r_a r_b \cos{\theta} = \mathbf{A} \cdot \mathbf{B} \\
\mathbf{Im}(\bar{A}B) &= r_a r_b \sin{\theta} = \mathbf{A} \times \mathbf{B} \\
\end{align}

The dot product gives the product of $A$ with the value of $B$ projected onto $A$, and the cross product is the area of the parallelogram created by the two vectors. 

## Geometric Series and Roots


## Branch Cuts

When dealing with a multivalued function (a *multifunction*), typically one will want to restrict the region of interest to a subregion where the function is single-valued. The location where the region is split is called a *branch cut*.

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

# Contour Integration 

> Here's a quote

*Keep the discussion minimal and essential; the justification of the theory is worth books, which is not your job.*

---

+++

For a simply-connected region, one where the contour can be reduced to a point without entering a pole,

$$
\oint_C d(z) dz = 0
$$

which can be proven with the complex Stokes' theorem.

Integrals are by convention taken to be in the counter-clockwise direction. The easy way to remember this is that the positive direction is the direction a phasor on an [Argand diagram](https://mathworld.wolfram.com/ArgandDiagram.html) (complex plane).

## Example : $z^n$
Integrating $z^n$ over a circular contour (i.e., $dr = 0$) with $z = r e^{i\theta}$, 

\begin{gather}
z = re^{i\theta} \qquad dz = i r e^{i \theta} d\theta \\
\oint_C z^n dz = i \oint_C r^n e^{i\theta n } r e^{i \theta} d\theta \\
= i r^{n+1} \oint_C e^{i \theta(n + 1)} d\theta \\
\end{gather}

If $n \neq -1$, the integral proceeds simply.

$$
= \frac{r^{n+1}}{n+1} e^{i\theta(n+1)}\big\rvert_0^{2\pi} = 0
$$

If $ n = -1$, we get a different result.

\begin{align}
\oint_C \frac{dz}{z} &= i \oint_C \frac{r e^{i\theta}}{r e^{i \theta}} d\theta \\
                     &= i \int_0^{2\pi} = 2\pi i
\end{align}

$$
\oint_C z^n dz = \left\{ \begin{array}{cl}
0 & : n \neq -1 \\
2\pi i & : n = 1
\end{array}\right.
$$

---

+++

## Cauchy's Integral Theorem

Cauchy's integral theorem is as follows:

$$
\boxed{\frac{1}{2\pi i} \oint_C \frac{f(z)}{z-z_0} dz = f(z_0)}
$$

As a simple example, try this by shifting $z = r e^{i\theta} + z_0$.

\begin{align}
\oint_C \frac{f(z)}{z - z_0} dz &= \int_0^{2\pi} \frac{f(re^{i\theta} + z_0)}{re^{i\theta} + z_0 - z_0} i r e^i\theta d\theta \\
                              I &= i \int_0^{2\pi} f(re^{i\theta} + z_0) d\theta
\end{align}

We're free to choose a contour of any radius here as long as it has the same endpoints in the integration variable $\theta$, so consider the limit $r \rightarrow 0$.

$$
I = \lim_{r\rightarrow 0} i \int_0^{2\pi} f(re^{i\theta} + z_0) d\theta = 2 \pi i f(z_0)
$$

## Derivative Rule
By taking subsequent derivatives of the integral theorem, useful rules for derivatives and higher order poles can be obtained.

\begin{align}
f(z_0) &= \frac{1}{2\pi i} \oint_C \frac{f(z)}{z-z_0} dz \\
f'(z_0) &= \frac{1}{2\pi i} 1! \oint_C \frac{f(z)}{(z-z_0)^2} dz \\
f''(z_0) &= \frac{1}{2\pi i} 2! \oint_C \frac{f(z)}{(z-z_0)^3} dz \\
\end{align}

$$
\boxed{f^{(n)}(z_0) = \frac{1}{2\pi i} n! \oint_C \frac{f(z)}{(z-z_0)^{n+1}} dz}
$$

+++

## Laurent Expansion
The Laurent expansion combined with Cauchy's integral theorem combine to give the residue theorem.

## Cauchy Principal Value

Over intervals where an integrand may be non-analytic, a contour integral is often well defined in terms of its [principal value](https://en.wikipedia.org/wiki/Cauchy_principal_value), which restricts the integral to the well defined region.

```{code-cell} ipython3

```

```{code-cell} ipython3

```

> *Move these out and into a demo notebook. The QFT project should become self-contained notes towards QFT, not a tutorial on life and mathematics from scratch.*

---

## Example 1

*Arfken 11.4.2.*

$$
\oint_\mathbb{C} \frac{dz}{z^2 -1}
$$

Closed by a contour fulfiling the circular parameterization $c : |z-1| = 1$. This is a circle of radius 1, centered at $1 + 0j$.

```{code-cell} ipython3
thetas = np.linspace(0, 2*np.pi)
c = 1 * np.exp(1j * thetas) + 1

fig, ax = plt.subplots(figsize=(3,3))
ax.plot(c.real, c.imag);
ax.grid("both")
ax.scatter([-1, 1], [0, 0], color="k", s=5)
ax.set(aspect=1);
```

The contour at $z=-1$ is not closed, so the answer is simply the residue from the remaining contour at $z=1$.

$$
\oint_\mathbb{C} \frac{dz}{z^2 -1} = \oint_\mathbb{C} \frac{dz}{(z+1)(z-1)} \\
 = \frac{2 \pi i}{(1+1)}
 = i\pi
$$

+++

## Example 2

*Arfken 11.4.6.*

$$
\oint_\mathbb{C} \frac{e^{iz}}{z^3} dz
$$

Square contour, centered at 0, sides length a>0.

This is a third order pole at $z=0$, so we need to take two derivatives of the residue.

\begin{align}
\oint_\mathbb{C} \frac{e^{iz}}{z^3} dz &= 2 \pi i \left( \frac{1}{2!} \frac{\partial^2}{\partial z^2} e^{iz}\right) \bigg|_{z=0} \\
    &= \pi i (i^2) e^{i(0)} \\
    &= -i\pi
\end{align}

## Example 3

$$
\frac{1}{2\pi i} \oint_\mathbb{C} \frac{z}{(z-1)^3(z+2)^2} dz \\
$$
There are two poles: $z=-2, z=1$.

```{code-cell} ipython3
thetas = np.linspace(0, 2*np.pi)
c = 1.5 * np.exp(1j * thetas)

fig, ax = plt.subplots(figsize=(3,3))
ax.plot(c.real, c.imag);
ax.grid("both")
ax.scatter([-2, 1], [0, 0], color="k", s=5)
ax.set(aspect=1);
```

Closing with a circle centered at the origin with radius $3/2$, only the third order pole will be enclosed. Using the derivative rule, the residue of this integral will be

\begin{align}
\frac{1}{2!} \frac{\partial^2}{\partial z^2} \left[ \frac{z}{(z+2)^2} \right] \bigg|_{z=1} &=
    \frac{1}{2!} \frac{\partial}{\partial z} \left[ \frac{1}{(z+2)^2} + \frac{-2z}{(z+2)^3} \right] \bigg|_{z=1} \\
    &= \frac{1}{2!} \left[ \frac{-2}{(z+2)^3} + \frac{-2}{(z+2)^3} + \frac{6z}{(z+2)^4} \right] \bigg|_{z=1}\\
    &= \left[ \frac{-1}{3^3} + \frac{-1}{(3^3} + \frac{3}{3^4} \right] \\
    &= \frac{-1}{27} \\
\end{align}

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```
