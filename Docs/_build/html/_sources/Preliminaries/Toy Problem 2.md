---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

---
# The "Child" Problem

The toy problem is useful as a demonstration of an asymptotic system with a poor convergence (large divergence of radius 0). On the other hand, it lacked a concept of propagation, and thus had diagrams which did not mean anything. In the toy problem, each term in the perturbative expansion was described by many different diagrams which did not have individual meaning. This will no longer be the case in the child problem.

$$
\int^{\infty}_{-\infty}...\int^{\infty}_{-\infty}dx_1...dx_N \equiv \int_{\mathbb{R}^N} \qquad
\mathbf{x} = \begin{pmatrix}
 x_0 \\
 x_1 \\
 \vdots \\
x_{n-1}
\end{pmatrix}
$$

This integral is over every x index in discretized space. What happens when the space is taken to the continuum limit is a consideration worth pondering.

$$
$$

$$
\tilde Z(J, \lambda) = \int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{x}^\intercal \mathbf{A} \mathbf{x} - \frac{\lambda}{4!}(\mathbf{x}^\intercal \mathbf{x})^2 + \mathbf{Jx} \right)} 
$$

+++

$$
G^{(2)}_{ij} = \left<q_i q_j \right> = \left< \Omega | \hat \phi(x_j) \hat\phi(x_i)|\Omega\right>
$$

### Relation to field theory

The child problem is a real space edition of the quantum mechanical path integral. 

$$
J_n \rightarrow J(x_n) \\
\int_{\mathbb{R}^N} e^{-S} \rightarrow \int \mathcal{D}x e^{iS}
$$

+++

As in the baby problem, we're welcome to expand in the $\lambda$ term first, and use it to take derivatives of $J$. This will turn out to be a less pleasant way to expand the integral, according to my taste.

\begin{align}
Z &= \int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{x}^\intercal \mathbf{A} \mathbf{x} - \frac{\lambda}{4!}(\mathbf{x}^\intercal \mathbf{x})^2 + \mathbf{Jx} \right)} \\
    &= \int_{\mathbb{R}^N} \sum_n \frac{1}{n!} \left[ \frac{\lambda}{4!}(\mathbf{x}^\intercal \mathbf{x})^2 \right]^n \exp{\left(-\frac{1}{2}\mathbf{x}^\intercal \mathbf{A} \mathbf{x} + \mathbf{Jx} \right)}
\end{align}

Consider a specific term; for starters, let $\lambda = 0$ and expand to $O(J^2)$.

Note that each factor of $J$ from this expansion should have its own unique set of indices; don't contract two $Js$ with each other without thinking!

$$
(\mathbf{J} \cdot \mathbf{x})^m = \prod_{j=1}^m \left[ \sum_{i_j=1}^n J_{i_j} x_{i_j}\right]
$$

+++

## [$\lambda^0, J^2$]

\begin{align}
Z &= \int\int\cdots\int dx_0 dx_1 \cdots dx_{n-1} \frac{1}{2!} \sum_{r=0}^{l}\sum_{s=0}^m J_s x_s J_r x_r e^{-xAx/2} \\
&= \frac{1}{2!} \sum_{r=0}^{l}\sum_{s=0}^m J_s J_r G^{(2)}_{rs}(\lambda^0) 
\qquad G^{(2)}_{rs}(\lambda^0) \equiv \int\int\cdots\int dx_0 dx_1 \cdots dx_{n-1} x_s x_r e^{-xAx/2}
\end{align}

Weinberg's method of performing this integral in ch.9 of *The Quantum Theory of Fields I* is to expand a general integral of $2N$ factors of $x_i$ and construct a sum rule for them, from which one can extract any particular term in the integral expansion. That's slick, but a bit formal for what I want to cover here. 

Any way you can do this integral, do it. One easy(ish) way is to differentiate under the integral sign. Add a term proportional to x (another $J$, perhaps), take derivatives of it, commute the derivative operators and at the end of the day set the dummy variable to zero. 

\begin{align}
G_{rs} &= \int\int\cdots\int dx_0 dx_1 \cdots dx_{n-1} x_s x_r e^{-xAx/2} \\
&= \left[ \int\int\cdots\int dx_0 dx_1 \cdots dx_{n-1} \frac{\partial}{\partial J_s}\frac{\partial}{\partial J_r} e^{-xAx/2 + Jx} \right]_{J=0} \\
& = \frac{\partial}{\partial J_s}\frac{\partial}{\partial J_r} 
    \underbrace{\left[\det{\left(\frac{A}{2\pi}\right)} \right]^{-1/2}}_{Z(0,0)}
    e^{-J A^{-1} J / 2} \\
&= Z(0,0) A^{-1}_{rs}
\end{align}

It's worth rewriting this one explicitly. For derivatives of two components of the $J$ vectors,

$$
\boxed{\left[ \frac{\partial^2}{\partial J_i \partial J_j} e^{J A^{-1} J / 2}\right]_{J=0} = A^{-1}_{ij}}
$$

+++

Putting this together, the result for this propagator is
\begin{align}
G_{ij}(\lambda^0) &= Z(0, 0) A^{-1}_{ij} \\
Z(J^2, \lambda^0) &= Z(0, 0) \frac{1}{2!} \sum_{i, j = 0}^{N-1} J_i J_j A^{-1}_{ij} \\
                  &= \frac{1}{2!} \sum_{i, j = 0}^{N-1} J_i J_j G_{ij}(\lambda^0)
\end{align}

Often, we won't even care about the rest of Z; the Green's function will suffice (since it contains the physics).

+++

### $G^{(4)}(\lambda^0)$

The next order, with four $J$ terms (if there were three, we'd be doing an integral like $\int dx x^3 e^{-x^2}$, which is odd, and thus vanishes), could be done the same way as above. Add a $Jx$ term to the exponent in the Green's function, use its expansion to take four derivatives, and then solve from there. If we stop to think a little though, the order of the expansion in $\lambda$ we're looking at is closed.

$$
Z(J, \lambda^0) = \int_{\mathbb{R}^N} e^{-xAx/2 + Jx} = Z(0,0) e^{-JA^{-1}J/2}\\
= \mathbb{1} - \frac{JA^{-1}J}{2} + \frac{1}{2!} \left(\frac{JA^{-1}J}{2} \right)^2 - \mathcal{O}(J^6)
$$

The second nontrivial term is the one we're looking for here. 

\begin{align}
Z(J^4, \lambda^0) &= \frac{1}{2^2 2!} \sum_{ijkl} \left[ J_i A^{-1}_{ij} J_j J_k A^{-1}_{kl} J_l \\
                                                      + J_i A^{-1}_{ik} J_k J_j A^{-1}_{jl} J_l \\
                                                      + J_i A^{-1}_{il} J_l J_j A^{-1}_{jk} J_k \right] \\
                  &= \frac{1}{2!} \sum_{ijkl} G^0_{ijkl} J_i J_j J_k J_l \\
\end{align}

$$
\boxed{G^0_{ijkl} = \left< x_i x_j x_k x_l\right> = S \left[ A^{-1}_{ij}A^{-1}_{kl} 
                            + A^{-1}_{ik}A^{-1}_{jl} + A^{-1}_{il}A^{-1}_{jk} \right]}
$$

The factor in front of this expansion is $\frac{1}{2^2 2!}$. This comes from the order of the expansion of $e^{JA^-1J/2}$ to $\mathcal O(J^4)$. We could have also gotten this factor by Wick contracting the propagator:

$$
\left<x_i x_j x_k x_l\right> \rightarrow 3!! = \frac{4!}{2^2 2!}
$$

There are $3!!$ ways to contract the indices here, and four $x_i$ terms to contract with the four $J_i$ terms outside the propagator. All of these numbers can be brought together using the following very crazy relation:

$$
\boxed{ (2n)!! = \frac{(2n)!}{2^n n!}}
$$

Wick's rule gives us the prefactor for the expansion in the diagram, but it must be divided by the number of contractions with outside $J$ terms to give the proper answer.

*The symmetry factors you can get from a diagram seem to include the constant not counted in the propagator. Should that expansion factor be included as well? Zee's notation is sometimes sloppy. Admitting that up front doesn't excuse it!*

**Work through $\phi^4$ and do the symmetry factors properly. Come back and fix this one after that; it should be a breeze.**

+++

:::{dropdown} Another way: derivative under the integral

$$
    \int_{\mathbb{R}^N} x_i x_j x_k x_l e^{-xAx/2} \\
    = \int_{\mathbb{R}^N} \left[ \partial_{J_i} \partial_{J_j} \partial_{J_k} \partial_{J_l} e^{-xAx/2 + Jx}\right]_{J=0} \\
    = \left[ \det \left( \frac{A}{2\pi} \right) \right]^{-1/2} \partial_{J_i} \partial_{J_j} \partial_{J_k} \partial_{J_l} e^{-JA^{-1}J/2} \\
    $$
       
From now on, the overall normalization from the determinant piece will be dropped. 
Taking the derivatives, and remembering to actually go through with the chain rule...
 
$$
    = \partial_{J_i} \partial_{J_j} \partial_{J_k} \left[ - \frac{A^{-1}_l J}{2} - \frac{J A^{-1}_l }{2} \right] e^{-JA^{-1}J/2} \\
    =  \partial_{J_i} \partial_{J_j} \left[ \left(- \frac{A^{-1}_{lk}}{2} - \frac{A^{-1}_{kl} }{2}\right) 
    + \left(- \frac{A^{-1}_l J}{2} - \frac{J A^{-1}_l }{2}\right)\left(- \frac{A^{-1}_k J}{2} - \frac{J A^{-1}_k }{2} \right)
    \right] e^{-JA^{-1}J/2} \\
        =  \partial_{J_i} \left[ \left(-A^{-1}_{lk}\right) \left(- \frac{A^{-1}_j J}{2} - \frac{J A^{-1}_j }{2}\right)
    + \left(- \frac{A^{-1}_{lk}}{2} - \frac{A^{-1}_{kl} }{2}\right) \left(\frac{A^{-1}_k J}{2} - \frac{J A^{-1}_k }{2} \right)
    + \left(- \frac{JA^{-1}_{l}}{2} - \frac{A^{-1}_{l}J}{2}\right)\left(- \frac{A^{-1}_{jk}}{2} - \frac{A^{-1}_{kj} }{2}\right)
    \right] e^{-JA^{-1}J/2} \\
   =  \partial_{J_i} \left[ \left( -A^{-1}_{lk} \right) \left(-\frac{A^{-1}_j J}{2} - \frac{J A^{-1}_j}{2} \right)
    + \left(-A^{-1}_{lk}\right) \left(\frac{A^{-1}_k J}{2} - \frac{J A^{-1}_k }{2} \right)
    + \left(- \frac{JA^{-1}_{l}}{2} - \frac{A^{-1}_{l}J}{2}\right)\left(- A^{-1}_{kj}\right)
    \right] e^{-JA^{-1}J/2} \\
    $$
    
$$
= \left[ \left( -A^{-1}_{lk}\right) \left( -A^{-1}_{ij}\right) 
    +  \left( -A^{-1}_{lj}\right) \left( -A^{-1}_{ki}\right) 
    +  \left( -A^{-1}_{li}\right) \left( -A^{-1}_{jk}\right)
    + \mathcal{O}(J) \right] e^{-JA^{-1}J/2}\\
    J \rightarrow 0; \\
    \boxed{= A^{-1}_{ij} A^{-1}_{lk} + A^{-1}_{ik} A^{-1}_{lj} + A^{-1}_{il} A^{-1}_{jk} 
    = \left<x_i x_j x_k x_l\right>} \\
    = \left<x_i x_j\right> \left<x_k x_l\right>
    + \left<x_i x_k\right> \left<x_j x_l\right>
    + \left<x_i x_l\right> \left<x_k x_j\right>
$$

After all of that, let's throw ourselves wholeheartedly into the bookkeeping tricks, Wick contractions and diagrams!

:::

+++

---

## $\mathcal O(\lambda^1)$

\begin{align}
Z &= \int_{\mathbb{R}^N} \left[ \frac{1}{1!} \frac{-\lambda}{4!} \sum_i \frac{\partial^4}{\partial J^4}\right]
    \left[ \frac{1}{4!}(J\cdot x)^4 \right] e^{-xAx/2} \\
    &\equiv \left(\frac{1}{4!}\right)^2 \sum_{\mathcal{l}_1\mathcal{l}_2\mathcal{l}_3\mathcal{l}_4}^{N-1} G^0(\lambda) \\
\end{align}

\begin{align}
G^0(\lambda) &= \int_{\mathbb{R}^N} \frac{-\lambda}{(4!)^2} \sum_i \frac{\partial^4}{\partial J_i^4} 
J_{\mathcal{l}_1} J_{\mathcal{l}_2} J_{\mathcal{l}_3} J_{\mathcal{l}_4} x_{\mathcal{l}_1} x_{\mathcal{l}_2}
x_{\mathcal{l}_3} x_{\mathcal{l}_4} \\
     &= \int_{\mathbb{R}^N} \frac{-\lambda}{(4!)^2} \sum_i \delta_{(\mathcal{l}_1, i)}\delta_{(\mathcal{l}_2, i)}
     \delta_{(\mathcal{l}_3, i)}\delta_{(\mathcal{l}_4, i)}x_{\mathcal{l}_1} x_{\mathcal{l}_2}x_{\mathcal{l}_3} x_{\mathcal{l}_4} e^{-xAx/2}
\end{align}

In the $G^0(\lambda)$ case, the $J$ terms all have the same location on the position lattice. In other words, one factor of $\lambda$ collpases four $J$ terms to one index.

$$
= \int_{\mathbb{R}^N} \frac{-\lambda}{(4!)^2} x_ix_ix_ix_i e^{-xAx/2} \\
=  \frac{-\lambda}{(4!)^2} \left( A^{-1}_{ii}A^{-1}_{ii} \cdot 3\right)
$$

There's a question of whether or not that symmetry factor of three should be there or should be absent. I think it's supposed to be there, but I'm not sure.

+++

### [incomplete] $J^2$

$$
Z(\lambda, J^2)
$$

### $J^4$
$$
Z(\lambda, J^4) = \int_{\mathbb{R}^N} \left[ \frac{-\lambda}{4!} \sum_i \frac{\partial^4}{\partial J^4}\right]
    \left[ \frac{1}{4!} x_i x_j x_k x_l J_i J_j J_k J_l \right] e^{-xAx/2} \\
$$
Finally, there will be a way to contract indices without leaving any bubbles behind! We need to evaluate the following Wick contraction, and the most complicated one that will be in this notebook.

$$
\left< x_a x_a x_a x_a x_i x_j x_k x_l \right>
$$

That's $7!! = 105$ contractions, but far fewer unique diagrams. In fact, I can only concoct three topologically unique diagrams--are there more? And how do we divy them up? At this point, a concrete understanding of symmetry factors in the diagrammatic picture is essential.

+++

---

# Wick Contraction
$$
\braket{x_a x_a x_a x_a x_i x_j}
$$

Lots of duplicate contractions. Let's look at this numerically to save a headache. Give each $x_a$ a dummy index, then permute the combinations, and reduce them by removing the dummy index at the end.

$$
\braket{x_{a_1} x_{a_2} x_{a_3} x_{a_4} x_i x_j}
$$

```{code-cell} ipython3
import numpy as np
import itertools
points = ["xa1", "xa2", "xa3", "xa4", "x1", "x2"]

outarr = np.empty([6, len(points)], dtype="<U6")

def permuteprinter(in_arr, out_arr, depth=0):
    size = len(in_arr)
    for i, val in enumerate(in_arr):
        for j, val2 in enumerate(in_arr):
            out_arr[i, j] = val2
    return out_arr

permuteprinter(in_arr=points, out_arr=outarr)
```

```{code-cell} ipython3
import numpy as np

a = np.array(["BOY", "SMINEM", "COOL"], dtype="str")
b = np.zeros([len(a), 6], dtype=a.dtype) # set b's dtype to max length of a

for i, mot in enumerate(a):
    b[i, 3] = mot   

# Test char reassignment
def test_char_assignment(name="RYAN"):
    a = np.array(["BOY", "SMINEM", "COOL"], dtype="str")
    a[1] = name
    test_array = np.array(["BOY", "RYAN", "COOL"], dtype="str")
    assert np.all(np.char.equal(a, test_array))
    
test_char_assignment()

b[:, 3]
```

```{code-cell} ipython3
from scipy.special import factorial2
import warnings 

class WickContratctions:
    
    def __init__(self, vertices=1, endpoints=4):
        
        self.vs = vertices
        self.es = endpoints
        
        self._v_inds = "abcdefghijklmnopqrstuvwxyz"

        self.points = self.get_vertices(v=self.vs) + self.get_endpoints(self.es)
        self.N_point_function = len(self.points)
        self.num_contractions = int(factorial2(self.N_point_function - 1))
                    
    def get_vertices(self, v=1):
        inds = self._v_inds
        
        vs = []
        
        for ind in range(4 * v):
            vs.append(f"x{inds[ind//4]}")
        return vs

    def get_endpoints(self, e=4):
        inds = np.arange(len(self._v_inds))
        es = []
        
        for ind in range(e):
            es.append(f"x{inds[ind]}")
        return es

    def compute_contractions(self, mylist):
        
        if self.num_contractions > factorial2(16 - 1):
            raise ValueError(
                f"You're about to make a ({len(self.points)}, {self.num_contractions}) array. Aborting.")
        
        def _generate_permutations(mylist):
            if len(mylist) < 2:
                yield []
                return

            if len(mylist) % 2 == 1:
                raise ValueError
            else:
                a = mylist[0]
                for i, val in enumerate(mylist[1:]):
                    pair = [a, val]
                    for uncontracted in _generate_permutations(mylist[1: i+1] + mylist[i+2:]):
                        yield [pair] + uncontracted

        res = np.array([pair for pair in _generate_permutations(mylist)], dtype="<U6")
        return res

    def print_contractions(self, contractions=None, numprint=100):
        if contractions is None:
            if not hasattr(self, "contractions"):
                self.contractions = self.compute_contractions(self.points)
            
            contractions = self.contractions

        term = 0
        
        print(f"Total terms in the Wick contraction: {self.num_contractions}\n")
        for row in contractions:
            term += 1
            rowstring = f"{term}:\t"
            for pair in row:
                rowstring += (f"<{pair[0]},{pair[1]}> ")
            if term <= numprint:
                print(rowstring)
            elif term == (numprint + 1):
                warnings.warn(f"The Green's function has {self.num_contractions} terms.\n" 
                      +f"\tNot printing any more than {numprint}. \n To print more, "
                     + "change the default value of `numprint` in this function's call.")
                
    def _reduce_array(self, vertices=1):
        """Assumes vertex interactions are the first 4V terms of `points`."""

        delta = int(vertices * 4)
        print(f"Reducing vertices {self.points[:delta]}.")
        res = self.contractions.copy()
        
        for point in self.points[:delta]:
            res = np.char.replace(res, point, "a")
        return res
        
    def print_feynman_rules(self):
        
        warnings.warn("INCOMPLETE.")
        print(f"- {self.num_contractions} terms in the contraction.\n")
        print(f"- (-\lambda / 4!)^{self.vs} / {self.vs}!" + "\t : $\lambda$ expansion.")
        print(f"- {1}/{self.es}!" + "\t\t\t\t : $Jx$ expansion.")
        print("")
        
    def print_symmetry_factors(self):
        raise NotImplementedError
    
# points = ["xa1", "xa2", "xa3", "xa4", "x1", "x2", "x3", "x4"]
WC = WickContratctions(vertices=1, endpoints=4)
WC.print_contractions(numprint=12)
pairs = WC.contractions
```

```{code-cell} ipython3
A = pairs[3]
B = pairs[6]
A, B
```

```{code-cell} ipython3
A.sort(axis=1)
A
```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```
