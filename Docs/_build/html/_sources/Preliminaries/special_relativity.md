---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

# Special Relativity

We choose the "east coast metric" here for its natural ubiquity in gravity. For the purposes of other notebooks, this might be changed, but in general it doesn't matter--sticking to one too dogmatically will make consulting certain books, and talking to certain people, difficult.

For an impassioned argument in favor of the east coast metric, see Peter Woit's [blog post](https://www.math.columbia.edu/~woit/wordpress/?p=7773) (and especially the comments).

\begin{equation}
g_{\mu \nu} = \begin{pmatrix}
-1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{pmatrix}
\end{equation}

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt
```

```{code-cell} ipython3
# West Coast, baby
g_mn = np.diag([-1, 1, 1, 1])
g_mn
```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```
