---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

# Math Toolkit

+++

There are some mathematical tools that you'll need to be firmly in control of to tackle real field theory problems. Here I'll only go over broad points and give a sample problem or two; the real required level of maturity with these techniques will be far more sophisticated than those on this page.

+++

# Fourier Transforms

## FT/IFT Conventions

\begin{align}
\text{FT}: \quad g(\mathbf{k}) &= \int^{\infty}_{\infty} f(\mathbf{x})  
    e^{i\mathbf{k\cdot x}} d^d\mathbf{x} \\
g(\omega) &= \int^{\infty}_{\infty} f(t)   e^{-i\omega t} dt\\
\text{IFT}: \quad f(\mathbf{x}) &= \frac{1}{(2 \pi)^d} \int^{\infty}_{\infty} g(\mathbf{k})
    e^{-i\mathbf{k\cdot x}} d^d\mathbf{k} \\
f(t) &= \frac{1}{(2 \pi)}\int^{\infty}_{\infty} g(\omega) e^{i\omega t} d\omega
\end{align}

With this convention, the two sets of $(d+1)$ integrals can be combined simply into one equation, as is often done in relativistic QFT in Minkowski spacetime.

\begin{align}
\text{FT}: \quad g(k^\mu) &= \int^{\infty}_{\infty} f(x)  
    e^{ik_\mu x^\mu} d^{(d+1)}x^\mu \\
\text{IFT}: \quad g(k^\mu) &= \int^{\infty}_{\infty} f(x^\mu)   e^{-i k_\mu x^\mu} d^{(d+1)}k\\
\end{align}

So far we haven't chosen a metric.

$$
kx \equiv \mathbf{k \cdot x} \equiv k_\mu x^\mu = g_{\mu \nu} k^\mu x^\nu
$$

If we work with the Minkowski metric, there are two main choices: $(1, -3)$, with positive time component and negative space components, or $(-1, 3)$, with negative time component and positive space components. One reason I like the FT sign conventions above is that I prefer the $(-1, 3)$ metric; defined this way, the phase of a plane wave is 

$$
\psi(x^\mu) \sim e^{i k_\mu x^\mu} = e^{i (kx - \omega t)} 
$$

+++

### A brief detour on plane waves

The difference between a forward and backward propagating wave is not an overall plus or minus sign, but a difference between the space and time components. Consider a plane wave in quantum mechanics.

\begin{align}
\left< p | p |x \right> &= p^\dagger \left< p | x \right> \text{(operate } \leftarrow \text{)} \\
                        &=  \frac{\hbar}{i} \left< p \bigg| \frac{\partial}{\partial x} \bigg| x \right> \\
                        &= \frac{\hbar}{i} \frac{\partial}{\partial x} \left< p | x \right> 
                                                            \text{(operate } \rightarrow \text{)} \\
            \int \frac{d \left< p | x \right>}{\left< p | x \right>}  &= \frac{-ip}{\hbar} \int dx \qquad p^\dagger = p
\end{align}

$$
\boxed{\left< x | p \right> = e^{ipx}}
$$

```{code-cell} ipython3
import numpy as np
from ipywidgets import interact
import matplotlib.pyplot as plt
%matplotlib inline 

N = 256

def cos_propagate(x, t, k=1, w=1, signature=1):
    theta = signature * (k * x - w * t)
    return np.exp(1j*theta)

@interact(X=(-5, 5, 0.1),
          T=(-5, 5, 0.1),
          w=(-5, 5, 0.1),
          k=(-2, 2, 0.1),
          signature=[('(-1, 3)', 1), ('(1, -3)', -1)])
def plot_cos(X=0, T=0, w=1, k=1, signature=1):
    fig, ax = plt.subplots(1, 2, figsize=(10, 5))

    xs = np.linspace(-10, 10, N)
    ts = np.linspace(0, 10, N)
    
    ax[0].plot(xs, cos_propagate(x=xs, t=T, k=k, w=w, signature=signature).real)
    ax[0].set(xlabel="xs", ylabel="A", title=f"cos(kx-wt), t={T:1.4f}")
    ax[1].plot(ts, cos_propagate(x=X, t=ts, k=k, w=w, signature=signature).real)
    ax[1].set(xlabel="ts", ylabel="A", title=f"cos(kx-wt), x={X:1.4f}")
    plt.tight_layout();
```

# Green's Functions (heuristic)

\begin{align}
\left(\Box + m^2 \right) \psi \left(x \right) &= J\left(x \right) \\
\left(\Box + m^2 \right) G \left(x, y \right)  &= \delta \left(x - y \right) \\
\left(\Box + m^2 \right) G \left(x, y \right) J \left(y \right) &= \delta \left(x - y \right) J \left(y \right) \\
\int dy \left(\Box + m^2 \right)G \left( x, y \right) J \left(y \right) &= \int dy \delta \left(x - y \right) J \left(y \right)\\
\left(\Box + m^2 \right) \int dy G \left(x, y \right) J \left((y \right) &= J \left(x \right)\\
\rightarrow \int dy G \left(x, y \right) J \left(y \right) &= \psi \left(x \right)
\end{align}

```{code-cell} ipython3

```
