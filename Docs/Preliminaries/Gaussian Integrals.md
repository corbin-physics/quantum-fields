---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

# Gaussian Integrals

The standard integrable scalar field has a Lagrangian that is quadratic in the field. Because the path integral involves the exponentiation of the action, which is in turn related to the Lagrangian (density), a large part of basic field theory relies on knowing how to deal with gaussian integrals. 

Here I'll review the standard 1D gaussian integral, while slowly modifying it to approach something more like what one encounters in field theory. Later I will consider multiple dimensional integrals, which are much closer to the sum over all paths interpretation of functional integration.

In what follows, a single integral over all space will be written with the shorthand
$$
\int_{-\infty}^{\infty}dx \equiv \int_\mathbb{R}  
$$

and an integral over N coordinates will be written

$$
\int_{-\infty}^{\infty}\int_{-\infty}^{\infty}...\int_{-\infty}^{\infty}dx_1 dx_2...dx_n \equiv \int_{\mathbb{R}^N} 
$$

---

## Gaussian 1D

## Expectation values
The expectation value of the coordinate $x^{2n}$ evaluated inside the gaussian can be computed for all $n$ by differentiating under the integral sign. Since derivatives commute with integration, the derivatives can be pulled outside the integrand, rendering the integral problem into a problem in derivation:

\begin{align}
\int_{-\infty}^{\infty}dx \; x^{2n} e^{-\alpha x^2} &= 
    \left(- \frac{ \partial }{ \partial \alpha } \right)^n \sqrt{\frac{\pi}{\alpha}} \\
    &= \sqrt{\frac{\pi}{\alpha}}\frac{(2n-1)!!}{(2\alpha)^n} \\
    &\equiv \left< x^{2n} \right>
\end{align}

```{code-cell} ipython3
%matplotlib inline
import numpy as np, matplotlib.pyplot as plt

def integrand(x, n=0, m=1):
    """Return the integrand for a gaussian integral with a factor of x^2n in the argument."""
    return (x**(2*n)) * np.exp(- (m * x)**2 / 2)

xs = np.linspace(-5, 5, 100)
dx = np.diff(xs)[-1]

m = 1.23

fig, ax = plt.subplots(figsize=(12,6))

for n in np.arange(0, 5):
    
    x_n0s = integrand(x=xs, n=n, m=m)
    I = np.trapz(y=x_n0s, x=xs)
    ax.plot(xs, x_n0s, label=f"{n=}: {I=:1.4f}")
    plt.legend();
    
```

```{code-cell} ipython3
def double_factorial(n):
    """
    Return the double factorial of n using recursion.
    scipy.special.factorial2 no longer returns 1 for (-1)!!
    """
    
    if n <= 1:
        return 1
    else:
        return n * double_factorial(n-2)

def gaussian_int_analytic(a, d=0):
    """Expresses the integral of q^{2n} e^{-aq^2} dq in terms of derivatives of the gaussian."""
    res = (np.sqrt(np.pi / a) * double_factorial(int(2 * d  - 1)) / (2 * a)**d)  
    return res

m = 1.23

xs = np.linspace(-100, 100, 10000)
ints_analytic = []
ints_numeric = []

for _order in np.arange(0, 10):
    ints_numeric.append(np.trapz(integrand(x=xs, n=_order, m=m), xs))
    ints_analytic.append(gaussian_int_analytic(a=m**2/2, d=_order))
for _numeric, _analytic in zip(ints_numeric, ints_analytic):
    assert np.allclose(_numeric, _analytic)
print("Function `gaussian_int_analytic` is working as expected.")
```

```{code-cell} ipython3
import scipy
from scipy.special import factorial, factorial2
print(f"{scipy.__version__=}, {factorial2(-1)=}")

# test double factorial
_ns = np.arange(0, 10)
_facts = [1, 1, 2, 3, 8, 15, 48, 105, 384, 945, 3840]
assert np.all([np.allclose(double_factorial(_n), _facts) for _n, _facts in zip(_ns, _facts)])
```

## Common 1D rescalings

Very often, more complicated gaussians look like rescalings of the above problem. First let's look at the integral obtained by adding a linear term to the exponential.

\begin{align}
\int_{\mathbb{R}} e^{-\alpha q^2 + Jq} &= \int_{\mathbb{R}} \exp\left[-\left(\sqrt{\alpha} q - \frac{J}{2\sqrt{\alpha}}\right)^2 + \frac{J^2}{4\alpha} \right] \\
    &= \exp\left(\frac{J^2}{4\alpha}\right) \int_{\mathbb{R}} \exp\left[-\left(\sqrt{\alpha} q - \frac{J}{2\sqrt{\alpha}}\right)^2 \right] \\
    &= \frac{1}{\sqrt{\alpha}}\exp\left(\frac{J^2}{4\alpha}\right) \int_{\mathbb{R}} \exp\left[-y^2 \right] \qquad y \equiv \sqrt{\alpha}x - \frac{J}{2\sqrt{\alpha}} \\
\end{align}

$$
\boxed{\int_{\mathbb{R}} e^{-\alpha q^2 + Jq} = \sqrt{\frac{\pi}{\alpha}}\exp\left(\frac{J^2}{4\alpha}\right)}
$$

Rescaling $J$ or $\alpha$ will give other useful integrals. Let $J\rightarrow iJ$. Then

$$
\boxed{\int_{\mathbb{R}} e^{-\alpha q^2 + iJq} = \sqrt{\frac{\pi}{\alpha}}\exp\left(\frac{-J^2}{4\alpha}\right)}
$$

If $J\rightarrow iJ$ and $\alpha\rightarrow -i\alpha$,

$$
\boxed{\int_{\mathbb{R}} e^{i\alpha q^2 + iJq} = \sqrt{\frac{i\pi}{\alpha}}\exp\left(\frac{-iJ^2}{4\alpha}\right)}
$$

and so on.

+++

## In multiple dimensions

+++

Adapting a shorthand notation for the arbitrarily many integrals over $q_i$,

$$
\int^{\infty}_{-\infty}...\int^{\infty}_{-\infty}dq_1...dq_N \equiv \int_{\mathbb{R}^N}
$$

we want to consider the multivariate gaussian integral below.

$$
I(A) = \int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{q}^\intercal \mathbf{A} \mathbf{q} \right)} 
$$

The trick is to diagonalize $\mathbf A$ to transform the messy multivariable gaussian into a product of $N$ 1D gaussians.

If $\mathbf q$ transforms as $x = \mathbf{Oq}$,

\begin{align}
\mathbf{q^\intercal Aq} &= \underbrace{\mathbf{q^{\intercal} O^\intercal}}_{\mathbf{x^\intercal}} 
                        \underbrace{\mathbf{O A O^\intercal}}_{\mathbf{D}}
                        \underbrace{\mathbf{O q}}_{\mathbf{x}} \\
                        &= \mathbf{x^\intercal D x}; \qquad \mathbf{D} \text{ diagonal}
\end{align}

Each integral can be evaluated as a 1D gaussian as above, giving the final result as a product over each $\mathcal{x}$ integral.

\begin{align}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Det}{Det}
I(A) &= \int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{x}^\intercal \mathbf{D} \mathbf{x} \right)} \\
     &= \prod_{i=0}^N \sqrt{\frac{2\pi}{D_{ii}}} \\
     &= \sqrt{\frac{(2\pi)^N}{\Tr{\mathbf{D}}}} = \sqrt{\frac{(2\pi)^N}{\Det{\mathbf{D}}}} \\
     &= \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{O^\intercal AO)}}}} = \
                 \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{O^\intercal})}\Det{(\mathbf{A})}\det{(\mathbf{O})}}} \\
     &= \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{A})}}}
\end{align}

- *Note: The determiant of an orthogonal rotation matrix (without reflections) is $\Det{(O)} = 1$.*

A more convincing picture: The operator $\mathbf D$ operates on the vector $\mathbf x$ and returns its eigenvalue, $\lambda_i$. This eigenvalue is what ends up in the denominator after integration. Then, 

\begin{align}
     &= \sqrt{\frac{(2\pi)^N}{\prod_{i=0}^N{\lambda_i}}} \\
         &= \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{A})}}}, \qquad \Det{(\mathbf{A})} = \prod_{i=0}^N \lambda_i
\end{align}

---

In summary, the multivariable gaussian integral is

$$
\boxed{I(A) = \int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{q}^\intercal \mathbf{A} \mathbf{q} \right)}  = \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{A})}}}}
$$

Now that we have the analytic form of the integral, modifications are as easy as they were in the 1D case to obtain.

\begin{align}
I(A) = \int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{q}^\intercal \mathbf{A} \mathbf{q} + \mathbf{Jq} \right)} \\
    = \exp{\left(-\frac{1}{2}\mathbf{x}^\intercal \mathbf{D} \mathbf{x} + \mathbf{(JO)x} \right)} \\
    = \exp{\left(-\frac{1}{2}\mathbf{x}^\intercal \mathbf{D} \mathbf{x} + \mathbf{(JO)x} \right)} \\
= \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{A})}}} \exp{((\mathbf{JO})^\intercal \mathbf{D^{-1}} (\mathbf{JO}/2)}) \\
\exp{((\mathbf{JO})^\intercal \mathbf{D^{-1}} (\mathbf{JO}/2)}) &= \exp{(\mathbf{O^\intercal J^\intercal O A^{-1} O^{\intercal} JO}/2)} \\
\rightarrow &= \exp{(\mathbf{J^\intercal A^{-1} J}/2)}
\end{align}


$$
\boxed{\int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{q}^\intercal \mathbf{A} \mathbf{q} + \mathbf{Jq} \right)} = \
    \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{A})}}} \exp{(\mathbf{J^\intercal A^{-1} J}/2)}}
$$

For another variation, let $\mathbf{J} \rightarrow \mathbf{iJ}$:

$$
\boxed{\int_{\mathbb{R}^N} 
    \exp{\left(-\frac{1}{2}\mathbf{q}^\intercal \mathbf{A} \mathbf{q} + \mathbf{iJq} \right)} = \
    \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{A})}}} \exp{(-\mathbf{J^\intercal A^{-1} J}/2)}}
$$

Let $\mathbf{J} \rightarrow \mathbf{iJ}$ and $\mathbf{A} \rightarrow \mathbf{iA}$:

$$
\boxed{\int_{\mathbb{R}^N} 
    \exp{\left(-\frac{\mathbf{i}}{2}\mathbf{q}^\intercal \mathbf{A} \mathbf{q} + \mathbf{iJq} \right)} = \
    \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{iA})}}} \exp{(-i\mathbf{J^\intercal A^{-1} J}/2)}}
$$

Note that it is possible to write the overall normalization factor as below, depending on taste.

$$
\sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{iA})}}} =\Det{\left(\frac{\mathbf{A}}{-2\pi \mathbf{i}}\right)}^{-1/2}
$$

The factor of $([-\mathbf{i}] 2 \pi)^N$ will reappear from the $N$ element multiplication in the determinant operation.

+++

--- 

# Example: covariance matrix

The matrix $\mathbf{A}$ in the above integrals is the **inverse** covariance matrix to a multivariate gaussian distribution. We can generate a sample distribution and calculate the covariance matrix to check the first integral above.

- [Covariance Matrix](https://en.wikipedia.org/wiki/Covariance_matrix)

$$
A = \begin{pmatrix}
\sigma^2(x,x) & \sigma^2(x,y) & \sigma^2(x,z) \\
\sigma^2(y,x) & \sigma^2(y,y) & \sigma^2(y,z) \\
\sigma^2(z,x) & \sigma^2(z,y) & \sigma^2(z,z)
\end{pmatrix}
$$

### Variance vs Standard Deviation
I've used the notation $\sigma^2(x, x)$ for the variance of $x$ with $x$, which is usually written just $\sigma^2(x) = \text{Var}(x) = V(x)$. I like this because it makes it easier to see what the *covariance* matrix is--it's the generalized variance, which in turn was the square of the standard deviation $\sigma(x)$. The single variable idea of a variance is then represented on the diagonals of the covariance matrix, and $\sqrt{\sigma^2(x,x)} = \sigma(x)$, the statistical waist for the 1D gaussian distribution. 

---
Note that to properly represent the covariance matrix, $\mathbf{A}$ should be defined as $\mathbf{A^{-1}}$ in the equations above. I've kept the old way to stay consistent with Zee's child problem in *QFT Nut* for now, but I might change this later. It's better to keep good faith with the statistical language than with a toy problem.

\begin{align}
I(A) = \int_{\mathbb{R}^N}
    \exp{\left(-\frac{1}{2}\mathbf{q}^\intercal \mathbf{A}^{-1} \mathbf{q} \right)}  
    &= \sqrt{\frac{(2\pi)^N}{\Det{(\mathbf{A^{-1}})}}} \\
    &= \sqrt{(2\pi)^N \Det{(\mathbf{A})}}
\end{align}

$$
\mathbf{A} \vec{v}_i = \lambda_i \mathbb{1} \vec{v}_i \qquad \text{Det}(\mathbf{A}) = \prod_{i=0}^N \lambda_i 
$$

```{code-cell} ipython3
# :tags: [hide-cell]

from scipy import integrate

rng = np.random.default_rng(233)

class Base:
    def __init__(self, **kws):
        for kw in kws:
            if not hasattr(self, kw):
                raise ValueError
            else:
                setattr(self, kw, kws[kw])


class MvarExplorer(Base):
    """Sample class for managing variable namespaces."""
    
    Lmax = 5
    N = 1000
    
    def __init__(self, sigmas=[0.2323, 0.7676], **kws):
        
        super().__init__(**kws)
        
        self.sigmas = sigmas
        self.d = len(self.sigmas)
        self.xs = np.linspace(-self.Lmax, self.Lmax, self.N)
        self.dx = self.xs[1] - self.xs[0]
        
        self.init()
    
    def init(self):
        self.dist = np.array([rng.normal(size=self.N, scale=std) for std in self.sigmas])
        self.A = np.cov(self.dist, bias=True)
        self.lams, self.vs = np.linalg.eig(self.A)

    def gaussian1D(self, sigma, integrated=False):
        if integrated is True:
            return np.sqrt(2 * np.pi) * sigma
        else:
            exponent = - self.xs**2 / sigma**2 / 2
            return np.exp(exponent)
    
    def integ_mvar(self):
        assert np.allclose(np.prod(self.lams), np.linalg.det(self.A))
        return np.sqrt((2 * np.pi)**self.d * np.prod(self.lams))
    
    def _integrate(self, y):
        """Integrate a function f over the class abscissa."""
        return integrate.trapezoid(y=y, x=self.xs, axis=0)
    
    def get_gaussians(self, sigmas=None):
        if sigmas is None:
            sigmas = np.sqrt(np.diag(self.A))
        
        if not hasattr(sigmas, "__len__"):
            sigmas = np.array([sigmas])
        res = [self.gaussian1D(sigma=s) for s in sigmas]
        return np.array(res)
                               
    def _checks(self):

        assert np.allclose(np.linalg.det(self.A), np.prod(self.lams))
        print("det(A) = prod(lams)")
        atol = 1e-2
        assert np.allclose(np.sqrt(self.A.diagonal()), self.sigmas, atol=atol)
        print(f"diag(sqrt(A)) = sigmas, {atol=}")

    def plot(self):
        origin = np.array([[0,0],
                   [0,0]])

        integrals = []

        fig, axs = plt.subplots(1, 2, figsize=(8,4))
        gs = self.get_gaussians(sigmas=self.sigmas)
        plt.sca(axs[0])
        for g, sigma in zip(gs, self.sigmas):
            integ = self._integrate(y=g)
            integrals.append(integ)
            axs[0].plot(self.xs, g, label=fr"$\sigma$={sigma:1.4f}")
        # title = r"$\int_\mathbb{R}$=" + f"{np.prod(integrals:1.4f)}"
        axs[0].set(xlabel="xs", ylabel="g(xs)", title=f"int={np.prod(integrals):1.4f}")
        plt.legend();


        ax = axs[1]
        data = self.dist[:2,:10000]
        ax.scatter(*data, alpha=0.2)
        scatter_params = dict(xlabel=fr"$x_1$", ylabel=fr"$x_2$", aspect=1)
        ax.set(**scatter_params);
        # ax.quiver(*origin, vs[:,0], vs[:,1], scale=10);
        arrow_params = dict(head_width=0.2, head_length=0.2, linewidth=1.5)
        for i, vec in enumerate(self.vs.T[:,:2]):
            ax.arrow(0, 0, *vec, **arrow_params)
        ax.set(title=f"int_mvar={self.integ_mvar():1.4f}")
        plt.tight_layout();
        
```

```{code-cell} ipython3
M = MvarExplorer(N=10000, sigmas=rng.random(5))
# M._checks()
M.plot()
```

```{code-cell} ipython3
def integrate_gaussian_1Ds(state, method="trapezoid"):

    def gaussian1d(x, sigma=1):
        exponent = - x**2 / sigma**2 / 2
        return np.exp(exponent)
    
    integ_list = []
    
    stds = state.sigmas
    xs = state.xs
    
    if method == "trapezoid":
        for sigma in stds:
            g_ = gaussian1d(x=xs, sigma=sigma)
            integ_list.append(integrate.trapezoid(y=g_, x=xs))
            
    else:
        for sigma in stds:
            g_ = integrate.quad(gaussian1d, a=-np.inf, b=np.inf, args=(sigma))
            integ_list.append(g_[0])
    
    return np.prod(integ_list)

%timeit integrate_gaussian_1Ds(state=M, method="trapezoid")
%timeit integrate_gaussian_1Ds(state=M, method="quad")

I_T = integrate_gaussian_1Ds(state=M, method="trapezoid")
I_Q = integrate_gaussian_1Ds(state=M, method="quad")
np.allclose(I_T, I_Q)
```

`scipy.integrate.trapezoid` is the clear winner in 1D if we already have the integral evaluated on some abscissa.

```{code-cell} ipython3

```
