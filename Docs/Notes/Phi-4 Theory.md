---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

# $\phi^4$ Theory

This page contains resources on $\phi^4$ field theory, the theory of a self interacting scalar quantum field. This is a simple yet rigorous field theory, and its thorough analysis takes one through all of the major machinery of field theory.

- The [Wikipedia entry](https://en.wikipedia.org/wiki/Quartic_interaction) is as good a place to start as any. There's not much depth though.
- Coleman's [Aspects of Symmetry]() has a discussion of $\phi^4$ in his chapter "1/N". He introduces a new term to the lagrangian ([auxiliary field](https://en.wikipedia.org/wiki/Auxiliary_field)) that doesn't change the dynamics but does change the Feynman rules. Integrating over one set of fields to obtain an EFT.
- 

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3

```
