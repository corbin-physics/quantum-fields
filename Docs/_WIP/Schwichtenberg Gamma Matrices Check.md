---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

# Some-Nonsense Quantum Field Theory
This notebook started as a check for the gamma matrices in the basis chosen in Jakob Schwichtenberg's *No Nonsense Quantum Field Theory*. But as I went through more of the book, I realized that it likely has simply not been edited by anyone but him. I gave up the idea of sending them in, but the check is below anyway.

Some gripes:

"This is completely analogous..." etc. They're not "analogous", they're **THE SAME**.

```{code-cell} ipython3
%matplotlib inline
from matplotlib import pyplot as plt
import numpy as np

from importlib import reload
import logging
logger = logging.getLogger()

import clifford
```

```{code-cell} ipython3
s = clifford.PauliMatrices()

class JSchGammas:
    """
    Create an object containing the gamma matrices as class attributes.
    
    These are in the chiral (Weyl?) basis, taken straight from page 195 of 
    Jakob Schwichtenberg's "No-Nonsense Quantum Field Theory". 
    
    It's worth noting that the gamma_x1 matrix does not follow the usual rules
    for its definition, and differs from gamma_x2 and gamma_x3.
    """
    
    gamma = 1j * s.y
    assert np.allclose(gamma.imag, 0)
    gamma = gamma.real
    
    x0 = np.kron(s.x, s.I)
    x1 = np.kron(s.x, s.x) # book x1
    #x1 = np.kron(gamma, s.x) # expected x1
    x2 = np.kron(s.y, s.z)
    x3 = np.kron(gamma, s.z)
    assert np.allclose(x3, 1j * x2)
    xj = np.array([x1, x2, x3])
    mu = np.array([x0, x1, x2, x3])
    
gJ = JSchGammas()
```

# BUG HUNT : JSch gamma representation


The above is getting too messy, so let's start from scratch. Reproducing exactly what is in the book at page 195 (eq 5.56):

$$
\gamma^0 = 
\begin{pmatrix}
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 \\
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0
\end{pmatrix} \qquad
\gamma^1 = 
\begin{pmatrix}
0 & 0 & 0 & 1 \\
0 & 0 & 1 & 0 \\
0 & 1 & 0 & 0 \\
1 & 0 & 0 & 0
\end{pmatrix}
$$

$$
\gamma^2 = 
\begin{pmatrix}
0 & 0 & -i & 0 \\
0 & 0 & 0 & i \\
i & 0 & 0 & 0 \\
0 & -i & 0 & 0
\end{pmatrix} \qquad
\gamma^3 = 
\begin{pmatrix}
0 & 0 & 1 & 0 \\
0 & 0 & 0 & -1 \\
-1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0
\end{pmatrix}
$$

Then (verified below) : 
$$
\begin{gather}
\gamma^0 = \sigma^x \otimes \mathbf{I} \\
\gamma^1 = \sigma^x \otimes \sigma^x \\
\gamma^2 = \sigma^y \otimes \sigma^z \\
\gamma^3 = i \sigma^y \otimes \sigma^z = i \gamma^2\\
\end{gather}
$$

This is not the expected result for $\gamma^{k}, k=[1,2]$, which most other sources show as (in both the mass and chiral bases):

$$
\gamma^k_{\text{standard}} = \begin{pmatrix}
0 & \sigma^k \\
-\sigma^k & 0 \\
\end{pmatrix}
$$

```{code-cell} ipython3
gJ.mu
```

Notice that these are wildly different than the normal definition of the chiral basis in other books. 

On page 211, eqn 5.97, the matrix used to transform from this chiral to the mass basis is given as below.

$$
N = \frac{1}{2}
\begin{pmatrix}
1 & 0 & 1 & 0 \\
0 & 1 & 0 & 1 \\
1 & 0 & -1 & 0 \\
0 & 1 & 0 & -1
\end{pmatrix}
$$

This is expressed in the present code in terms of $\sigma^i$ and appropriate kronecker products.

$$ N = \frac{1}{2}(\sigma^x + \sigma^z) \otimes \mathbf{I}$$ 

A transformation of $\gamma^0$ to the mass basis means a rotation,
$$
\gamma^{'\mu} = N \gamma^{\mu} N^{-1} \\
\text{But } N^{-1} = 2N, \\
\gamma^{'\mu} = 2 N \gamma^{\mu} N \\
$$

This leads to a small typo on page 212, where a factor of $1/2$ is forgotten. Verify eqn 5.99 below.

```{code-cell} ipython3
N = 0.5 * np.kron((s.x + s.z), s.I)
2 * N
```

```{code-cell} ipython3
def chiral_to_mass(g):
    return 2 * N @ g @ N

# eqn. 5.99
print(gJ.x0)
print(chiral_to_mass(gJ.x0))
```

So far this looks good! Since the $\gamma^0$ term is not affected by raising or lowering indices with the west coast metric (1,-3), there's no need to worry about sign problems here. The "spatial" terms are the ones I'm concerned about though. (5.100) is the next to check.

```{code-cell} ipython3
# from IPython.display import display, Math

g_i = [gJ.x1, gJ.x2, gJ.x3]
g0_mass, g1_mass, g2_mass, g3_mass = map(chiral_to_mass, gJ.mu)

print(f"$\gamma^0_m$ : \n{g0_mass}, \n")
print(f"$\gamma^1_m$ : \n{g1_mass}, \n")
print(f"$\gamma^2_m$ : \n{g2_mass}, \n")
print(f"$\gamma^3_m$ : \n{g3_mass}, \n")
```

Except for $\gamma^3$, which is off by a minus sign, these are nowhere near the matrices in (5.100). That equation matches everything I'd expect to find from other books, but the above are nothing near.

**Dirac Representation above**:

$$
\begin{gather}
\gamma^1 = \sigma^z \otimes \sigma^x \\
\gamma^2 = - \sigma^y \otimes \sigma^z \\
\gamma^3 = - i \sigma^y \otimes \sigma^z \\
\end{gather}
$$

```{code-cell} ipython3
assert np.allclose(np.kron(s.z, s.x), g1_mass)
assert np.allclose(np.kron(-s.y, s.z), g2_mass)
assert np.allclose(np.kron(-1j*s.y, s.z), g3_mass)
```

## Bases as Expected
Dirac and Weyl bases as seen in Langacker, Coleman, etc.

### Dirac (mass)
$$
\gamma^0 = \begin{pmatrix}
\mathbf{I} & 0 \\
0 & -\mathbf{I}
\end{pmatrix} \qquad
\gamma^k = \begin{pmatrix}
0 & \sigma^k \\
- \sigma^k & 0
\end{pmatrix}\qquad (2.164)
$$
### Weyl (chiral)
$$
\gamma^0 = \begin{pmatrix}
0 & \mathbf{I} \\
\mathbf{I} & 0
\end{pmatrix} \qquad
\gamma^k = \begin{pmatrix}
0 & \sigma^k \\
- \sigma^k & 0
\end{pmatrix}\qquad (2.165)
$$
---

Below is a test routine that will take an object with the properties x0, x1, x2, x3 and loop over the indices to verify some of the basis independent gamma matrix relations. If these fail for the definition of the chiral basis given on page 195, the problem is found.

```{code-cell} ipython3
logger.setLevel(logging.DEBUG)

from clifford import anticommutator, commutator

def get_metric(convention="particle"):
    res = np.array([1, -1, -1, -1])
    
    if convention == "particle":
        pass
    elif convention == "gravity":
        res *= -1
    return np.diag(res)

metric = get_metric("particle")

def test_gamma_matrix_properties(g):
    inds = ["x0", "x1", "x2", "x3"]
    I4 = np.kron(s.I, s.I)
    
    fails = 0
    
    for _mu, _ind1 in enumerate(inds):
        g_mu = getattr(g, _ind1)
        if not np.allclose(np.trace(g_mu), 0):
            logging.debug(f"tr(g_{_ind1}) != 0")
        
        for _nu, _ind2 in enumerate(inds):
            
            eta = metric[_mu, _nu]
            
            g_nu = getattr(g, _ind2)
            
            if not np.allclose(np.trace(g_mu @ g_nu), 4 * eta):
                logging.debug(f"tr(g_{_ind1} g_{_ind2}) != {4 * eta}")
                fails += 1
                
            if _mu < _nu:
                if not np.allclose(anticommutator(g_mu, g_nu), 0):
                    logging.debug(f"anticommute g_{_ind1} g_{_ind2} fails.")
                    fails += 1
                    
            elif _mu == _nu:
                if not np.allclose(anticommutator(g_mu, g_nu), 2 * eta * I4):
                    logging.debug(f"[g_{_ind1}, g_{_ind2}]_A fails.")
                    fails += 1

    if fails > 0:
        logging.debug(f"{fails=}")
        raise AssertionError
    else:
        pass
```

### Test : Dirac / Weyl
As a quick check, the above properties are tested on my implementation of the gamma matrices in both the Dirac and Weyl bases.

```{code-cell} ipython3
g_dirac = clifford.GammaMatrices(basis="Dirac")
g_weyl  = clifford.GammaMatrices(basis="Weyl")

test_gamma_matrix_properties(g_dirac)
test_gamma_matrix_properties(g_weyl)
```

### Test : Weyl (Schwichtenberg)
Perform the same test as above for the matrices as defined on page 195.

```{code-cell} ipython3
test_gamma_matrix_properties(gJ)
```

Right away, the commutation relation between $\gamma^0$ and $\gamma^1$ fails. The transformation of the spatial $\gamma$ matrices in *No-Nonsense* between bases suggests that whatever's going in the book _is_ deliberate, but I don't understand it.

## Draw up
Is it possible that JS is automating some of his matrix evaluations, and got the order of the kronecker product wrong?

Check $\gamma \otimes \sigma^k$ and $\sigma^k \otimes \gamma$.

-- no dice; it was worth a shot though.

```{code-cell} ipython3
gamma = (1j * s.y).real
print(gamma)

print(np.kron(gamma, s.x), "\n", np.kron(s.x, gamma))
```

---

$$
N N = \frac{1}{2}\mathbf{I} \\
2 N N = \mathbf I \\
2 N = \mathbf I N^{-1} \\
2 N = N^{-1}
$$

```{code-cell} ipython3
N = 0.5 * np.kron((s.z + s.x), s.I)

def transform(g):
    return 2 * (N @ g @ N)

def test_lower_ind_transform(g):
    for _i, _gj in enumerate(g.mu[1:]):
        _gj_lower = - _gj
        assert np.allclose(_gj_lower, transform(_gj))
        print(_gj, "\n", transform(_gj), "\n")

test_lower_ind_transform(g_dirac)        

# The above assert passing implies that the only transformation done to the 
# \gamma^k matrices is to hit them with a minus sign. Ideally they wouldn't
# change at all...
```

```{code-cell} ipython3
test_lower_ind_transform(gJ)        
```

$$
N \gamma^{0} N^{-1} = \gamma^{0}{'}
$$

```{code-cell} ipython3
Es, Vs = np.linalg.eig(gJ.x0)

assert np.alltrue([np.allclose(_E * Vs[:,_i], gJ.x0 @ Vs[:,_i]) for _i, _E in enumerate(Es)])
Es
```

$$
\newcommand{\slash}[1]{\not\phantom{\!\!}{#1}}
$$
$$
\slash D \slash P \slash \gamma
$$

```{code-cell} ipython3
assert np.allclose(transform(g_dirac.x0), gJ.x0)
for _i in ["x1", "x2", "x3"]:
    print(f"{transform(getattr(gJ, _i))}")
    print(f"{getattr(g_dirac, _i)}", "\n")
```

# Some other comments

I have some other comments and catches. Some of these might be ignorable, but I've tried to keep this at the level of typos and breaks in consistency. Hopefully these are helpful. In general, if there is ever a second edition, it might be nice to have a scrupulous editor help out.

**Book edition 1.4**

- p.214 : (5.107) Factor of 1/2 vs 1/4 in appendix A.1 should be explained (rescaling of lagrangian; I assume the second term could be written $\sim \frac{1}{2}m^2A \wedge A$). I think this is to make varying the Dirac Lagrangian give something without a prefactor, in which case it should be a factor of $1/4$...
- p.219 : (5.118) $\partial_sigma$ (missing \ in $\sigma$)
- p.231 : Footnote 74 is travelling in the $\hat y$ direction, while the eqn it refers to travels in $\hat z$. Not too important, but inconsistent.
- p. 231 : Same issue with $k_z = 0$ in (5.152). Should be $(1, 0, 0, 1)$, not $(1, 0, 1, 0)$.
- p. 232 : All matrices should have $(1, 0, 0, 1)$.
- p. 261 : Which terms the potential depends on should be made consistent. ($V'(\psi, \phi)$, for instance).
- p. 265 : Minus sign in eq. (6.63)? These are always hard for me to keep track of.
$$
\begin{gather}
\slash D_\mu = \partial_\mu - i e A_\mu \\
(i\slash D - m)\psi = 0 \\
(i \slash \partial - i^2 e \slash A - m)\psi = 0 \\
(i \slash \partial - m)\psi = - e \slash A \psi \\
\end{gather}
$$
- p. 278 : "e.q." below (7.8) is probably supposed to be "e.g." ("equestrian" vs "exempli gratia"!)
- p. 301 : There's one other macroscopic classical field besides the electromagnetic... the very end of the footnote might need modification. The footnote is otherwise about the _quantum_ fields present in the discussion. Of _those_, only one has a macroscopic manifestation?
- **p. 310 : Especially between equations (8.10-8.37), but all throughout the back half of the book, all $dk^3$ factors should be $d^3k$.**
- p. 322 : (8.42) was already introduced in (8.29). A reference back might be nice.
- p. 329 : Footnote 19 should read "...well-defined _wave_ lengths..."
- p. 331 : The first line of (8.71) should end with $\exp({-i\vec{0}\cdot\vec{x}})$ (the delta function is $\delta(\vec{k})$).
- p. 347 : Before footnote 36, "Green function" should be "Green's function"
- p. 362 : parenthesis around 

$$
\bigg(\frac{\partial \mathcal{L}}{\partial(\partial_0\psi)}\partial_0\psi - \mathcal{L}\bigg)
$$

- **Instanton chapter** : The coupling constant here isn't $g$, it's $\lambda$.
- p. 467 : Notation for the three fields is not consistent. Copypasted from different sections.
- p. 600 : It might be nice to remind silly readers here (like the one writing this) that $\partial_\mu = (\partial_t, \partial_i)$!

```{code-cell} ipython3
from scipy.linalg import svd

svd(gJ.x0)
```

```{code-cell} ipython3
svd?
```

```{code-cell} ipython3

```
