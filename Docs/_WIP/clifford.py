import numpy as np


class PauliMatrices:
    x = np.array([[0, 1], [1, 0]])

    z = np.array([[1, 0], [0, -1]])

    y = -1j * z @ x

    I = np.diag(np.ones(2))

    # define aliases for these to use consistently with gamma matrices.
    x1, x2, x3 = x, y, z

    def __call__(self):
        return np.array([self.x, self.y, self.z])


s = PauliMatrices()


def _commutator_core(a, b, s=-1):
    """
    parameters
    ----------
    a, b : dtype
        Values to take the commutator of.
    s : float
        Sign of the operation. -1 for commutator, +1 for anticommutator.
    """
    assert np.logical_or(s == 1, s == -1)

    def params_are_type(dtype):
        res = np.all([type(_i) == dtype for _i in [a, b]])
        return res

    if params_are_type(int):
        if s == 1:
            comm = 0  # = a * b - b * a
        elif s == -1:
            comm = 2 * a * b

    elif params_are_type(np.ndarray):
        comm = a @ b - s * b @ a

    else:
        raise TypeError

    return comm


def commutator(a, b):
    return _commutator_core(a=a, b=b, s=1)


def anticommutator(a, b):
    return _commutator_core(a=a, b=b, s=-1)


class GammaMatrices:
    """
    Representation of the gamma matrices in the selected basis.

    parameters:
    basis : str
        Creates the gamma_0 and gamma_5 matrices in the selected basis.
        - Options are "Dirac" and "Weyl"
    """

    gamma = (1j * s.y).real

    I = np.diag(np.ones(4))

    def __init__(self, basis="Dirac"):
        """
        Settting of xj must be done here, due to the designed scope
        of list comprehensions in Python3:
                             https://stackoverflow.com/questions/13905741/accessing-class-variables-from-a-list-comprehension-in-the-class-definition
        """
        self.xj = np.array([np.kron(self.gamma, _s) for _s in s()])
        self.x1, self.x2, self.x3 = self.xj

        self.basis = basis

        if basis == "Dirac":
            self._set_dirac_basis()
        elif basis == "Weyl":
            self._set_weyl_basis()

    @property
    def mu(self):
        return np.concatenate(([self.x0], self.xj))

    def _set_dirac_basis(self):
        """Change the basis to the Dirac basis."""
        self.x0 = np.kron(s.z, s.I)
        self.x5 = self._set_gamma_5()

    def _set_weyl_basis(self):
        """Change the basis to the Weyl basis."""
        self.x0 = np.kron(s.x, s.I)
        self._set_gamma_5()

    def _set_gamma_5(self):
        res = 1j * self.I
        for g_mu in self.mu:
            res = res @ g_mu
        if self.basis == "Dirac":
            assert np.allclose(res, np.kron(s.x1, s.I))

        self.x5 = res


def N():
    """Return the matrix transforming the Weyl and Dirac
    representations into each other."""

    _N = 0.5 * (np.kron((s.x + s.z), s.I))

    return _N
