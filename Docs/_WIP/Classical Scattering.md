---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

In this notebook I'll go through some of the basic results of classical scattering. Confusing topics will be defined, and some ways to consider visualizing the results of problems will be explored.

```{code-cell} ipython3
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
```

# Lagrangian equations of motion

For a central force problem. More complicated potentials will require more complicated treatment.

The Lagrangian can be written in the standard non-relativistic form 

$$
L(q, \dot q, t) = T(\dot q) - V(q)
$$

where in principle each $q_i$ and $\dot q_i$ should be understood to have an index representing the ith particle. 

For a central force, the angular momentum will be conserved, and the equations of motion can be restricted to a plane.

## convert to radial basis

+++

## Some brief trig
I don't look at arcfuncs very often, so I have less of an intuitive idea what they look like.

```{code-cell} ipython3
xs = np.linspace(-1.0, 1.0, 200)
arcsin = np.arcsin(xs)
arccos = np.arccos(xs)
xs2 = np.linspace(-np.pi, np.pi, 1000)
arctan = np.arctan(xs2)

fig, ax = plt.subplots(figsize=(8, 4))
ax.plot(xs, arcsin, label="arcsin")
ax.plot(xs, arccos, label="arccos")
# ax.hlines(arccos+arcsin, -np.pi, np.pi, alpha=0.1)
ax.legend(loc=2);
ax2 = plt.twiny()
ax2.plot(xs2, arctan, label="arctan", color="k")
ax2.plot(xs2, np.cos(xs2), "--", alpha=0.4, label="cos");
ax2.plot(xs2, np.sin(xs2), "--", alpha=0.4, label="sin");
ax2.legend();
ax.set(xlim=(-1.5, 1.5));
plt.tight_layout();
```

# Scattering

Integrals:
All of the textbooks say **"BY A STANDARD INTEGRATION, WE GET..."** but no one explains! The integral is indeed not too bad but it's probably one of the harder ones in the undergraduate curriculum, and there's a tricky question of which trig function you should use in the substitution.

For all of these types of integrals:
1) complete the square
2) Change variables so that $A, B, C$ are outside the integral
3) Sub for sine or cosine as the integration variable.

$$
\begin{align}
\int \frac{dx}{\sqrt{A - Bx - Cx^2}} = \frac{1}{\sqrt{C}} \cos^{-1}\bigg( \frac{2Cx + B}{\sqrt{4AC + B^2}} \bigg) \\
\int \frac{dx}{\sqrt{A + Bx - Cx^2}} = \frac{1}{\sqrt{C}} \cos^{-1}\bigg( \frac{2Cx - B}{\sqrt{4AC + B^2}} \bigg) \\
\int \frac{dx}{\sqrt{A + Bx + Cx^2}} = \frac{1}{\sqrt{C}} \sinh^{-1}\bigg( \frac{2Cx + B}{\sqrt{4AC - B^2}} \bigg)
\end{align}
$$

- It's fun to think about what the last term means. A repulsive potential for which the angular momentum appears to pull a mass *inwards*. Sinh and Cosh functions appear in relativistic boosts. Is there a silly analogy to be found among the hyperbolic formalism?
- Less speculative, the bounds on x for the arguments of the first two inverse trig functions are suspicious. If $A=B=C=1$, the requirement that the argument vary between $[-1. 1]$ means that $x$ is bound by (for all four combinations of $\pm$):

$$
y = \frac{\pm 2x \pm 1}{\sqrt{5}}, y \in [-1, 1] \\
\rightarrow x = \frac{\pm \sqrt{5} \pm 1}{2}
$$

The four bounds are $\pm \phi_i$, two sets of the roots of $\phi^2 = \phi + 1$, the Golden Ratio! This might be obvious to a mathematician, but it caught me off guard. We can set up the orbits problem to generate the Fibonacci sequence for us.

```{code-cell} ipython3
from typing import NamedTuple
from numpy.polynomial import Polynomial
import scipy.constants as sc

def _int_result_(x, func=np.arccos, cs=[1, 1, 1], signs=[1, 1]):
    A, B, C = cs
    
    _num = 2 * C * x + signs[0] * B
    delta = 4 * A * C + signs[1] * B**2
    _denom = np.sqrt(delta)
    res = (1 / np.sqrt(C)) * func(_num / _denom)
    return res

def rhs1(x, cs=[1, 1, 1]):
    _func = lambda x : -np.arccos(x)
    return _int_result_(x=x, func=_func, cs=cs, signs=[1, 1])

def rhs2(x, cs=[1, 1, 1]):
    _func = lambda x : -np.arccos(x)
    return _int_result_(x=x, func=_func, cs=cs, signs=[-1, 1])

def rhs3(x, cs=[1, 1, 1]):
    _func = lambda x : np.arcsinh(x)
    return _int_result_(x=x, func=_func, cs=cs, signs=[1, -1])

def _integral_(x, cs=[1, 1, 1], signs=[1, -1, -1]):
    dx = np.diff(x)[-1]
    ABC = np.prod(np.array([cs, signs]), axis=0)
    p = Polynomial(list(ABC))
    integrand =  1 / np.sqrt(p(x))
    return np.cumsum(integrand) * dx
    
def integral_1(x, cs=[1, 1, 1]):
    return _integral_(x=x, cs=cs, signs=[1, -1, -1])

def integral_2(x, cs=[1, 1, 1]):
    return _integral_(x=x, cs=cs, signs=[1, 1, -1])

def integral_3(x, cs=[1, 1, 1]):
    return _integral_(x=x, cs=cs, signs=[1, 1, 1])

class Integrals(NamedTuple):
    func : "function"
    rhs  : "function"
    cs : list
    bounds : list
    N : int
    
    #@property
    #def bounds(self):
    #    A, B, C = self.cs
    #    delta = np.sqrt(4*A*C + )
    
    @property
    def integrate(self):
        return self.func(cs=self.cs, x=self.xs)
    
    @property
    def analytic(self):
        res = self.rhs(cs=self.cs, x=self.xs)
        return  res + abs(self.integrate[0] - res[0])
    
    @property
    def xs(self):
        return np.linspace(self.bounds[0], self.bounds[1], self.N)

phi1 = round((1 + np.sqrt(5)) / 2, 4)
phi2 = round((1 - np.sqrt(5)) / 2, 4)
```

```{code-cell} ipython3
import ipywidgets as widgets


@widgets.interact(
    integral=["+--", "++-", "+++"])
def plot(A=(0, 10, 0.1), B=(0, 10, 0.1), C=(0, 10, 0.1),
        integral="+++"):
    
    if integral == "+--":
        func_dict = dict(func=integral_1, rhs=rhs1, bounds=[-phi1, -phi2])
    elif integral == "++-":
        func_dict = dict(func=integral_2, rhs=rhs2, bounds=[phi2, phi1])
    elif integral == "+++":
        func_dict = dict(func=integral_3, rhs=rhs3, bounds=[-10, 10])
    
    cs = [A, B, C]

    I3 = Integrals(cs=cs, N=500*10, **func_dict)
    
    fig, axs = plt.subplots()
    f = I3
    int_vals = f.integrate
    rhs_vals = f.analytic
    xs = f.xs
    
    axs.plot(xs*np.cos(int_vals), xs*np.sin(int_vals), label="integral");
    axs.plot(xs*np.cos(rhs_vals), xs*np.sin(rhs_vals), "--", color="k", label="analytic", alpha=0.8);
    axs.set(xlabel="xs", ylabel="ys")
    axs.legend();
```

Here A is like the energy, B is the potential energy strength, and C is the angular momentum effective potential. Check out the "+--" integral for the values A=B=C=1!

```{code-cell} ipython3
eps = np.finfo(float).eps

intargs = dict(cs=[3, 3, 3], N=10000)

I1 = Integrals(func=integral_1, rhs=rhs1, bounds=[-phi1, -phi2], **intargs)
I2 = Integrals(func=integral_2, rhs=rhs2, bounds=[phi2, phi1], **intargs)
I3 = Integrals(func=integral_3, rhs=rhs3, bounds=[-100, 100], **intargs)

titles = ["+--", "++-", "+++"]
fs = [I1, I2, I3]

# plot below
rows = 1
cols = len(titles)
aspect = 1
ax_width = 4
width = cols * ax_width
height = rows * ax_width / aspect 
fig, axs = plt.subplots(rows, cols, figsize=(width, height))

for i, (ax, f, _title) in enumerate(zip(axs, fs, titles)):
    int_vals = f.integrate
    rhs_vals = f.analytic
    xs = f.xs
    
    ax.plot(xs, int_vals, label="integral");
    ax.plot(xs, rhs_vals, "--", color="k", label="analytic", alpha=0.8);
    ax.set(title=_title, xlabel="rs", ylabel=r"$\theta$")
    ax.legend();
plt.tight_layout();
```

```{code-cell} ipython3
eps = np.finfo(float).eps

scale = 2
xs = np.linspace(-phi2, phi2, 100)
rs = 1 / xs[::-1]
cs = [1, -1, 1]
pltargs = dict(cs=cs, x=xs)

funcs = [integral_1, integral_2, integral_3]
rhss  = [rhs1, rhs2, rhs3]
titles = ["+--", "++-", "+++"]

rows = 1
cols = len(funcs)
aspect = 1
ax_width = 4
width = cols * ax_width
height = rows * ax_width / aspect 
fig, axs = plt.subplots(rows, cols, figsize=(width, height))

for i, (ax, _func, _rhs, _title) in enumerate(zip(axs, funcs, rhss, titles)):
    int_vals = _func(**pltargs)
    rhs_vals = _rhs(**pltargs)
    shift = rhs_vals[0]
    rhs_vals -= shift
    xs = pltargs["x"]
    
    ax.plot(xs, int_vals, label="integral");
    ax.plot(xs, rhs_vals, "--", color="k", label="analytic", alpha=0.8);
    ax.set(title=_title + f", shift={shift:1.4f}", xlabel="rs", ylabel=r"$\theta$")
    ax.legend();
plt.tight_layout();
```

## Hard sphere scattering

## Rutherford Scattering

+++

$$
\theta = \int_{\text{r_min}}^{\infty} \frac{dr}{r^2 \sqrt{2m(E - V_{\text{eff}}(r))}}
$$

```{code-cell} ipython3
from scipy.integrate import solve_ivp

E = 0.1
k = 1
m = 1

def dr_dtheta(t, r):
    res = r**2 * np.sqrt(2 * m * (E - k / r - 1/r**2))
    return res

y0 = np.array([12])

res = solve_ivp(dr_dtheta, t_span=[0, 1], y0=y0)
```

```{code-cell} ipython3
def Rutherford(z, Z, e2, E, Theta):
    k_2E = e2 * z * Z / 2 / E
    sigma = (np.sin(Theta / 2))**(-1/4) * k_2E / 4
    return sigma
             
Thetas = np.linspace(0.1, np.pi, 100)
plt.plot(Thetas, Rutherford(z=1, Z=2, e2=1, E=1, Theta=Thetas));
```

# Gaussian Scattering
This is informed from MMF's derivations of the properties of the BEC orbit simulations.

```{code-cell} ipython3

```

```{code-cell} ipython3

```
