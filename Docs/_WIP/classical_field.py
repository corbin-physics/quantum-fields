import numpy as np
from scipy.integrate import odeint
from scipy.special import jv

try:
    from mmfutils.contexts import NoInterrupt
except ImportError:
    from contexts import NoInterrupt

__all__ = ["ClassicalField", "animate_fields"]


class ClassicalField(object):
    """Represent and simulate a solution of a classical field in 1D.

    Paremeters
    ----------
    N : int
       Number of lattice points in grid (should factor into powers of 2, 3, and
       5 for performance reasons.)
    L : float
       Length of periodic box at time t=0.
    a : float
       Quadratic term in potential: `a = m**2` for positive `a`.
    lam : float
       Quartic term in potential.
    H : float
       Hubble constant.  Scale factor satisfies `s(t) = exp(H*t)`.
    friction : float
       Friction term.  Useful for "cooling" solutions to reduce the energy.
    J0 : float
       Strength of the delta-function source term.
    sigma_dx : float
       Width of the gaussian simulating the delta-function source.
    """

    def __init__(
        self,
        N=2 ** 10,
        L=100.0,
        a=1.0,
        lam=1.0,
        H=0.0,
        friction=0.0,
        t=0.0,
        J0=0.0,
        sigma_dx=2.0,
        phi=None,
        dphi=None,
    ):
        self.N = N
        self._L = L
        self.a = a
        self.lam = lam

        self.t = t

        # Friction and Hubble constant
        self._friction = friction
        self.H = H

        # Source term parameters
        self.J0 = J0
        self.sigma_dx = sigma_dx

        # Initial state
        if phi is None:
            phi = np.zeros(self.N, dtype=float)
        if dphi is None:
            dphi = np.zeros(self.N, dtype=float)

        self.phi = phi
        self.dphi = dphi

    # The @property decorator allows you to use this as if it were simply
    # a member of the class `field.scale_factor` instead of calling it
    # as a function `field.scale_factor()`, but still calls the function.
    # For example, right now the Hubble constant is a fixed value, but
    # you could define it as a property and make it depend on time without
    # changing any other code.
    @property
    def scale_factor(self):
        """Return the spatial scaling factor for an expanding universe."""
        return np.exp(self.H * self.t)

    @property
    def L(self):
        """Return the lattice spacing."""
        return self._L * self.scale_factor

    @property
    def dx(self):
        """Return the lattice spacing."""
        return self.L / self.N

    @property
    def xs(self):
        """Return the set of abscissa (lattice points)."""
        return np.arange(self.N) * self.dx - self.L / 2.0

    @property
    def ks(self):
        """Return the set of wavevectors corresponding to the FFT."""
        return 2 * np.pi * np.fft.fftfreq(self.N, self.dx)

    @property
    def friction(self):
        """Return the friction coefficient, including the Hubble constant."""
        return self._friction + self.H

    @property
    def J(self):
        """Source term.  This is a gaussian approximating a delta-function."""
        if not self.J0:
            return 0.0

        sigma_x = self.sigma_dx * self.dx
        sigma_t = sigma_x / self.scale_factor
        return self.J0 * (
            np.exp(-((self.xs / sigma_x) ** 2 + (self.t / sigma_t) ** 2) / 2.0)
            / (2 * np.pi * sigma_x * sigma_t)
        )

    def laplacian(self, f):
        """Return the laplacian of f computed using the FFT."""
        res = np.fft.ifft(-self.ks ** 2 * np.fft.fft(f))
        assert np.allclose(res.imag, 0)
        return res.real

    def V(self, phi, d=0):
        """Return the d'th derivative of the potential."""
        if d == 0:
            return self.a / 2 * phi ** 2 + self.lam / 4 * phi ** 4
        elif d == 1:
            return self.a * phi + self.lam * phi ** 3
        else:
            raise NotImplementedError("d={} not supported.".format(d))

    def _rhs(self, q, t):
        """RHS for odeint.  Computes the time derivatives of 'phi, dphi'.

        This is intended for internal use, so we name it with an underscore
        which is the pythonic convention for "private" variables/methods.  (If
        you want more security you cn start it with two underscores, which will
        also cause the name to be mangled so accidental use is more difficult.)
        """
        phi, dphi = q.reshape((2, len(q) // 2))
        self.t = t
        ddphi = self.laplacian(phi) - self.V(phi, d=1) - self.friction * dphi - self.J
        return np.hstack([dphi, ddphi])

    def evolve(self, dt):
        """Evolve the state from time `self.t` to `self.t+dt`.

        Updates `self.t`, `self.phi`, and `self.dphi`.
        """
        q0 = np.ravel([self.phi, self.dphi])
        t0 = self.t
        t1 = self.t + dt
        q1 = odeint(self._rhs, q0, [t0, t1])[-1]
        self.phi, self.dphi = q1.reshape((2, self.N))
        self.t = t1

    def energy(self):
        """Return the total energy of the current configuration."""
        phi, dphi = self.phi, self.dphi
        return (
            np.sum((dphi ** 2 - phi * self.laplacian(phi)) / 2.0 + self.V(phi))
            * self.dx
        )

    ######################################################################
    # Some tools for setting states
    def set_packet(self, width=1.0, k=0.0):
        """Set the state to a gaussian wave packet containing a plane wave with
        wavevector k.  This should propagate perfectly if there is no mass term
        or non-linear interaction, but is only approximate otherwise.
        """
        x = self.xs
        m2 = self.a
        w = E = np.sqrt(k ** 2 + m2)
        v_group = k / E
        # v_phase = E/k
        t = self.t
        envelope = np.exp(-(((x - v_group * t) / width) ** 2) / 2.0)
        denvelope = v_group * ((x - v_group * t) / width ** 2) * envelope
        theta = k * x - w * t
        phase = np.cos(theta)
        dphase = w * np.sin(theta)
        self.phi = envelope * phase
        self.dphi = denvelope * phase + envelope * dphase
        return self

    def get_G_ret(self):
        """Return the retarded Green's function.

        This is the analytic solution to evolution of the vacuum
        from a delta-function source term at time t=0.
        """
        x = self.xs
        t = self.t
        m = np.sqrt(self.a)
        u = np.sqrt(np.maximum(0, t ** 2 - x ** 2))
        return np.where(u > 0, -m * jv(0, m * u) / 2, 0)

    ######################################################################
    # Optional plotting (used by animate() below)
    def plot(self, plot_data=None):
        """Plot the current field configuration.

        Returns `plot_data` which contains the plotting element.
        If passed back to subsequent calls of plot(), these will
        be reused to update the plot more quickly for faster animation
        performance.
        """
        # Import this here so we don't have a hard dependence on matplotlib
        from matplotlib import pyplot as plt

        phi = self.phi
        E = self.energy()
        title = "t={}, E={:.4f}".format(self.t, E)
        if plot_data is None:
            (line1,) = plt.plot(self.xs, phi)
            text = plt.title(title)
        else:
            # If we have data
            line1, text = plot_data
            line1.set_data(self.xs, phi)
            text.set_text(title)
            if self.H:
                ax = plt.gca()
                ax.relim()
                ax.autoscale_view(False, True, False)

        plot_data = line1, text
        return plot_data


def animate_fields(fields, dt=0.1, t=0.0, T=10.0, ylims=(-1.5, 1.5)):
    """Animate the states.

    Arguments
    ---------
    fields : ClassicalField instance or list of ClassicalField instances

    To view this, use a notebook with one of the following
    executed in a previous cell::

       %matplotlib inline
       %pylab inline --no-import-all

    This will update the plot every `dt` steps from `t` to `T`.

    You may interrupt the animation at any time by pressing Ctrl-C or sending a
    KeyboardInterrupt exception via the `Kernel/Interrupt` menu item (stop
    button) in the notebook interface.

    (This code uses a special context to ensure that interrupts only apply once
    the evolution for `dt` is complete keeping the field configuration valid.
    If for some reason the evolution for `dt` is taking too long, you might
    have to force a stop by sending three interrupts rapidly.  This might kill
    the kernel though, or invalidate the fields.)
    """
    # Import these here so that IPython is only required for plotting
    from IPython.display import display, clear_output
    from matplotlib import pyplot as plt

    if not isinstance(fields, list):
        # Allows code to accept a single field instance.
        fields = [fields]
    plot_data = [None] * len(fields)
    for field in fields:
        field.t = t

    with NoInterrupt(ignore=True) as interrupted:
        # Special context to defer interruption until evolution
        # for dt is complete.
        plt.clf()
        while not interrupted and fields[0].t < T:
            for _n, field in enumerate(fields):
                if interrupted:
                    break
                field.evolve(dt)
                plot_data[_n] = field.plot(plot_data[_n])
                if ylims is not None:
                    plt.ylim(*ylims)
            display(plt.gcf())
            clear_output(wait=True)
