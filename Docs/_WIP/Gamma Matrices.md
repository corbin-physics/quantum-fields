---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

```{code-cell} ipython3
%matplotlib inline
from matplotlib import pyplot as plt
import numpy as np

from importlib import reload
import logging
logger = logging.getLogger()

import clifford
from clifford import commutator, anticommutator
```

```{code-cell} ipython3
def test_pauli_matrix_properties(s):
    """
    Test the Pauli matrix relations for an object s with
    attributes named x, y, z, corresponding to the three
    Pauli matrices, and I, the 2x2 identity.
    """

    cyclic_sets = [("x", "y", "z"),
               ("y", "z", "x"),
               ("z", "x", "y")]

    for (_i, _j, _k) in cyclic_sets:
        s_i, s_j, s_k = [getattr(s, _ind) for _ind in (_i, _j, _k)]

        # test cyclic condition : s_i @ s_j = 1j * eps_ijk s_k
        assert np.allclose(s_i @ s_j, 1j * s_k)
        print(f"\"s_{_i} s_{_j} = i s_{_k}\" cyclic product condition holds.")
        
        # square to identity : s_i @ s_i = I
        assert np.allclose(s_i @ s_i, s.I)
        print(f"s_{_i} squares to the identity.")
        
        # anticommutation rules
        assert np.allclose(anticommutator(s_i, s_j), 0)
        assert np.allclose(anticommutator(s_i, s_i), 2 * s.I)
        print(f"s_{_i} anticommutes to 2 * I(2x2).")
        
        # commutation rules
        assert np.allclose(commutator(s_i, s_j), 2j * s_k)
        assert np.allclose(commutator(s_i, s_i), 0)
        print(f"s_{_i} commutes with itself.")
        
# test scalar commutators
assert np.allclose(commutator(12, 5), 0)
assert np.allclose(anticommutator(12, 5), 2 * 12 * 5)
```

---

# Pauli Matrices

$$
\sigma^x = \begin{pmatrix}
0 & 1 \\
1 & 0
\end{pmatrix} \qquad \
\sigma^y = \begin{pmatrix}
0 & 1 \\
1 & 0 \end{pmatrix} \qquad \
\sigma^z = \begin{pmatrix}
1 & 0 \\
0 & -1
\end{pmatrix}
$$

These matrices with the 2x2 identity matrix form a complete basis, and in combination with the factors $\pm 1, \pm i$, form the 16 element Pauli group.

$$
G_p = \{\pm \sigma^x, \pm i \sigma^x, \pm \sigma^y, \pm i \sigma^y, \pm \sigma^z, \pm i \sigma^z, \pm I \pm i I\}
$$

```{code-cell} ipython3
s = clifford.PauliMatrices()

commutator = clifford.commutator
anticommutator = clifford.anticommutator
```

$$
[\sigma^i, \sigma^j] = 2 i \epsilon_{ijk}\sigma^k \\
\{\sigma^i, \sigma^j\} = 2 \delta_{ij} \mathbf{I} \\
\begin{gather}
[\sigma^i, \sigma^j] + \{\sigma^i, \sigma^j\} = (\sigma^i \sigma^j - \sigma^j \sigma^i) + (\sigma^i \sigma^j + \sigma^j \sigma^i) \\
= 2 \sigma^i \sigma^j \\ 
\boxed{\sigma^i \sigma^j = i \epsilon_{ijk}\sigma^k + \delta_{ij} \mathbf{I}}
\end{gather}
$$

```{code-cell} ipython3
test_pauli_matrix_properties(s=s)
```

# Gamma Matrices

+++

## Dirac Basis

The **Dirac basis**, also known as the **mass basis**.

The point of this basis is to diagonalize the mass term of the Lagrangian,
$$
\mathcal L = \bar \Psi \big( i \gamma^{\mu} \partial_\mu - m\big) \Psi \\
\rightarrow - \bar \Psi m \Psi = - \underbrace{(\Psi)^\dagger \gamma^0}_{\bar{\Psi}} m \Psi 
$$

$$
\gamma^0 = \begin{pmatrix}
\mathbf{I} & 0 \\
0 & -\mathbf{I}
\end{pmatrix} \qquad
\gamma^k = \begin{pmatrix}
0 & \sigma^k \\
- \sigma^k & 0
\end{pmatrix}\qquad (2.164)
$$

```{code-cell} ipython3
g_dirac = clifford.GammaMatrices(basis="Dirac")

print(g_dirac.mu, "\n")
```

```{code-cell} ipython3
N = 0.5 * np.kron((s.z + s.x), s.I)
assert np.alltrue([np.allclose(N.T, N), np.allclose(np.linalg.inv(N), 2 * N)])

def transform(g):
    return 2 * (N @ g @ N)

# check lines of algebra for this junk
# 2 N \gamma^0 (Dirac basis) N
res =  0.5 * np.kron((s.z + s.x), s.I) @ np.kron(s.z, s.I) @ np.kron((s.z + s.x), s.I)
res2 = 0.5 * np.kron((s.z + s.x) @ s.z, s.I) @ np.kron((s.z + s.x), s.I)
res3 = 0.5 * np.kron((s.I - 1j * s.y), s.I) @ np.kron((s.z + s.x), s.I)
res4 = np.kron(s.x, s.I)

assert np.allclose(res, np.kron(s.x, s.I))
assert np.allclose(res, res2)
assert np.allclose(res2, res3)
assert np.allclose(res3, res4)

print(f"Dirac :\n{g_dirac.x0}")
print(f"Weyl  :\n{transform(g_dirac.x0)}")
```

```{code-cell} ipython3
# Weyl Basis
# print(transform(g_dirac.x0), "\n")
[print(transform(_gmu), "\n") for _gmu in g_dirac.mu]

assert np.allclose(g_dirac.mu, transform(transform(g_dirac.mu)))
```

## Clifford Algebra

For the [West Coast metric](https://www.math.columbia.edu/~woit/wordpress/?p=7773), with minuses on the spatial terms, the gamma matrices obey the below anticommutation relation, known as the Clifford Algebra.

$$
\{\gamma^\mu, \gamma^\nu \} = \gamma^\mu \gamma^\nu + \gamma^\nu \gamma^\mu = 2 \eta^{\mu \nu}\mathbf{I}
$$

```{code-cell} ipython3
def get_metric(conv="particle"):
    res = np.array([1, -1, -1, -1])
    
    if conv == "particle":
        pass
    elif conv == "gravity":
        res *= -1
    return np.diag(res)

metric = get_metric()
identity = g_dirac.I

assert np.all([np.allclose(anticommutator(g_mu, g_mu), 2 * metric[i, i] * identity) 
               for i, g_mu in enumerate(g_dirac.mu)])
```

$$
\frac{i}{2}\big[\gamma^{i} \gamma^{j}\big] =  \epsilon^{ijk} \mathbf{I}\otimes \sigma^k \equiv \sigma^{ij}
$$

```{code-cell} ipython3
from itertools import permutations

[_x for _x in permutations(["x1", "x2", "x3"])]
```

```{code-cell} ipython3
#for (_i, _j, _k) in permutations(["x1", "x2", "x3"]):
#    raise Exception("This will fail without Levi-Civita!")

cyclic_sets = [("x1", "x2", "x3"),
               ("x2", "x3", "x1"),
               ("x3", "x1", "x2")]

for (_i, _j, _k) in cyclic_sets:
    _gi = getattr(g_dirac, _i)
    _gj = getattr(g_dirac, _j)
    _sk = getattr(s, _k)
    assert np.allclose(1j / 2 * commutator(_gi, _gj), np.kron(s.I, _sk))
```

## Basis matrices - set of 16

```{code-cell} ipython3
g_mus = [getattr(g_dirac, f"x{i}") for i in [0, 1, 2, 3, 5]]
g_mus[-1]
```

## Weyl (chiral) Basis
$$
\gamma^0 = \begin{pmatrix}
0 & \mathbf{I} \\
\mathbf{I} & 0
\end{pmatrix} \qquad
\gamma^k = \begin{pmatrix}
0 & \sigma^k \\
- \sigma^k & 0
\end{pmatrix}\qquad (2.165)
$$

+++

### Tests : commutation, anticommutation, etc.

Here some tests will be performed on the gamma matrix object in multiple bases to ensure that their basis independent, fundamental properties are satisfied.

```{code-cell} ipython3
logger.setLevel(logging.DEBUG)

def test_gamma_matrix_properties(g):
    inds = ["x0", "x1", "x2", "x3"]
    I4 = np.kron(s.I, s.I)
    
    fails = 0
    
    for _mu, _ind1 in enumerate(inds):
        g_mu = getattr(g, _ind1)
        if not np.allclose(np.trace(g_mu), 0):
            logging.debug(f"tr(g_{_ind1}) != 0")
        
        for _nu, _ind2 in enumerate(inds):
            
            eta = metric[_mu, _nu]
            
            g_nu = getattr(g, _ind2)
            
            if not np.allclose(np.trace(g_mu @ g_nu), 4 * eta):
                logging.debug(f"tr(g_{_ind1} g_{_ind2}) != {4 * eta}")
                fails += 1
                
            if _mu < _nu:
                if not np.allclose(anticommutator(g_mu, g_nu), 0):
                    logging.debug(f"anticommute g_{_ind1} g_{_ind2} fails.")
                    fails += 1
                    
            elif _mu == _nu:
                if not np.allclose(anticommutator(g_mu, g_nu), 2 * eta * I4):
                    logging.debug(f"[g_{_ind1}, g_{_ind2}]_A fails.")
                    fails += 1
          
    if fails > 0:
        logging.debug(f"{fails=}")
        raise AssertionError
    else:
        pass
```

```{code-cell} ipython3
g_dirac = clifford.GammaMatrices(basis="Dirac")
g_weyl  = clifford.GammaMatrices(basis="Weyl")

test_gamma_matrix_properties(g_dirac)
test_gamma_matrix_properties(g_weyl)
```

```{code-cell} ipython3
g_squared = [_g @ _g for _g in g_dirac.mu]
g_squared
```

```{code-cell} ipython3

```
