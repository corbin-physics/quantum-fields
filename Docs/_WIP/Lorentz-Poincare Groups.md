---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.15.2
kernelspec:
  display_name: Python 3 (phys--quantum-fields)
  language: python
  name: phys--quantum-fields
---

# Group Theory Primer

Quantum mechanics (and physics generally) is saturated in group theory, and yet many students never have to take a course in the topic. I was one of those. 

To start with, I'll run through the postulates of what constitutes a group, and discuss a few common cases in classical physics. I'll expand the argument to cover their quantum counterparts, before covering the main topic this page was begun for, the Lorentz and Poincare groups.


# Rotations

+++

# Lorentz/Poincare Groups

[Notes in heavy notebook editing mode]

## Relativity review

###  The metric
The metric is a bilinear machine that maps two vectors into a number. In math notation, $g \colon v \times v \rightarrow \mathbb{R}$.

There are three cases for the sign of the metric: 

$$
g(v,w)  \left\{ \begin{array}{cl}
< 0 & : \text{timelike} \\
= 0 & : \text{null/lightlike} \\
> 0 & : \text{spacelike}
\end{array} \right.
$$


\begin{equation}
g^{\mu\nu} = \
\end{equation}

SO(3,1) = SO(1,3). There's no metric built into this notation--all it says is that the group is distinct from SO(4), which would imply four dimensions transforming the same way.

## Four-vectors

```{code-cell} ipython3
%matplotlib inline 
import numpy as np, matplotlib.pyplot as plt
```

```{code-cell} ipython3
class J_rot:
    """Return the relativistic rotation matrices."""
    
    @property
    def J1(self):     
        ref = np.array([[0, 1], 
                      [1, 0]])

        BR = np.diag([-1, 1]) @ ref
        zeros = 0 * BR

        J_1 = 1j * np.block([[zeros, zeros],
                        [zeros, BR]])
        return J_1
    
    @staticmethod
    def J2(self):
        pass
    
J = J_rot()
```

```{code-cell} ipython3
def mat_power(M, p=1):
    """Return the matrix times itself p times.
    Note that this does NOT raise individual elements to a power."""
    if p == 0:
        return np.eye(len(M))
    M0 = M
    for _p in range(p - 1):
        M = M @ M0
    
    return M
```

```{code-cell} ipython3
for p in range(0, 4):
    print(f"O({p}):\n", mat_power(M=J.J1, p=p), "\n")
```

```{code-cell} ipython3
from scipy.linalg import expm
mat_power(M=J.J1, p=4) + mat_power(M=J.J1, p=8)
```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

```{code-cell} ipython3
from itertools import permutations

inds = list(permutations(np.arange(3)))
np.fft.fft(inds).sum(axis=1)
```

```{code-cell} ipython3
dim = 3
e_ijk = np.zeros((dim, dim, dim)) # Levi-Civita symbol.
# eps[0,1,2] = eps[1,2,0] = eps[2,0,1] = 1
# eps[2,1,0] = eps[1,0,2] = eps[0,2,1] = -1
e_ijk
```

```{code-cell} ipython3

```

```{code-cell} ipython3

```

> *Q: What's $2023 \times 151$?* \
> *A: One heck of a New Year's party!*

```{code-cell} ipython3
num1 = [0, 2000, 0, 20, 3]
num2 = [0, 0, 100, 50, 1]

def fft_prod(a1, a2):
    res = np.fft.ifft(
        np.fft.fft(a1) * np.fft.fft(a2))
    return res.sum().real.round()

fft_prod(num1, num2), 2023 * 151
```
