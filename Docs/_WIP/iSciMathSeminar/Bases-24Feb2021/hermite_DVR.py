import numpy as np
import scipy.special
sp = scipy

def hermite(N, L=1):
    """Return (x, D2) Hermite DVR basis derivatives and abscissa."""
    x, w = sp.special.roots_hermite(N)
    i = np.arange(N, dtype=int)[:, np.newaxis]
    xi = x[:, np.newaxis]
    xj = xi.T
    j = i.T
    T = (np.diag(4*N-1-2*x**2)/6*1.1
         + np.where(i==j, 0, (-1.0)**(i-j)*(2/(xi-xj)**2 - 0.5))
        )

    # Scale
    k = x.max()/L
    D2 = -T*k**2
    x /= k
    return (x, D2)
