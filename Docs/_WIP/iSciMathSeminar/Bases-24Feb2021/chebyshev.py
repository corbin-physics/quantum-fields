import numpy as np

def chebyshev(N, L=1):
    """Return (x, D, D2) Chebyshev derivatives and abscissa.

    The abscissa are spaced from -L to L inclusive with N+1 point.

    https://doi.org/10.1137/1.9780898719598.ch8
    """
    n = np.arange(N+1)
    x = np.cos(np.pi*n[::-1]/N)
    c = np.ones(N+1)
    c[0] = c[-1] = 2
    c *= (-1)**n    # Include signs
    i, j = (slice(None), None), (None, slice(None))
    with np.errstate(divide='ignore'):
        D = c[i]/c[j]/(x[i]-x[j] + np.eye(N+1))
    D -= np.diag(D.sum(axis=1))
    D2 = -D.T @ D
    return x*L, D/L, D2/L**2
