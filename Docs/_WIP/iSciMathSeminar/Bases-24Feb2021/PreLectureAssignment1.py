# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light,md:myst
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.15.2
#   kernelspec:
#     display_name: Python 3 (phys--quantum-fields)
#     language: python
#     name: phys--quantum-fields
# ---

# fmt: off
# %matplotlib inline
try: # Atempt to install my mmf_setup package.  May need kernel restart.
    import mmf_setup
except:
    import sys
    # !{sys.executable} -m pip install mmf_setup
import mmf_setup;mmf_setup.nbinit()
import numpy as np, matplotlib.pyplot as plt
# fmt: on

# # Numerical Demos

# The objective for these lectures is to be able to comfortably solve partial differential equations such as the Schrödinger equation:
#
# $$
#   \I \hbar \pdiff{}{t} \psi(\vect{r}, t) = -\frac{\hbar^2}{2m}\nabla^2\psi(\vect{r}, t)
#   + V(\vect{r})\psi(\vect{r}, t).
# $$
#
# The approach will be to recognize that operators such as the Laplacian $\nabla^2$ are linear, hence equations of this form can be recast into a "matrix" equation:
#
# $$
#   \I \hbar \pdiff{}{t} \ket{\psi(t)} = \op{H}\ket{\psi(t)}, \qquad
#   \op{H} = \frac{\op{p}^2}{2m} + V(\op{x}), \qquad
#   \op{p} = - \I \hbar \vect{\nabla},
# $$
#
# and approached both generally and efficiently by identifying appropriate bases.  *Note: we are using physics bra-ket notation for vectors $\ket{f} \equiv \vec{f}$.  Think of these "ket"s as column vectors.*
#
# In the example of the time-independent Schrödinger equation, for example, the solution can be fully specified in terms of a matrix exponential *(called a **propagator**)*:
#
# $$
#   \ket{\psi(t)} = \overbrace{e^{\op{H}t/\I\hbar}}^{\mat{U}(t)}\ket{\psi(0)}
# $$
#
# where the vector $\ket{\psi(0)}$ specifies the initial conditions at time $t=0$.

# ## Test Problems
#
# We consider test-problems here with the goal of finding the ground state of the time-independent Schrödinger equation in 1D:
#
# $$
#   \overbrace{\left(\frac{-\hbar^2}{2m}\nabla^2 + V(x)\right)}^{\mat{H}}\psi_0 = E_0\psi_0
# $$
#
# with different potentials $V(x)$.  Our strategy will be to use different discretizations to turn the Hamiltonian $\mat{H}$ into a matrix, then to diagonalize this.  Excited states can also be found this way, but we will focus on the ground state here.
#
# The tests cases are:
#
# 1. Harmonic Oscillator:
#
#    $$
#      V(x) = \frac{m\omega^2}{2}x^2 = \frac{\hbar\omega}{2}\left(\frac{x}{a}\right)^2,
#      \qquad a = \sqrt{\frac{\hbar}{m\omega}},
#      \qquad\hbar = m = \omega = a = 1,\\
#      \psi_0(x) \propto e^{-x^2/2a^2},
#      \qquad E_{0} = \tfrac{1}{2}\hbar \omega.
#    $$
#
#    This is an analytic potential and has an analytic solution. (Analytic here means that the Taylor series converges with infinite radius of convergence.)
# 2. General oscillator (`V_osc(x)` and various `V1(x) = V_osc(x, p=1)`, `V3(x) = V_osc(x, p=3)` etc. in the code):
#
#    $$
#      V(x) = \frac{\hbar\omega}{2}\left(\frac{\abs{x}}{a}\right)^p, 
#      \qquad a = \sqrt{\frac{\hbar}{m\omega}},\qquad
#      \hbar = m = \omega = a = 1.
#    $$
#
#    This is analytic and has analytic solutions for even $p$, but is not analytic for odd $p$.  $p=1$ has a kink, while $p=3$ is smooth.  I am not aware of any exact solutions to this problem except for $p=1$ (in terms of Airy functions) and $p=2$ above.
# 3. $C^{\infty}$ but not analytic (`VC(x)` in the code):
#
#    $$
#      V(x) = \frac{\hbar\omega}{2}\frac{2a^4(6x^2 - 4a^2)}{x^6(e^{a^2/x^2}-1)}, 
#      \qquad a = \sqrt{\frac{\hbar}{m\omega}},
#      \qquad \hbar=m=\omega=a=1,\\
#      \psi_0(x) \propto 1-e^{-a^2/x^2},
#      E_0 = 0.
#    $$
#
#    Here the potential and wavefunctions are $C^{\infty}$ but not analytic.  (I constructed this potential explicitly from the wavefunction using the Schrödinger equation:
#
#    $$
#      V(x) - E_0 = \frac{\hbar^2}{2m}\frac{\nabla^2\psi_0(x)}{\psi_0(x)}.
#    $$
#
#    This approach works for any wavefunction $\psi_0(x)$ that has no nodes.

import sympy
x = sympy.var('x')
psi = sympy.S('(1-exp(-a**2/x**2))*exp(-x**2/a**2/2)')
V = (psi.diff(x,x)/psi).simplify()
V

# +
from functools import partial
hbar = m = w = 1
a = np.sqrt(hbar/m/w)  # Typical length scale = 1
E0 = hbar*w/2          # Typical energy scale

def V_osc(x, p=2):
    return E0 * (abs(x)/a)**p

# Could also do V1 = lambda x: V_osc(x, p=1) etc.
V1 = partial(V_osc, p=1)
V2 = partial(V_osc, p=2)
V3 = partial(V_osc, p=3)
V4 = partial(V_osc, p=4)

def VC(x):
    return E0 * np.where(
        x==0,
        1,
        1+2*(3*x**2 - 2*a**2)/(x/a)**6/(np.exp(a**2/x**2) - 1))

def VC1(x):
    return E0 * np.where(
        x==0,
        1,
        4*a**2*(3/2*a**2*x**2 + x**4 - a**4)/(x**6*(np.exp(a**2/x**2) - 1))
        + (x**2-a**2)/a**2)

# Exact solutions
def get_psi2_exact(x):
    psi0 = np.exp(-x**2/a**2/2)
    E0 = 0.5
    return E0, psi0

def get_psiC_exact(x):
    psi0 = np.where(x==0, 1, 1-np.exp(-a**2/x**2))
    E0 = 0
    return E0, psi0

def get_psiC1_exact(x):
    psi0 = np.where(x==0, 1, (1-np.exp(-a**2/x**2))*np.exp(-x**2/2/a**2))
    E0 = 0
    return E0, psi0

x = np.linspace(-2,2,200)
ax = plt.subplot(111)
for p in [1, 2, 3]:
    ax.plot(x/a, V_osc(x, p=p)/E0, label=f"V{p}")
ax.plot(x/a, VC(x)/E0, label="VC")
ax.plot(x/a, VC1(x)/E0, label="VC1")
ax.legend()
ax.set(xlabel='x/a', ylabel='V/E_0', ylim=(-4, 4));


# -

# ## Helpers

# Here are some helper functions to get your started.  Your task is to write a function `get_H(N, L, V)` that will return the matrix representation of the Hamiltonian.

# ***Programming Trick:** Here we use a trick to form the Hamiltonian matrix $\mat{H}$.  We instead write a function `apply_H` which applies the Hamiltonian to a wavefunction using the FFT.  We then apply this to the identify matrix to form the Hamiltonian matrix.*
#
# $$
#   [\texttt{apply_H(eye(N)}]_{ab} = \sum_{c}H_{ac}\delta_{bc} = H_{ab}.
# $$
#
# *It might make more physical sense to write `apply_H` to act on the first index (columns): $\sum_{c}H_{ac}\delta_{cb} = H_{ab}$.  This is harder to do, however, because NumPy by default applies operations like the FFT and matrix multiplication to the **last** index.  These are known as NumPy's [broadcasting rules](https://numpy.org/doc/stable/user/basics.broadcasting.html) and are implemented this way for performance since the last index corresponds to contiguous memory when arrays are stored in C-ordering (instead of Fortran ordering).  **All-ways check your code** with `assert` statements or comprehensive tests to make sure that things are working as expected.*

def get_H_FFT(N, L, V):
    """Return `(x, H)` using the FFT."""
    dx = L/N
    x = np.arange(N) * dx - L/2
    k = 2*np.pi * np.fft.fftfreq(N, dx)

    # Trick: see note above.
    def apply_H(psi):
        return (np.fft.ifft(hbar**2*k**2/2/m*np.fft.fft(psi))
                + V(x)*psi)

    H = apply_H(np.eye(N))

    # Always tests this type of approach!
    np.random.seed(3)
    psi = (np.random.random(2*N) - 0.5).view(dtype=complex)
    assert np.allclose(H @ psi, apply_H(psi))
    assert np.allclose(H, H.T.conj())
    return x, H


def get_H_FD(N, L, V):
    """Return (x, H) using centered differences."""
    dx = L/N
    x = np.arange(N) * dx - L/2
    D2 = (np.diag(np.ones(N-1), k=-1)
          + np.diag(np.ones(N-1), k=+1)
          - 2 * np.diag(np.ones(N))) / dx**2

    H = -hbar**2*D2/2/m + np.diag(V(x))
    assert np.allclose(H, H.T.conj())
    return x, H


# +
# This module is defined lower in this notebook
from chebyshev import chebyshev

def get_H_Ch(N, L, V):
    """Return (x, H) using the Chebyshev basis."""
    x, D, D2 = chebyshev(N=N-1, L=L/2)
    H = -hbar**2*D2/2/m + np.diag(V(x))
    assert np.allclose(H, H.T.conj())
    return x, H


# +
# This module is defined lower in this notebook
from hermite_DVR import hermite

def get_H_Hermite(N, L, V):
    """Return (x, H) using the Hermite DVR basis."""
    x, D2 = hermite(N=N, L=L/2)
    H = -hbar**2*D2/2/m + np.diag(V(x))
    assert np.allclose(H, H.T.conj())
    return x, H


# -

from IPython.display import display, clear_output
def analyze(get_H, V, E0_exact, Nmax=256, Ls=[10, 15, 20, 30], dxs=[0.001, 0.01, 0.1]):
    """Analyze the errors in the ground state."""
    fig, axs = plt.subplots(1, 2, figsize=(10,5))
    Ns = 2**np.arange(3, int(np.log2(Nmax))+1, dtype=int)

    # UV Errors
    ax = axs[0]
    for L in Ls:
        dxs_ = L/Ns
        errs = []
        for N in Ns:
            clear_output()
            display(f"L={L}: N={N} of {Ns[-1]}")
            x, H = get_H(N=N, L=L, V=V)
            assert np.allclose(H, H.T.conj())
            errs.append(abs(np.linalg.eigvalsh(H)[0] - E0_exact))
        ax.loglog(dxs_/a, np.abs(errs)/E0, '--', label=f"L={L}")
    ax.legend()
    ax.set(xlabel='dx/a', title='UV Errors')

    # IR Errors
    ax = axs[1]
    for dx in dxs:
        Ls_ = dx*Ns
        errs = []
        for N in Ns:
            clear_output()
            display(f"dx={dx}: N={N} of {Ns[-1]}")
            x, H = get_H(N=N, L=dx*N, V=V)
            assert np.allclose(H, H.T.conj())
            errs.append(abs(np.linalg.eigvalsh(H)[0] - E0_exact))
        ax.loglog(Ls_/a, np.abs(errs)/E0, '--', label=f"dx={dx}")
    ax.legend()
    ax.set(xlabel='L/a', title='IR Errors')
    return axs


# +
axs = analyze(get_H_FD, V=V2, E0_exact=E0, Nmax=128)
plt.suptitle("Finite Difference (HO with p=2)");

#dxs = np.linspace(0.001, 1, 1000)
#axs[0].plot(dxs, dxs**2/12)
# -

axs = analyze(get_H_FFT, V=V2, E0_exact=E0, Nmax=128)
plt.suptitle("Fourier (HO with p=2)");

axs = analyze(get_H_Ch, V=V2, E0_exact=E0, Nmax=128)
plt.suptitle("Chebyshev (HO with p=2)");

axs = analyze(get_H_Hermite, V=V2, E0_exact=E0, Nmax=128)
plt.suptitle("Hermite DVR (HO with p=2)");

# ## Exercise
#
# Try to understand what is in these plots.  What determines the rate of convergence?  You can use the returned axis object `ax_UV, ax_IR = axs` to add your own convergence estimates to these plots to check.

# ## Sample Analyses

# We start by getting a sample solution.  Our goal is first to make sure that we understand what the solution looks like.  The key to understanding the IR errors is to note that they are due to the box, thus, we must make sure our box is large enough:

from collections import OrderedDict
# Add any new discretizations here:
get_H_dict = OrderedDict([
    ('FD', get_H_FD),
    ('FFT', get_H_FFT),
    ('Chebyshev', get_H_Ch),
    ('Hermite', get_H_Hermite)])

# ### Harmonic Oscillator ($V_2(x)$)

V = V2    # Harmonic Oscillator potential
N = 256
L = 30
ax = plt.subplot(111)
for label, get_H in get_H_dict.items():
    x, H = get_H(N=N, L=L, V=V)
    Es, Psis = np.linalg.eigh(H)
    ax.semilogy(x/a, abs(Psis[:,0])/abs(Psis[:, 0]).max(), label=label)
ax.set(xlabel='x/a', ylabel=r'$|\psi|$')
ax.axhline(np.finfo(float).eps, c='y')
ax.legend()

# From this plot, we see that a box length of $L=20a$ allows the solution to fall off by a factor of $\epsilon$, so this should ensure IR convergence.  Now we can explore the UV convergence.  A bit of playing shows that we can go up to $N = 1024 = 2^{10}$ without too much effort (we can go much larger if we do not use dense eigenvalue solvers).  For now we use the energy of this state as a guess, and plot the errors:

# +
L = 20.0
Ns = 2**np.arange(2, 10, dtype=int)
dxs = L/Ns
ax = plt.subplot(111)
for label, get_H in get_H_dict.items():
    # Use N=1024 as a proxy for the exact solution since 
    # we do not know the actual answer.
    x, H = get_H(N=1024, L=L, V=V)
    E0_ = np.linalg.eigvalsh(H)[0]

    # Here we compute the energies using a list comprehension.  This is just
    # a shorthand for a simple for-loop.  At the end we apply np.asarray() to
    # make sure the result is a numpy array so we can do arithmetic with it.
    Es = np.asarray(
        [ # List comprehension
            np.linalg.eigvalsh(
                get_H(N=_N, L=L, V=V)[1] # The get_H()[1] = H
            )[0] # eigvalsh sorts the eigenvalues, so the first is the smallest
            for _N in Ns
        ]
    )
    ax.loglog(dxs/a, abs(Es - E0_)/E0, '-+', label=label)
ax.plot(dxs/a, dxs**2/12, ':', label='$dx^2/12$')
ax.set(xlabel='dx/a', ylabel='dE/E0');
ax.legend()

# Hack to get Ns on top axis... not perfect.
axtop = plt.twiny()
axtop.set(xlim=ax.get_xlim(), xscale='log', xlabel='N', xticks=dxs/a, xticklabels=Ns);
# -

# We have included the error estimate for the finite difference approach $h^2f''(x)/12$ assuming that $f''(x) \sim 1$.  This accurately characterizes the errors.  Notice that the Fourier approach converges much faster.  (Can you explain why and characterize this?)  Note that we can achieved converged results with $N=32$ or $N=64$ points using the Fourier basis.  To achieve similar convergence with finite differences would need $N\sim3\times 10^{6}$ points.

# ### Linear Oscillator $V_1(x) \propto |x|$

V = V1
N = 256
L = 35
dx = L/N
ax = plt.subplot(111)
for label, get_H in get_H_dict.items():
    x, H = get_H(N=N, L=L, V=V)
    Es, Psis = np.linalg.eigh(H)
    ax.semilogy(x/a, abs(Psis[:,0])/abs(Psis[:, 0]).max(), label=label)
ax.set(xlabel='x/a', ylabel=r'$|\psi|$')
ax.axhline(np.finfo(float).eps, c='y')
ax.legend();

# To make sure that IR errors are under control, we need $L\sim 35a$.

# +
L = 35.0
Ns = 2**np.arange(2, 10, dtype=int)
dxs = L/Ns
ax = plt.subplot(111)
for label, get_H in get_H_dict.items():
    E0_ = np.linalg.eigvalsh(get_H(N=1024, L=L, V=V)[1])[0]
    Es = np.asarray([np.linalg.eigvalsh(get_H(N=_N, L=L, V=V)[1])[0] for _N in Ns])
    ax.loglog(dxs/a, abs(Es - E0_)/E0, '-+', label=label)
ax.plot(dxs/a, dxs**2/12, ':', label='$dx^2/12$')
ax.set(xlabel='dx/a', ylabel='dE/E0');
ax.legend();

# Hack to get Ns on top axis... not perfect.
axtop = plt.twiny()
axtop.set(xlim=ax.get_xlim(), xscale='log', xlabel='N', xticks=dxs/a, xticklabels=Ns);
# -

# Now we see power-law converegnce for both finite-difference and Fourier techniques.  This plot illustrates the important point that spectral methods work best with smooth functions.

# ### Your Turn

# Use this section to complete a similar analysis of the other potentials like $V_3(x)$, $V_4(x)$ and $V_C(x)$.  Try to predict the scaling of the errors before checking numerically.

V = VC1
N = 256
L = 16.0
dx = L/N
ax = plt.subplot(111)
for label, get_H in get_H_dict.items():
    x, H = get_H(N=N, L=L, V=V)
    Es, Psis = np.linalg.eigh(H)
    ax.semilogy(x/a, abs(Psis[:,0])/abs(Psis[:, 0]).max(), label=label)
ax.set(xlabel='x/a', ylabel=r'$|\psi|$')
ax.axhline(np.finfo(float).eps, c='y')
ax.legend();

_tmp = Psis[:, 0]
for n in range(4):
    _tmp = np.gradient(_tmp, x)
plt.plot(x, abs(_tmp))
d4_psi_max = abs(_tmp).max()

x = np.linspace(-8,8,10000)
_tmp = get_psiC1_exact(x)[1]
for n in range(4):
    _tmp = np.gradient(_tmp, x)
plt.plot(x, abs(_tmp))
d4_psi_max = abs(_tmp).max()

# +
L = 16.0
L = 12.0
Ns = 2**np.arange(2, 10, dtype=int)
dxs = L/Ns
ax = plt.subplot(111)
for label, get_H in get_H_dict.items():
    E0_ = np.linalg.eigvalsh(get_H(N=1024, L=L, V=V)[1])[0]
    Es = np.asarray([np.linalg.eigvalsh(get_H(N=_N, L=L, V=V)[1])[0] for _N in Ns])
    ax.loglog(dxs/a, abs(Es - E0_)/E0, '-+', label=label)
ax.plot(dxs/a, d4_psi_max*dxs**2/12, ':', label=f"${d4_psi_max:.2f}dx^2/12$")
ax.set(xlabel='dx/a', ylabel='dE/E0');
ax.legend();

# Hack to get Ns on top axis... not perfect.
axtop = plt.twiny()
axtop.set(xlim=ax.get_xlim(), xscale='log', xlabel='N', xticks=dxs/a, xticklabels=Ns);
# -

# # Chebyshev Basis

# Here we write a file to disk which computes the derivative matrices for the Chebyshev basis.  This is used above.
#
# For accuracy here we use a Chebyshev differentiation matrix on at the points $N+1$ points $x_n$ with $n\in \{0, 1, \cdots N\}$:
#
# $$
#   x_n = \left.\cos\frac{\pi n}{N}\right|_{n=0}^{N}, \qquad
#   c_n = (2, 1, 1, \cdots, 1, 1, 2),\\
#   D_{ij} = \begin{cases}
#     \frac{c_i (-1)^{i+j}}{c_j(x_i-x_j)} & i \neq j\\
#     -\sum_{\substack{j=0\\j\neq i}}^{N} D_{ij} & i = j
#   \end{cases}.
# $$
#
# Note that the Laplacian $\mat{D}_2 = -\mat{D}^T\cdot \mat{D}$ is implemented with simple Neumann boundary conditions using this scheme.
#
# *(Formulae from [Trefethen:2000_8].)*
#
# [Trefethen:2000_8]: https://doi.org/10.1137/1.9780898719598.ch8 'Lloyd N. Trefethen, "Chebyshev Series and the FFT",  , 75--86 (2000) '

# +
# %%file chebyshev.py
import numpy as np

def chebyshev(N, L=1):
    """Return (x, D, D2) Chebyshev derivatives and abscissa.

    The abscissa are spaced from -L to L inclusive with N+1 point.

    https://doi.org/10.1137/1.9780898719598.ch8
    """
    n = np.arange(N+1)
    x = np.cos(np.pi*n[::-1]/N)
    c = np.ones(N+1)
    c[0] = c[-1] = 2
    c *= (-1)**n    # Include signs
    i, j = (slice(None), None), (None, slice(None))
    with np.errstate(divide='ignore'):
        D = c[i]/c[j]/(x[i]-x[j] + np.eye(N+1))
    D -= np.diag(D.sum(axis=1))
    D2 = -D.T @ D
    return x*L, D/L, D2/L**2


# -

x, D, D2 = chebyshev(64, L=10)
plt.plot(x, D2 @ np.exp(-x**2) - (4*x**2 - 2)*np.exp(-x**2))

# # Hermite DVR Basis

# Here are the formulae from Bayes and Heenen 1985:

# +
# %%file hermite_DVR.py
import numpy as np
import scipy.special
sp = scipy

def hermite(N, L=1):
    """Return (x, D2) Hermite DVR basis derivatives and abscissa."""
    x, w = sp.special.roots_hermite(N)
    i = np.arange(N, dtype=int)[:, np.newaxis]
    xi = x[:, np.newaxis]
    xj = xi.T
    j = i.T
    T = (np.diag(4*N-1-2*x**2)/6*1.1
         + np.where(i==j, 0, (-1.0)**(i-j)*(2/(xi-xj)**2 - 0.5))
        )

    # Scale
    k = x.max()/L
    D2 = -T*k**2
    x /= k
    return (x, D2)


# -

# %load_ext autoreload
# %autoreload
import hermite_DVR
from hermite_DVR import hermite
L = 10.0
x, D2 = hermite(N=16, L=L)
plt.plot(x, D2 @ np.exp(-x**2) - (4*x**2 - 2)*np.exp(-x**2))

# ---
#
# # NB2
#
# The objective for these lectures is to be able to comfortably solve partial differential equations such as the SchrÃ¶dinger equation:
#
# $$
#   \I \hbar \pdiff{}{t} \psi(\vect{r}, t) = -\frac{\hbar^2}{2m}\nabla^2\psi(\vect{r}, t)
#   + V(\vect{r})\psi(\vect{r}, t).
# $$
#
# The approach will be to recognize that operators such as the Laplacian $\nabla^2$ are linear, hence equations of this form can be recast into a "matrix" equation:
#
# $$
#   \I \hbar \pdiff{}{t} \ket{\psi(t)} = \op{H}\ket{\psi(t)}, \qquad
#   \op{H} = \frac{\op{p}^2}{2m} + V(\op{x}), \qquad
#   \op{p} = - \I \hbar \vect{\nabla},
# $$
#
# and approached both generally and efficiently by identifying appropriate bases.  *Note: we are using physics bra-ket notation for vectors $\ket{f} \equiv \vec{f}$.  Think of these "ket"s as column vectors.*
#
# In the example of the time-independent SchrÃ¶dinger equation, for example, the solution can be fully specified in terms of a matrix exponential *(called a **propagator**)*:
#
# $$
#   \ket{\psi(t)} = \overbrace{e^{\op{H}t/\I\hbar}}^{\mat{U}(t)}\ket{\psi(0)}
# $$
#
# where the vector $\ket{\psi(0)}$ specifies the initial conditions at time $t=0$.

# # Numerical Derivatives
#
# The key challenge when solving differential equations is to deal numerically with derivatives.  Consider numerically calculating the derivative of a function $f(x)$.  One possible approach is to use the definition from calculus:
#
# $$
#   f'(x) = \lim_{h\rightarrow 0} \frac{f(x+h) - f(x)}{h}.
# $$
#
# To be explicit here, we consider:
#
# $$
#   f(x) = \sin(x), \qquad
#   f'(x) = \cos(x), \qquad
#   f''(x) = -\sin(x) = -f(x), \qquad
#   f^{(d)}(x) = - f^{(d-2)}(x),
# $$
#
# and evaluate the derivative at $x=1$.
#
# We can implement this numerically if we choose a small, but finite $h$.  This is called a forward finite difference approach.

# ## Exercise 1
#
# Explain what you see in this plot.  Specifically:
#
# 1. What is the source of the error for $h>10^{-7}$? Why is it linear on this log-log plot with slope 1?
# 2. What is the source of the error for $h<10^{-7}$? Why is it jagged?  Why is it roughly linear on this plot with slope -1?
# 3. What would the optimal step size $h$ be for this problem?  How could you estimate this before hand?
# 4. Notice that even with the optimal step size, the absolute error is about $10^{-8}$, even though we are working in double precision, where the machine precision is $\epsilon = 2^{-52} \approx 10^{-16}$.  Thus, we loose about half the digits of accuracy each time we apply this formula.

# ## Higher Order

# Hopefully you figured out that the error of the forward difference approximation for larger $h$ was due truncation errors in the Taylor expansion:
#
# $$
#   f(x+h) = f(x) + h f'(x) + \frac{h^2}{2} f''(x) + \frac{h^3}{3!} f'''(x) + \frac{h^4}{4!} f^{(4)}(x) \cdots,\\
#   \frac{f(x+h) - f(x)}{h} = f'(x) + \overbrace{\frac{h}{2} f''(x)}^{\text{leading error}} + \frac{h^2}{3!} f'''(x)  \cdots.
# $$
#
# One can improve the approach by using a **higher order** approximation, including not only $f(x)$ and $f(x+h)$, but additional points such as $f(x-h)$ or $f(x+2h)$.  By including more and more terms, one can eliminate higher and higher orders, obtaining more and more accurate approximations.  Of particular use is the **centered difference** approximation:
#
# $$
#   \frac{f(x+h) - f(x-h)}{2h} = f'(x) + \frac{h^2}{3} f^{'''}(x) + \cdots.
# $$
#
# Likewise, one can get a good approximation to the second derivative by similar manipulations:
#
# $$
#   \frac{f(x+h) + f(x-h) - 2f(x)}{h^2} = f''(x) + \frac{h^2}{12} f^{(4)}(x) + \cdots.
# $$

# ## Exercise 2

# 1. Derive the leading error estimates shown above.
# 2. Check these results numerically by modifying the FIX THIS sections below.

# +
# FIX THIS: Here are the previous forward difference formulae.  Updated them
# to implement the formulae above.  I am using vectorization here (see the 
# Programming note) but it should be clear how to modify this.  You can
# go back to a for loop if you like.
errs1 = abs(((f(x + hs) - f(x-hs)) / (2*hs)) - f(x, d=1))
errs2 = abs(((f(x + hs) + f(x - hs) - 2*f(x)) / (hs ** 2)) - f(x, d=2))

err_est1 = np.maximum(
    eps / hs / 2,                       # FIX THIS: Roundoff error - is this still okay?
    abs(hs ** 2 * f(x, d=3) / 3),   # Truncation error from above
)
err_est2 = np.maximum(
    eps / hs**2,                         # FIX THIS: Roundoff error - is this still okay?
    abs((hs ** 2) * f(x, d=4) / 12),  # Truncation error from above
)

fig, axs = plt.subplots(1, 2, figsize=(15, 5))
ax1, ax2 = axs
ax1.loglog(hs, errs1)
ax2.loglog(hs, errs2)
ax1.loglog(hs, err_est1, ":")
ax2.loglog(hs, err_est2, ':')

# Hint: error estimates.  Where do these come from?
eps = np.finfo(float).eps  # Machine precision

ax1.set(xlabel="h", ylabel="Absolute Error", title="Centered First Derivative");
ax2.set(xlabel="h", ylabel="Absolute Error", title="Centered Second Derivative");


# -

# Here is what you should get if you have the correct formulae:
#
# <!-- 
# errs1 = abs((f(x + hs) - f(x - hs)) / 2 / hs - f(x, d=1))
# errs2 = abs((f(x + hs) + f(x - hs) - 2 * f(x)) / hs ** 2 - f(x, d=2))
# -->
# ![index.png](attachment:index.png)

# **Challenge:** can you come up with a better solution?  Here are some solutions with minimal explanation to show you that there are other approaches when accuracy is needed.  Often there is a trade-off between accuracy and speed.  The first is based on complex arithmetic - if your libraries can compute in the complex plane, then they can accurately compute the real derivative.  The second is based on the process of [Richardson extrapolation](https://sites.math.washington.edu/~greenbau/Math_498/lecture04_richardson.pdf).  The idea is to compute the centered difference at a variety of step sizes $h$ and then extrapolate to the $h\rightarrow 0$ limit before roundoff errors set in:
#
# $$
#   d_{m,0} = \frac{f(x + h/2^m) - f(x - h/2^m)}{2 h/2^m}, \qquad
#   d_{m,n} = \frac{4^n d_{m,n-1} - d_{m-1, n-1}}{4^n - 1}.
# $$

# +
def derivative_1(f, x=x):
    """This is kind of a cheat, but works in the right contexts... why?"""
    h = 1j*eps
    return ((f(x+h) - f(x))/h).real

def derivative_2(f, x=x, h=1, levels=5):
    # h: initial step size
    m = np.arange(levels)
    ds = [(f(x+h/2**m) - f(x-h/2**m))/(2*h/2**m)]
    for n in range(1, levels):
        dn = ds[-1]  # d_{m,n-1}
        ds.append((4**n * dn[1:] - dn[:-1]) / (4**n - 1))
    return ds[-1][-1]


# -

print(f"Err 1: {abs(derivative_1(f) - f(x, d=1))}")
for levels in range(4,7):
    print(f"Err 2: {abs(derivative_2(f, levels=levels) - f(x, d=1)):.2g} ({levels} levels)")

# ## Derivatives are Linear

# Recall that derivatives are linear.  Notice that in all of the previous formula, $f()$ appears linearly, so that if we multiply $f()$ by a factor the derivatives are multiplied by the same factor.  This means that we can represent the action of the derivative as a matrix.  The simplest way to do this is to represent $f()$ numerically as a vector by evaluating it on a set of **abscissa** $x_n$, which are often taken as equally spaced with spacing $\d{x}$:
#
# $$
#   \ket{f} \equiv \begin{pmatrix}
#     f_0 = f(x_0)\\
#     f_1 = f(x_1)\\
#     \vdots\\
#     f_{N-1} = f(x_{N-1})
#   \end{pmatrix}, \qquad
#   x_n = n\overbrace{\frac{N}{L}}^{h=\d{x}} = n\;\d{x}.
# $$
#
# *Here we will use the notation $f_n = [\ket{f}]_n$ to represent the numerical components of this vector.  Later, we will generalize this to think of $f_n = \braket{n|f}$ as the component in a particular basis of states $\ket{n}$ where $\braket{n|f} \equiv \braket{\ket{n}, \ket{f}}$ is the bra-ket notation for an inner product and $\bra{n} \equiv \ket{n}^{\dagger}$ is the Hermitian conjugate (conjugate transpose) of the vector (ket) $\ket{n}$.*
#
# In this case, we can represent functions on the interval $x\in[0, L)$.  Consider our original function $f(x) = \sin(x)$ on the interval from $[0, 5)$ with $N=10$ points:

L = 5
N = 10
dx = L/N
print('dx =', dx)
n = np.arange(N)
x = n * dx
ax = plt.gca()
ax.plot(x, f(x), 'x-')
ax.set(xlabel='x', ylabel='f(x)', title=f"N={N}");

# Note that we can now compute the derivative of this function using our finite difference formula, which can now be implemented as matrix multiplication.  For convenience, we introduce the following notation:
#
# $$
#   \overbrace{D_{+h}(f) = \frac{f(x+h) - f(x)}{h}}^{\text{forward difference}}, \qquad
#   \overbrace{D_{-h}(f) = \frac{f(x) - f(x-h)}{h}}^{\text{backward difference}} , \qquad  
#   \overbrace{D_{h}(f) = \frac{f(x+h) - f(x-h)}{2h}}^{\text{centered difference}}.
# $$
#
# Consider the forward difference operator:
#
# $$
#   D_{+h}(f)(x_n) = \frac{f(x_n+h) - f(x_n)}{h} = \frac{f(x_{n+1}) - f(x_n)}{h}
#                  = \frac{f_{n+1} - f_n}{h}.
# $$
#
# Let $\ket{f'_{+h}}$ be the vector with coefficients $D_{+h}(f)(x_n)$.  The derivative can thus be represented as a matrix equation: $\ket{f'_{+h}} = \mat{D}_{+h}\ket{f}$:
#
# $$
#   \overbrace{
#   \begin{pmatrix}
#     D_{+h}(f)(x_0)\\
#     D_{+h}(f)(x_1)\\
#     \vdots\\
#     D_{+h}(f)(x_{N-1})    
#   \end{pmatrix}}^{\ket{f'_{+h}}}
#   = \overbrace{
#   \frac{1}{h}
#   \begin{pmatrix}
#     -1 & 1 \\
#        & -1 & 1 \\
#        &    & \ddots & \ddots\\
#        &    &        & -1 & 1\\
#        &    &        &    & -1
#   \end{pmatrix}}^{\mat{D}_{+h}}\cdot
#   \overbrace{
#   \begin{pmatrix}
#     f_0\\
#     f_1\\
#     \vdots\\
#     f_{N-2}\\
#     f_{N-1}
#   \end{pmatrix}}^{\ket{f}}.
# $$
#
# That is, the forward difference matrix $\mat{D}_{+h}$ is band-diagonal with diagonal entries $-1/h$ and off-diagonal entries to the right of $+1/h$.  This can be constructed in code using `np.diag` which takes an argument $k$ specifying which diagonal to set:

D1_forward = (np.diag(np.ones(N-1), k=1) - np.diag(np.ones(N)))/dx
D1_forward

# Here is what it does when acting on our vector $\ket{f}$:

plt.plot(x, f(x, d=1), label = "f'(x)")
plt.plot(x, D1_forward @ f(x), '-+', label=r'$D_{+h}\cdot f$')
plt.legend()

# The approximation is qualitatively correct, but not very good because our step size is very large $\d{x} = 0.5$.  If you look carefully, you might see that we could do better by shifting the results half a cell to the right: this is exactly what the centered difference formula will do.  The last point, however, is very badly approximated.  This is due to the boundary condition we implicitly chose in our matrix - Dirichlet boundary conditions, which assume that the function is at the boundaries.  This is a good approximation for $\sin(x)$ at the left $x=0$, but bad at the right $x=L=5$.  We will come back to this shortly, but first we want to discuss a few "tricks".
#
# From the matrix representation of the forward difference operator $\mat{D}_{+h}$ it should be clear that the backwards difference operator $\mat{D}_{-h} = -\mat{D}_{+h}^T$ is the negative transpose and that the centered difference is the average of these: $\mat{D}_{h} = (\mat{D}_{+h} + \mat{D}_{-h})/2$.  A little less obvious is that the centered second derivative is $\mat{D}^2_{h} = \mat{D}_{+h}\cdot\mat{D}_{-h}$.

# **Finite Difference Matrices**: Equally spaced abscissa with spacing $\d{x} = h$ and Dirichlet boundary conditions.
#
# \begin{align}
#   \mat{D}_{+h}
#   &= 
#   \frac{1}{h}
#   \begin{pmatrix}
#     -1 & 1 \\
#        & -1 & 1 \\
#        &    & \ddots & \ddots\\
#        &    &        & -1 & 1\\
#        &    &        &    & -1
#   \end{pmatrix}, &
#   \mat{D}_{-h}
#   &= 
#   \frac{1}{h}
#   \begin{pmatrix}
#     1 &\\
#     -1 & 1 \\
#        &    \ddots & \ddots\\
#        &    & -1 & 1\\
#        &    &    & -1 & 1\\
#   \end{pmatrix},\\
#   \mat{D}_{h} = \frac{\mat{D}_{+h} + \mat{D}_{-h}}{2}
#   &= 
#   \frac{1}{2h}
#   \begin{pmatrix}
#      & 1\\
#     -1 &  & 1 \\
#        &    -1 &  & \ddots\\
#        &    & \ddots & & 1\\
#        &    &    & -1 
#   \end{pmatrix}, & 
#   \mat{D}^2_{h} = \mat{D}_{+h}\cdot\mat{D}_{-h}
#   &= 
#   \frac{1}{2h}
#   \begin{pmatrix}
#      -2& 1\\
#       1 & -2 & 1 \\
#        &    1 & \ddots & \ddots\\
#        &    & \ddots & -2 & 1\\
#        &    &    & 1 & -2
#   \end{pmatrix}.
# \end{align}

D1_forward = (np.diag(np.ones(N-1), k=1) - np.diag(np.ones(N)))/dx
D1_backward = -D1_forward.T
D1 = (D1_forward + D1_backward)/2
D2 = D1_forward @ D1_backward
plt.plot(x, f(x, d=1), label = "f'(x)")
plt.plot(x, D1 @ f(x), '-+', label=r"$D_{h}\cdot f$")
plt.plot(x, f(x, d=2), '--', label="f''(x)")
plt.plot(x, D2 @ f(x), '--x', label=r"$D^2_{h}\cdot f$")
plt.legend()

# ## Boundary Conditions

# There are several ways of dealing with boundary conditions.  One straightforward approach is to include an additional point on either side specifying the appropriate boundary conditions.  Dirichlet boundary conditions are very easy to implement this way since the additional points are just zero $f_{-1} = f_{N} = 0$.
#
# $$
# \mat{D}^2_{h}\ket{f} = 
#   \frac{1}{2h}
#   \begin{pmatrix}
#      \color{red}{1} & -2& 1\\
#       & 1 & -2 & 1 \\
#       &  &    1 & \ddots & \ddots\\
#       &  &    & \ddots & -2 & 1\\
#       & &    &    & 1 & -2 & \color{red}{1}
#   \end{pmatrix} \cdot
#   \begin{pmatrix}
#   \color{red}{f_{-1}}\\
#   f_0\\
#   f_1\\
#   \vdots\\
#   f_{N-1}\\
#   \color{red}{f_{N}}
#   \end{pmatrix}.
# $$
#
# With this formalism, Neumann boundary conditions can be implemented by setting $\color{red}{f_{-1}} = f_{0}$ and $\color{red}{f_{N}} = f_{N-1}$, and periodic boundary conditions can be implemented by setting $\color{red}{f_{-1}} = f_{N-1}$ and $\color{red}{f_{N}} = f_{0}$.  This gives, for example:
#
# $$
#   \mat{D}^2_{h, \text{Neumann}} = 
#   \frac{1}{2h}
#   \begin{pmatrix}
#      -1 & 1\\
#      1 & -2 & 1 \\
#        &    1 & \ddots & \ddots\\
#        &    & \ddots & -2 & 1\\
#        &    &    & 1 & -1 &
#   \end{pmatrix}, \qquad
#   \mat{D}^2_{h, \text{periodic}} = 
#   \frac{1}{2h}
#   \begin{pmatrix}
#      -2& 1 & & & 1\\
#      1 & -2 & 1 \\
#        &    1 & \ddots & \ddots\\
#        &    & \ddots & -2 & 1\\
#      1 &    &    & 1 & -2 &
#   \end{pmatrix}.
# $$
#
# **Note**: The matrix $\mat{D}^2_{h, \text{Neumann}}$ is actually not very good for production work.  While it does implement Neumann boundary conditions, the derivatives at the boundaries are only accurate to order $\order(h)$ while on the interior the derivative is accurate to order $\order(h^2)$.

# # The SchrÃ¶dinger Equation

# We can now start solving the SchrÃ¶dinger equation.  For simplicity, we will consider 1D, but the idea is straightforward to generalize to higher dimensions.  We start by looking for solutions of the form:
#
# $$
#   \psi(x, t) = \sum_{n} e^{E_n t/\I\hbar} \psi_n(x)\\
#   \I\hbar \pdiff{}{t}\psi(x, t) 
#   = \sum_{n} E_n e^{E_n t/\I\hbar} \psi_n(x)
#   = \sum_{n} \left(\frac{-\hbar^2}{2m} \nabla^2 + V(x)\right)\psi_n(x).
# $$
#
# We can thus solve the problem if we can find the solutions $\psi_n(x)$ that satisfy the **time-independent SchrÃ¶dinger Equation**:
#
# $$
#   \left(\frac{-\hbar^2}{2m} \nabla^2 + V(x)\right)\psi_n(x) = E\psi_n(x).
# $$
#
# The idea is to replace the Laplacian with the second derivative operator.  This makes the entire right-hand-side a matrix:
#
# $$
#   \left(\frac{-\hbar^2}{2m} \nabla^2 + V(x)\right) \rightarrow \mat{H} = \frac{-\hbar^2}{2m}\mat{D}_h^2 + \diag{V(x_n)}.
# $$
#
# We can then use standard linear-algebra techniques to find the eigenvalues:
#
# $$
#   \mat{H}\ket{n} = \ket{n}E_n.
# $$
#
# As an example, we solve for the eigenstates of the harmonic oscillator:
#
# $$
#   V(x) = \frac{m\omega^2 x^2}{2}.
# $$
#
# This is analytically solvable and has solutions of the form:
#
# $$
#   \psi_n(x) \propto  H_n(\tfrac{x}{a})e^{-x^2/2a^2}, 
#   \qquad E_n = \hbar \omega (n + \tfrac{1}{2}), \qquad
#   a = \sqrt{\frac{\hbar}{m\omega}}
# $$
#
# where $H_n(z)$ are the Hermite polynomials.

# +
m = w = hbar = 1
a = np.sqrt(hbar / m / w)  # Harmonic oscillator length
N = 1024
L = 10.0
dx = L / N
n = np.arange(N)
x = n * dx - L / 2
D2 = (
    np.diag(np.ones(N - 1), k=1)
    + np.diag(np.ones(N - 1), k=-1)
    - 2 * np.diag(np.ones(N))
) / dx ** 2

Vx = m * w ** 2 / 2 * x ** 2

# Make the matrix: we use a diagonal matrix for the potential.
H = -(hbar ** 2) / 2 / m * D2 + np.diag(Vx)
Es_exact = hbar * w * (n + 1 / 2)

# Check that H is Hermitian.  This allows us to use the more efficient and accurate
# np.linalg.eigh rather than np.linalg.eig.
assert np.allclose(H, H.T.conj())

# Here we solve for the eigenstates (wavefunctions) and the energies
Es, psis = np.linalg.eigh(H)

fig, axs = plt.subplots(1, 2, figsize=(15, 5))
ax0, ax1 = axs
ax0.semilogy(n, abs(Es / Es_exact - 1), "-+")
ax0.set(ylabel="Relative error in $E_n$", xlabel="n")

states_to_plot = 7
ax1.plot(x / a, Vx / hbar / w, scalex=False)
for n in range(states_to_plot):
    l, = ax1.plot(x / a, Es[n] / hbar / w + 3 * psis[:, n]) # shift up by constant, then plot wave func
    ax1.axhline([Es[n] / hbar / w], ls='--', c=l.get_c())
ax1.set(ylabel=r"$E_n/\hbar\omega$", xlabel="x/a", ylim=(0, Es[states_to_plot]));
# -

# ## Exercise 3
#
# * Play a little with this code.  How does the accuracy depend on the number of points $N$ and on the size of the box $L$?  Can you explain the observed convergence?
# * Now solve your own problem.  Use analogous code to find the states and energies for one of the following potentials:
#
#   $$
#     V(x) = \sqrt{m\hbar \omega^3}\abs{x}, \qquad
#     V(x) = \frac{m^2\omega^3}{\hbar}x^4.
#   $$
#   
#   Since you do not know the analytic solution in these cases, how can you check the convergence of your results?

# +
# First solution
m = w = hbar = 1
a = np.sqrt(hbar / m / w)  # Harmonic oscillator length
N = 256
L = 10.0
dx = L / N
n = np.arange(N)
x = n * dx - L / 2
D2 = (
    np.diag(np.ones(N - 1), k=1)
    + np.diag(np.ones(N - 1), k=-1)
    - 2 * np.diag(np.ones(N))
) / dx ** 2

Vx = np.sqrt(500 * m * hbar * w**3) * abs(x)

# Make the matrix: we use a diagonal matrix for the potential.
H = -(hbar ** 2) / 2 / m * D2 + np.diag(Vx)
Es_exact = hbar * w * (n + 1 / 2)

# Check that H is Hermitian.  This allows us to use the more efficient and accurate
# np.linalg.eigh rather than np.linalg.eig.
assert np.allclose(H, H.T.conj())

# Here we solve for the eigenstates (wavefunctions) and the energies
Es, psis = np.linalg.eigh(H)

fig, axs = plt.subplots(1, 2, figsize=(15, 5))
ax0, ax1 = axs
ax0.semilogy(n, abs(Es / Es_exact - 1), "-+")
ax0.set(ylabel="Relative error in $E_n$", xlabel="n")

states_to_plot = 7
ax1.plot(x / a, Vx / hbar / w, scalex=False)
for n in range(states_to_plot):
    l, = ax1.plot(x / a, Es[n] / hbar / w + 3 * psis[:, n]) # shift up by constant, then plot wave func
    ax1.axhline([Es[n] / hbar / w], ls='--', c=l.get_c())
ax1.set(ylabel=r"$E_n/\hbar\omega$", xlabel="x/a", ylim=(0, Es[states_to_plot]));

# +
# First solution
m = w = hbar = 1
a = np.sqrt(hbar / m / w)  # Harmonic oscillator length
N = 256
L = 10.0
dx = L / N
n = np.arange(N)
x = n * dx - L / 2
D2 = (
    np.diag(np.ones(N - 1), k=1)
    + np.diag(np.ones(N - 1), k=-1)
    - 2 * np.diag(np.ones(N))
) / dx ** 2

Vx = m**2 * w**3 * x**4 / hbar

# Make the matrix: we use a diagonal matrix for the potential.
H = -(hbar ** 2) / 2 / m * D2 + np.diag(Vx)
Es_exact = hbar * w * (n + 1 / 2)

# Check that H is Hermitian.  This allows us to use the more efficient and accurate
# np.linalg.eigh rather than np.linalg.eig.
assert np.allclose(H, H.T.conj())

# Here we solve for the eigenstates (wavefunctions) and the energies
Es, psis = np.linalg.eigh(H)

fig, axs = plt.subplots(1, 2, figsize=(15, 5))
ax0, ax1 = axs
ax0.semilogy(n, abs(Es / Es_exact - 1), "-+")
ax0.set(ylabel="Relative error in $E_n$", xlabel="n")

states_to_plot = 7
ax1.plot(x / a, Vx / hbar / w, scalex=False)
for n in range(states_to_plot):
    l, = ax1.plot(x / a, Es[n] / hbar / w + 3 * psis[:, n]) # shift up by constant, then plot wave func
    ax1.axhline([Es[n] / hbar / w], ls='--', c=l.get_c())
ax1.set(ylabel=r"$E_n/\hbar\omega$", xlabel="x/a", ylim=(0, Es[states_to_plot]));
# -

# <!--
# m = w = hbar = 1
# a = np.sqrt(hbar / m / w)  # Harmonic oscillator length
# N = 256
# L = 10.0
# dx = L / N
# n = np.arange(N)
# x = n * dx - L / 2
# D2 = (
#     np.diag(np.ones(N - 1), k=1)
#     + np.diag(np.ones(N - 1), k=-1)
#     - 2 * np.diag(np.ones(N))
# ) / dx ** 2
#
# Vx = m**2 * w**3 /hbar  * x ** 4
# Vx = np.sqrt(m * hbar * w**3) * abs(x)
# #Vx = - hbar * w * np.exp(-(x/a)**2)
# H = -(hbar ** 2) / 2 / m * D2 + np.diag(Vx)
#
# # Check that H is Hermitian.  This allows us to use the more efficient and accurate
# # np.linalg.eigh rather than np.linalg.eig.
# assert np.allclose(H, H.T.conj())
#
# # Here we solve for the eigenstates (wavefunctions) and the energies
# Es, psis = np.linalg.eigh(H)
#
# fig, axs = plt.subplots(1, 2, figsize=(15, 5))
# ax0, ax1 = axs
# ax0.plot(n[:10], Es[:10], "+")
# ax0.set(ylabel="$E_n$", xlabel="n")
#
# states_to_plot = 7
# ax1.plot(x / a, Vx / hbar / w, scalex=False)
# for n in range(states_to_plot):
#     l, = ax1.plot(x / a, Es[n] / hbar / w + 3 * psis[:, n])
#     ax1.axhline([Es[n] / hbar / w], ls='--', c=l.get_c())
# ax1.set(ylabel=r"$E_n/\hbar\omega$", xlabel="x/a", ylim=(min(0, Es[0]-1), Es[states_to_plot]));
# -->

# # Roundoff Error

h = np.linspace(-1e-13,+1e-13,1000)
x = h + 1
y = (h)**8
y_ = (x**9 - 9*x**8 + 36*x**7 - 84*x**6 + 126*x**5 - 126*x**4 + 84*x**3 - 36*x**2 + 9*x - 1)/(x-1)
plt.plot(x, y)
plt.plot(x, y_)

str(((sympy.var('x')-1)**9).expand())

# # Fourier Basis

# Here we present some more details about the Fourier bases.  I use the typical physics normalization here:
#
# * Functions on $x \in (-\infty, \infty)$:
#
#   $$
#     \braket{f|g} = \int_{-\infty}^{\infty}f^*(x)g(x)\d{x}, \qquad
#     \op{1} = \int_{-\infty}^{\infty}\ket{x}\bra{x}\d{x} = \int_{-\infty}^{\infty}\ket{k}\bra{k}\frac{\d{k}}{2\pi},\qquad
#     \braket{x|k} = e^{\I k x}.
#   $$
#
# * Periodic functions on $x \in [0, L)$:
#
#   $$
#     \braket{f|g} = \int_{0}^{L}f^*(x)g(x)\d{x}, \qquad
#     \op{1} = \int_{0}^{L}\ket{x}\bra{x}\d{x} = \frac{1}{L}\sum_{n}\ket{k_n}\bra{k_n},\qquad
#     k_n = \left.\frac{2\pi n}{L}\right|_{n=0}^{N-1}, \qquad
#     \braket{x|k_n} = e^{\I k_n x}.
#   $$
#
# * Periodic functions on a lattice $x \in \{x_n = n\d{x}\}$ for $n \in \{0, 1, \cdots, N-1\}$:
#
#   $$
#     \braket{f|g} = \sum_{n=0}^{N-1} f^*(x_n) g(x_n), \qquad
#     \op{1} = \overbrace{\frac{L}{N}}^{\d{x}}\sum_{n}\ket{x_n}\bra{x_n} 
#            = \overbrace{\frac{1}{L}}^{\d{k}/2\pi}\sum_{n}\ket{k_n}\bra{k_n},\qquad
#     x_n = \left.\frac{L}{N}\right|_{N=0}^{N-1}, \qquad
#     k_n = \left.\frac{2\pi n}{L}\right|_{n=0}^{N-1}, \qquad
#     \braket{x_m|k_n} = e^{\I k_n x_m}.
#   $$
#
#   Note that these can be expressed without any reference to $L$ or $\d{x}$ if we just regard $f_n = \braket{x_n|f} = f(x_n)$ and $\tilde{f}_{m} = \braket{k_m|f}$ as a collection of numbers.  It is on these that the fast Fourier transform FFT acts:
#
#   $$
#     \braket{f|g} = \sum_{n=0}^{N-1} f_n^* g_n, \qquad
#     \op{1} = \overbrace{\frac{L}{N}}^{\d{x}}\sum_{n}\ket{x_n}\bra{x_n} 
#            = \overbrace{\frac{1}{L}}^{\d{k}/2\pi}\sum_{n}\ket{k_n}\bra{k_n},\qquad
#     \braket{x_m|k_n} = e^{2\pi\I n m/N} .
#   $$

# ## Exercise
# The FFT algorithm simply changes bases from the position basis $\ket{x}$ to the momentum basis $\ket{k}$ and back:
#
# $$
#   \DeclareMathOperator{\FFT}{FFT}
#   \DeclareMathOperator{\IFFT}{IFFT}
#   \FFT(\vect{f}) = \mat{F}\cdot \vect{f}, \qquad
#   \IFFT\big(\FFT(\vect{f})\big) = \mat{F}^{-1}\cdot\mat{F}\cdot \vect{f} = \vect{f}.
# $$
#
# Explicitly compute the transformation matrix $\mat{F}$ and check that this is what the code does.
# * Pay particular attention to the normalization.  *($\mat{F}^{-1} = \mat{F}^\dagger/N$ for most implementations.)*
# * Note also that if you use a centered lattice $x_n = n\d{x} - L/2$, then you will need to include another phase factor in $\mat{F}$.  Explicitly check this.

# +
import numpy as np, matplotlib.pyplot as plt

N = 256
L = 10.0
dx = L/N
x = np.arange(N)*dx
k = 2*np.pi * np.fft.fftfreq(N, dx)
F = np.exp(-1j*k[:, np.newaxis]*x[np.newaxis, :])

np.random.seed(3)
f = (np.random.random(2*N) - 0.5
).view(dtype=complex)
assert np.allclose(np.fft.fft(f), F @ f)
assert np.allclose(np.fft.ifft(f), F.T.conj() @ f / N)

# +
import time
Ns1 = np.arange(2, 1024)
Ns2 = 2**np.arange(1, 11)
Ns3 = [_N for _N in sorted(set((2**np.arange(1, 12)[:, None]*3**np.arange(1, 12)[None, :]).ravel()))
      if _N < 1025]
Nrun = 10
Nsamp = 20

Nss = [Ns1, Ns2, Ns3]
tss = []
for Ns in Nss:
    ts = []
    for N in Ns:
        f = f = (np.random.random(2*N) - 0.5).view(dtype=complex)
        _ts = []
        for _n in range(Nsamp):
            tic = time.time()
            for _n in range(Nrun):
                np.fft.fft(f)
            t = (time.time() - tic)/Nrun
            _ts.append(t)
        ts.append(min(_ts))
    tss.append((Ns, ts))

for Ns, ts in tss:
    plt.semilogy(Ns, ts);

plt.xlabel('Size of array')
plt.ylabel('Time/FFT (s)')
#plt.plot(Ns, Ns*np.log(Ns) / (Ns[-1]*np.log(Ns[-1])) * ts[-1], '--')
#plt.plot(Ns, np.array(Ns)**2 / (Ns[-1]**2) * ts[-1], '--')
# -

# # Multiplication

res = np.fft.ifft(
    np.fft.fft([0,0,0,0,1000,200,30,4])
    * np.fft.fft([0,0,0,0,0,300,20,1])).real.round(0)
sum(res), 1234*321

# # PSD

1234*234, 29*1000+20*100+11*10+4*1

# Sample demonstration how to use FFT to extract a single with the Power-Spectral Density (PSD).

# +
N = 1024
T = 1.0
dt = T/N
t = np.arange(N)*dt
f = 20.1
y = np.sin(2*np.pi * f*t)
np.random.seed(3)
dy = np.random.normal(size=N)
fs = np.fft.fftfreq(N, d=dt)

fig, axs = plt.subplots(1,2, figsize=(20,5))
axs[0].plot(t, y+dy)
axs[0].plot(t, y)
axs[0].set(xlabel='t', ylabel='y')

axs[1].psd(y+dy, Fs=N/T);
axs[1].psd(y, Fs=N/T);
axs[1].axvline(f, c='y')

# +
fig, axs = plt.subplots(2, 1, figsize=(20,10))
axs[0].semilogy(np.fft.fftshift(fs), abs(np.fft.fftshift(np.fft.fft(y+dy)))**2)
axs[0].axvline([-f], c='y')
axs[0].axvline([f], c='y')

axs[1].semilogy(np.fft.fftshift(fs), abs(np.fft.fftshift(np.fft.fft(y+dy)))**2)
axs[1].axvline([-f], c='y')
axs[1].axvline([f], c='y')

axs[1].set(xlim=(-30,30))
# -

# # Time Dependent Schrödinger Equation

# ## Particle in a Box

# Here we solve the problem of a particle in a box as discussed in class.  The Schrödinger equation has the form
#
# $$
#   \I \hbar \dot{\psi}(x, t) = \frac{-\hbar^2\psi''(x, t)}{2m} + V(x, t)\psi(x, t).
# $$
#
# A particle in a box has the following potential:
#
# $$
#   V(x, t) = \begin{cases}
#     0 & 0<x<L,\\
#     \infty & \text{otherwise}.
#   \end{cases}
# $$
#
# The eigenstates are:
#
# $$
#   \psi_n(x) = \braket{x|n} = \sqrt{\frac{2}{L}}\sin(k_n x), \qquad
#   k_n = \left.\frac{\pi n}{L}\right|_{n=1}^{\infty}, \qquad
#   E_n = \frac{\hbar^2k_n^2}{2m}.
# $$
#
# These satisfy the boundary conditions that $\psi(0)= \psi(L) = 0$ and the SEQ in the interior.

# ### Sudden Expansion

# In class, we considered the problem of what happens to the ground state $\ket{1}$ when the box is suddenly changed to have length $2L$.  To deal with this, we first express the eigenstates $\ket{n'}$ of box of length $2L$:
#
# $$
#   \braket{x|n'} = \sqrt{\frac{2}{2L}}\sin(k_{n'} x) 
#                          = \sqrt{\frac{1}{L}}\sin(k_{n'} x), \qquad
#   k_{n'} = \left.\frac{\pi n'}{2L}\right|_{n'=1}^{\infty}, \qquad
#   E_{n'} = \frac{\hbar^2k_{n'}^2}{2m} = \frac{\hbar^2 \pi^2}{8mL^2}{n'}^2.
# $$
#
# The strategy is to express the initial state $\ket{1}$ in terms of the new eigenstates $\ket{n'}$ and then to follow the time evolution in the new eigenbasis of the Hamiltonian in the expanded box:
#
# $$
#   \ket{1, t=0} = \sum_{n'}\ket{n'}\braket{n'|1}
#                        = \sum_{n'}\ket{n'}\int_{0}^{L} \sqrt{\frac{2}{L}}\sqrt{\frac{1}{L}}\sin(k_{n'}x)\sin(k_1x)\d{x}
#                        = \frac{\sqrt{2}}{L}\sum_{n'}\ket{n'}\int_{0}^{L} \sin\frac{\pi n' x}{2L}\sin\frac{\pi x}{L}\d{x} = \\
#   = \frac{\sqrt{2}}{\pi}\sum_{n'}\ket{n'} \int_{0}^{\pi} \sin\frac{n'\theta}{2}\sin\theta\d{\theta}.
# $$
#
# The integral can be evaluated by converting to exponential form $\sin\theta = \frac{e^{\I\theta}-e^{-\I\theta}}{2}$, and after some algebra, we have:
#
# $$
#   \ket{1, t=0} = \frac{\sqrt{2}}{\pi}\sum_{n'}\ket{n'}\frac{4\sin\frac{n'\pi}{2}}{4-{n'}^2}
#                        = \frac{\ket{2'}}{\sqrt{2}} + \frac{\sqrt{2}}{\pi}\sum_{n' \text{ odd}}^{\infty}\ket{n'}\frac{4(-1)^{(n'-1)/2}}{4-{n'}^2}\\
#   = \frac{\ket{2'}}{\sqrt{2}} 
#       + \frac{4\sqrt{2}}{\pi}\left(
#       - \frac{\ket{1'}}{3\times (-1)}
#      + \frac{\ket{3'}}{5\times 1} 
#      - \frac{\ket{5'}}{7\times 3} 
#      + \frac{\ket{7'}}{9\times 5} 
#      - \frac{\ket{9'}}{11\times 7}
#      %+ \frac{\ket{11'}}{13\times 9}
#      %- \frac{\ket{13'}}{15\times 11}
#      + \dots
#      \right)
# $$
#
# All even terms vanish except for $n'=2$ which has the specified limiting value.

# +
# %pylab inline --no-import-all
N_terms = 5000
L = 1.0
x = np.linspace(0, 2*L, 100)   # Abscissa for plotting
n = np.arange(N_terms)         # Odd n'
c_n = np.sqrt(2)/np.pi * np.where(n%2 == 0, 0, 4*(-1)**((n-1)/2)/(4-n**2))
c_n[2] = 1./np.sqrt(2)

assert np.allclose(sum(c_n**2), 1)  # Check normalization
x_ = x[None, :]
n_ = n[:, None]
c_n_ = c_n[:, None]
hbar = m = 1.0
k_n_ = (np.pi*n_/2/L)
E_n_ = (hbar*k_n_)**2/2/m

def get_psi(t):
  """Return the time-dependent wavefunction."""
  return (np.exp(E_n_*t/1j/hbar)
          * c_n_ 
          * np.sin(k_n_*x_)*np.sqrt(2/2/L)
         ).sum(axis=0)

def get_dpsi(t):
  """Return the derivative of the time-dependent wavefunction"""
  return (np.exp(E_n_*t/1j/hbar)
          * c_n_ * k_n_
          * np.cos(k_n_*x_)*np.sqrt(2/2/L)
         ).sum(axis=0)

plt.plot(x, abs(get_psi(t=0))**2)

# +
from ipywidgets import interact

tau = 16*m*L**2/hbar/np.pi

@interact(t_tau=(0, 1, 0.001))
def draw(t_tau=0):
    plt.plot(x, abs(get_psi(t=t_tau*tau))**2)
    plt.axis([0,2,0,2.2])


# +
#@title Particle in a Box { run: "auto", display-mode: "form" }
from collections import namedtuple
from matplotlib import animation, rc
from IPython.display import HTML, clear_output
from matplotlib.patches import Rectangle

tau = 16*m*L**2/hbar/np.pi

# animate over some set of ts
Nt = 492          #@param {type:"slider", min:0, max:1000, step:2}
t_max_tau = 0.7   #@param {type:"slider", min:0.1, max:1.0, step:0.1}
t_max = t_max_tau * tau
ts = np.linspace(0, t_max, Nt+1)

# First set up the figure, the axes, and the plot element
fig, ax = plt.subplots()
plt.close()
ax.set_xlim((-0.1, 2*L+0.1))
ax.set_ylim((0, 2.3))
_rect_args = dict(width=0.1, height=2.3, hatch='\\', fill=False)
ax.add_patch(Rectangle((-0.1,0), **_rect_args))
p = ax.add_patch(Rectangle((L,0.), **_rect_args))

# We use a namedtuple to store the observables.  They can
# then be accessed by name later on.
Observables = namedtuple(
    'Observables',
    ['density', 'x_avg'])

def get_observables(t):
    """Return various observables at time t.
    
    Returns
    -------
    density : abs(psi)^2
    x_avg : expecatation value of x
    """
    psi = get_psi(t=t)
    #dpsi = get_dpsi(t=0)
    #j = (psi.conj()*(-1j*hbar*dpsi)/m).real
    density = abs(psi)**2
    x_avg = (density*x).sum()/density.sum()
    
    return Observables(
        density=density,
        x_avg=x_avg)

line1, = ax.plot([], [], 'b-', lw=2)
#ax2 = plt.twinx()
#line2, = ax.plot([], [], 'g--', lw=2)
line_x = ax.axvline(0.1, color='y')
title = ax.set_title(r"$t/\tau=0$")

# initialization function: plot the background of each frame
def init():
    obs = get_observables(t=0)
    density = obs.density
    line1.set_data(x, density)
    #line2.set_data(x, obs.j)
    #line_x.set_data(obs.x_avg)
    return (line1,)

# animation function: this is called sequentially
def animate(i):
    t = ts[i] 
    # Update data
    obs = get_observables(t=t)
    line1.set_data(x, obs.density)
    #line2.set_data(x, obs.j)    
    line_x.set_data([(obs.x_avg,)*2, (0,1)])
    if i > 0:
        # Move right wall after first step.
        p.set_xy((2*L, 0))
        title.set_text(r"$t/\tau={:.4f}$".format(t/tau))
    return (line1,)

anim = animation.FuncAnimation(
    fig, animate, init_func=init, frames=len(ts), 
    interval=50, repeat=False, blit=True)
plt.close('all')
display(anim)
# -

x = np.linspace(-1,1,100)[:, None]
n = np.arange(-100, 100)[None, :]
th = np.exp(-np.pi*x**2*n).sum(axis=-1)
plt.plot(x, th)
#plt.ylim(-4,4)


