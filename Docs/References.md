(sec:readings)=
Resources, Readings, and References
===================================

## Books

The literature on QFT is vast and no two books seem to attack the topic the same way. Any list of books will be heavily biased towards my own tastes.

References
==========

```{bibliography}
:style: alpha
```
