Phys  - Quantum Fields
======================

[![pipeline status](https://gitlab.com/corbin-physics/quantum-fields/badges/master/pipeline.svg)](https://gitlab.com/corbin-physics/quantum-fields/-/commits/master)
[![coverage report](https://gitlab.com/corbin-physics/quantum-fields/badges/master/coverage.svg)](https://gitlab.com/corbin-physics/quantum-fields/-/commits/master)


Welcome to the main repository for the Phys  - Quantum Fields project.

## TL;DR

Clone this repository somewhere with [Anaconda Project][] and [Conda][] installed, then run

```bash
make init
```

This will generate a [Conda][] environment you can use to work with the project.  Once
this is done you can make the documentation, tests, etc. with commands like:

```bash
make test
make html
make doc-server
...
```

The latter will host the documentation on https://localhost:8000 and auto-update when you
make changes.

These will use `anaconda-project run` to perform the various actions.  If you want to
manually interact with the environment, then you can run:

```bash
anaconda-project run shell
```

In this shell, you can directly run `pytest` for example.

## Developer Notes

This file can be included in the documentation by adding the following to
`Docs/index.md`:

````markdown
```{include} ../README.md
```
````

See `Docs/index.md` for more details.



<!-- Links -->
[Anaconda Project]: <https://github.com/Anaconda-Platform/anaconda-project> "Anaconda Project"
[CoCalc]: <https://cocalc.com> "CoCalc: Collaborative Calculation and Data Science"
[Conda]: <https://docs.conda.io/en/latest/> "Conda: Package, dependency and environment management for any language—Python, R, Ruby, Lua, Scala, Java, JavaScript, C/ C++, FORTRAN, and more."
[GitHub CI]: <https://docs.github.com/en/actions/guides/about-continuous-integration> "GitHub CI"
[GitHub]: <https://github.com> "GitHub"
[GitLab]: <https://gitlab.com> "GitLab"
[Git]: <https://git-scm.com> "Git"
[Heptapod]: <https://heptapod.net> "Heptapod: is a community driven effort to bring Mercurial SCM support to GitLab"
[Jupyter]: <https://jupyter.org> "Jupyter"
[Jupytext]: <https://jupytext.readthedocs.io> "Jupyter Notebooks as Markdown Documents, Julia, Python or R Scripts"
[Mercurial]: <https://www.mercurial-scm.org> "Mercurial"
[Miniconda]: <https://docs.conda.io/en/latest/miniconda.html> "Miniconda is a free minimal installer for conda."
[MyST]: <https://myst-parser.readthedocs.io/en/latest/> "MyST - Markedly Structured Text"
[Read the Docs]: <https://readthedocs.org> "Read the Docs homepage"
[WSU Physics]: <https://physics.wsu.edu> "WSU Department of Physics and Astronomy"
[WSU Mathematics]: <https://www.math.wsu.edu/> "WSU Department of Mathematics and Statistics"
[`anaconda-project`]: <https://anaconda-project.readthedocs.io> "Anaconda Project: Tool for encapsulating, running, and reproducing data science projects."
[`anybadge`]: <https://github.com/jongracecox/anybadge> "Python project for generating badges for your projects"
[`conda-forge`]: <https://conda-forge.org/> "A community-led collection of recipes, build infrastructure and distributions for the conda package manager."
[`genbadge`]: <https://smarie.github.io/python-genbadge/> "Generate badges for tools that do not provide one."
[`mmf-setup`]: <https://pypi.org/project/mmf-setup/> "PyPI mmf-setup page"
[`pytest`]: <https://docs.pytest.org> "pytest: helps you write better programs"
[hg-git]: <https://hg-git.github.io> "The Hg-Git mercurial plugin"
[GitLab test coverage visualization]: <https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html>




[GitHub Mirror]: <> "GitHub mirror"
[GitLab public repo]: <> "GitLab public repository."
[Gitlab private resources repo]: <> "Private resources repository."
[file an issue]: </-/issues> "Issues on the GitLab project."

<!-- End Links -->
